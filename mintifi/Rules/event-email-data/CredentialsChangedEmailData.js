function CredentialsChangedEmailData(payload) {
    function jsUcfirst(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    };
    if (payload && payload.application) {
        if (payload.application[0]) {
            var applicationData = payload.application[0];
            var eventdata = payload.eventData.EventData;
            var legalBusinessName = jsUcfirst(applicationData.legalBusinessName);
            var email = '';
            var fullName = '';
            var contactName = applicationData.businessApplicantName;
            var url = '{{server_url}}:{{borrower_port}}/login';
            var queryStringData = '?token=' + applicationData.applicationNumber;
            var objPrimaryOwner = applicationData.owners.filter(function (item) {
                if (item.IsPrimary == true) {
                    return item;
                }
            });
            var primaryOwner = null;
            if (objPrimaryOwner != null && objPrimaryOwner.length > 0) {
                primaryOwner = objPrimaryOwner[0];
                fullName = jsUcfirst(primaryOwner.FirstName) + ' ' + jsUcfirst(primaryOwner.LastName);
            }
            if (applicationData != null) {
                email = applicationData.ApplicantWorkEmail;
            }
            var result = {
                Email: email,
                Name: fullName,
                ContactName: contactName,
                LegalBusinessName: legalBusinessName,
                Logo: '{{Logo_url}}',
                url: url,
                queryString: queryStringData,
                ContactAddress: '{{server_url}}:{{server_port}}/static/fc-360/images/contact_address.jpg'
            };
            return {
                'result': 'Passed',
                'detail': null,
                'data': result,
                'rejectcode': '',
                'exception': []
            };
        }
    }
    var errorData = [];
    errorData.push('Data Not found for email');
    return {
        'result': 'Failed',
        'detail': null,
        'data': '',
        'rejectcode': '',
        'exception': errorData
    };
}