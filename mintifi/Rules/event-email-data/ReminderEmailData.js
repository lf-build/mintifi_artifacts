function ReminderEmailData(payload) {
    function jsUcfirst(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    };
    if (payload) {
        var eventdata = payload.eventData.DataAttributes.RecipientId;
        var legalBusinessName = '';
        var email = '';
        var fullName = '';
        var contactName = '';
        var url = '{{server_url}}:{{borrower_port}}/login';
        var queryStringData = '?token=';
        if (eventdata != null) {
            email = eventdata.Email;
        }
        var result = {
            Email: email,
            Name: fullName,
            ContactName: contactName,
            LegalBusinessName: legalBusinessName,
            Logo: '{{Logo_url}}',
            url: url,
            queryString: queryStringData,
            ContactAddress: '{{server_url}}:{{server_port}}/static/fc-360/images/contact_address.jpg'
        };
        return {
            'result': 'Passed',
            'detail': null,
            'data': result,
            'rejectcode': '',
            'exception': []
        };
    }
    var errorData = [];
    errorData.push('Data Not found for email');
    return {
        'result': 'Failed',
        'detail': null,
        'data': '',
        'rejectcode': '',
        'exception': errorData
    };
}