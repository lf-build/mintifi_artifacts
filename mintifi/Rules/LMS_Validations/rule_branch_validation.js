function rule_branch_validation(payload) {
    return {
        'Result': true,
        'Details': {
            'IFSC': 'Valid'
        }
    };
}