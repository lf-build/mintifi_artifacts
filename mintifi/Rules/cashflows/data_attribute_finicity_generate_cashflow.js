function data_attribute_finicity_generate_cashflow(event) {
	var self = this;
	var DataAttributePostFix = '';
	var allowedCategory = ['Loan Fees and Charges', 'Loan Insurance', 'Loan Interest', 'Loan Payment', 'Loan Principal', 'Loans'];
	var ExcludeWeekEnds = false;
	var Finicitymonths = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
	var cashFlowService = self.call('cashflow');
	function isWeekday(year, month, day) {
		var day = new Date(year, month, day).getDay();
		return day != 0 && day != 6;
	}
	function finicityGetDaysInMonth(month, year) {
		var TotalDaysInMonth = new Date(year, month + 1, 0).getDate();
		if (ExcludeWeekEnds == false) {
			return TotalDaysInMonth;
		} else {
			var weekdays = 0;
			for (var i = 0; i < TotalDaysInMonth; i++) {
				if (isWeekday(year, month, i + 1))
					weekdays++;
			}
			return weekdays;
		}
	}
	function FinicitycustomRound(value, precision) {
		var multiplier = Math.pow(10, precision || 0);
		return Math.round(value * multiplier) / multiplier;
	}
	function FinicitysortObject(o) {
		var sorted = {},
			key,
			a = [];
		for (key in o) {
			if (o.hasOwnProperty(key)) {
				a.push(key);
			}
		}
		a.sort();
		for (key = 0; key < a.length; key++) {
			sorted[a[key]] = o[a[key]];
		}
		return sorted;
	}
	function Finicitypad(s) {
		return (s < 10) ? '0' + s : s;
	}
	function FinicitycustomDate(d) {
		return [Finicitypad(d.getMonth() + 1), Finicitypad(d.getDate()), d.getFullYear()].join('/');
	}
	function FinicitycalculateAvgBalanceOfMonth(dailyBalanceList, numberOfDaysInMonth) {
		var totalMonthlyDailyBalance = 0;
		var emptyCount = 1;
		for (var index = 30; index >= 0; index--) {
			if (dailyBalanceList[index] == undefined) {
				emptyCount++;
			} else {
				totalMonthlyDailyBalance += (dailyBalanceList[index] * emptyCount);
				emptyCount = 1;
			}
		}
		return FinicitycustomRound((totalMonthlyDailyBalance / numberOfDaysInMonth), 2);
	}
	function CalculateADB(dailyBalanceList, numberOfDaysInMonth, totalDailyBalance) {
		var totalMonthlyDailyBalance = 0;
		var daysCounter = numberOfDaysInMonth - 1;
		var emptyCount = 1;
		for (var index = daysCounter; index >= 0; index--) {
			if (dailyBalanceList[index] == undefined) {
				emptyCount++;
			} else {
				totalMonthlyDailyBalance += (dailyBalanceList[index] * emptyCount);
				emptyCount = 1;
			}
		}
		if (emptyCount > 1) {
			var lastDailyBalance = totalDailyBalance[totalDailyBalance.length - 1] * (emptyCount - 1);
			totalMonthlyDailyBalance += lastDailyBalance;
		}
		return FinicitycustomRound((totalMonthlyDailyBalance / numberOfDaysInMonth), 2);
	}
	function CalculateADBFirstMonth(dailyBalanceList, numberOfDaysInMonth) {
		var totalMonthlyDailyBalance = 0;
		var emptyCount = 1;
		var daysCounter = numberOfDaysInMonth - 1;
		for (var index = daysCounter; index >= 0; index--) {
			if (dailyBalanceList[index] == undefined) {
				emptyCount++;
			} else {
				totalMonthlyDailyBalance += (dailyBalanceList[index] * emptyCount);
				emptyCount = 1;
			}
		}
		if (emptyCount > 1) {
			numberOfDaysInMonth = (numberOfDaysInMonth - emptyCount) + 1;
		}
		return FinicitycustomRound((totalMonthlyDailyBalance / numberOfDaysInMonth), 2);
	}
	function CalculateADBLastMonth(dailyBalanceList, lastTransactionDate, totalDailyBalance) {
		var totalMonthlyDailyBalance = 0;
		var emptyCount = 1;
		var numberOfDaysInMonth = 0;
		var daysCounter = numberOfDaysInMonth - 1;
		if (lastTransactionDate != null && lastTransactionDate != undefined) {
			numberOfDaysInMonth = new Date(lastTransactionDate).getDate();
			daysCounter = numberOfDaysInMonth - 1;
		}
		for (var index = daysCounter; index >= 0; index--) {
			if (dailyBalanceList[index] == undefined) {
				emptyCount++;
			} else {
				totalMonthlyDailyBalance += (dailyBalanceList[index] * emptyCount);
				emptyCount = 1;
			}
		}
		if (emptyCount > 1) {
			var lastDailyBalance = totalDailyBalance[totalDailyBalance.length - 1] * (emptyCount - 1);
			totalMonthlyDailyBalance += lastDailyBalance;
		}
		return FinicitycustomRound((totalMonthlyDailyBalance / numberOfDaysInMonth), 2);
	}
	function GetDaysBelow100Count(data) {
		var count = 0;
		data = RemoveUndefinedData(data);
		for (var c = 0; c < data.length; c++) {
			if (data[c] < 100) {
				count = count + 1;
			}
		}
		return count;
	}
	function RemoveUndefinedData(data) {
		data = data.filter(function (element) {
			return element !== undefined;
		});
		return data;
	}
	function GetMedianSingleArray(data) {
		data = RemoveUndefinedData(data);
		var m = data.sort(function (a, b) {
			return a - b;
		});
		var middle = Math.floor((m.length - 1) / 2);
		if (m.length % 2) {
			return m[middle];
		} else {
			return (m[middle] + m[middle + 1]) / 2.0;
		}
	}
	function GetMaxDaysBelow100(data) {
		return Math.max.apply(Math, data.map(function (o) {
			return o.DaysBelow100Count;
		}));
	}
	function DailyCoEfficient(data, sum, count) {
		Math.mean = function (array) {
			return array.reduce(function (a, b) {
				return a + b;
			}) / array.length;
		};
		Math.stDeviation = function (array, mean) {
			var dev = array.map(function (itm) {
				return (itm - mean) * (itm - mean);
			});
			return Math.sqrt(dev.reduce(function (a, b) {
				return a + b;
			}) / (array.length - 1));
		};
		data = RemoveUndefinedData(data);
		if (data != null && data.length > 0) {
			var average = Math.mean(data);
			var standardDeviation = Math.stDeviation(data, average);
			if (standardDeviation != undefined && !isNaN(standardDeviation)) {
				return (standardDeviation / average) * 100;
			}
			return 0;
		} else {
			return 0;
		}
	}
	function ConvertEPOCDate(d) {
		var date = new Date(d * 1000);
		date = (date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear();
		return date;
	}
	function FilterTransactions(startDate) {
		var monthDuration = -5;
		var endDate = new Date(startDate);
		endDate = addMonths(endDate, monthDuration);
		endDate = (endDate.getMonth() + 1) + '/' + '1' + '/' + endDate.getFullYear();
		return endDate;
	}
	function addMonths(date, months) {
		date.setMonth(date.getMonth() + months);
		return date;
	}
	function GetTransactionList(trans, handleSuccess) {
		var TransactionList = [];
		if (trans != null) {
			for (var t = 0; t < trans.length; t++) {
				var customDate = getCustomFormattedDate(trans[t].transactionDate);
				var transItem = {
					'Date': customDate.Date,
					'Description': trans[t].description.trim(),
					'Amount': Math.abs(trans[t].amount),
					'TransactionType': trans[t].amount > 0 ? 'Credit' : 'Debit',
					'RunningBalance': 0,
					'Month': customDate.Month,
					'Year': customDate.Year
				};
				TransactionList.push(transItem);
			}
		}
		handleSuccess(TransactionList);
	}
	function GetCategorySummary(trans, handleSuccess) {
		var predefinedCategoryTransactions = [];
		var CategorySummaryList = [];
		for (var c = 0; c < trans.length; c++) {
			if (allowedCategory.indexOf(trans[c].categoryId) > -1) {
				predefinedCategoryTransactions.push(trans[c]);
			}
		}
		var temp = [];
		var dayRange = 30;
		var allData = GetCategoryLookup();
		for (var n = 0; n < allData.length; n++) {
			var tempIndex = allData[n].CustomCategoryId;
			temp[tempIndex] = {
				TransactionCount: 0,
				CategoryName: allData[n].Name,
				TransactionTotal: 0.00,
				CategoryId: allData[n].FinicityCategoryId,
				CustomCategoryId: tempIndex,
				LastMonthTransactionCount: 0,
				LastMonthTransactionTotal: 0.00
			};
			CategorySummaryList.push(temp[tempIndex]);
		}
		predefinedCategoryTransactions.reduce(function (res, value) {
			var tIndex = value.categoryId;
			var objTemp = temp.filter(function (e) {
				return e.CategoryId == tIndex;
			});
			var customIndex = 0;
			if (objTemp != null) {
				if (objTemp.length == 1) {
					customIndex = objTemp[0].CustomCategoryId;
				} else {
					if (value.amount > 0) {
						customIndex = 4;
					} else {
						customIndex = 5;
					}
				}
				temp[customIndex].TransactionTotal += Math.abs(parseFloat(value.amount));
				temp[customIndex].TransactionCount += 1;
				if (GetDaysBetweenDate(value.transactionDate) <= dayRange) {
					temp[customIndex].LastMonthTransactionTotal += Math.abs(parseFloat(value.amount));
					temp[customIndex].LastMonthTransactionCount += 1;
				}
			}
			return temp;
		}, {});
		handleSuccess(CategorySummaryList);
		return CategorySummaryList;
	}
	function GetCategoryLookup() {
		var myCat = [{
			'FinicityCategoryId': 'Loan Fees and Charges',
			'Name': 'Loan Fees and Charges',
			'CustomCategoryId': '1'
		}, {
			'FinicityCategoryId': 'Loan Insurance',
			'Name': 'Loan Insurance',
			'CustomCategoryId': '2'
		}, {
			'FinicityCategoryId': 'Loan Interest',
			'Name': 'Loan Interest',
			'CustomCategoryId': '3'
		}, {
			'FinicityCategoryId': 'Loan Payment',
			'Name': 'Loan Payment',
			'CustomCategoryId': '4'
		}, {
			'FinicityCategoryId': 'Loan Principal',
			'Name': 'Loan Principal',
			'CustomCategoryId': '5'
		}, {
			'FinicityCategoryId': 'Loans',
			'Name': 'Loans',
			'CustomCategoryId': '6'
		}
		];
		return myCat;
	}
	function GetDaysBetweenTransaction(d1, d2) {
		var date1 = new Date(d1);
		var date2 = new Date(d2);
		var timeDiff = Math.abs(date2.getTime() - date1.getTime());
		var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
		return diffDays;
	}
	function GetRoundingAmount(amount, methodType) {
		var result = amount;
		switch (methodType) {
			case 1:
				result = Math.floor(amount);
				break;
			case 2:
				result = Math.ceil(amount);
				break;
		}
		return result;
	}
	function GetGroupByData(data, prop) {
		return data.reduce(function (groups, item) {
			var val = item[prop];
			groups[val] = groups[val] || [];
			groups[val].push(item);
			return groups;
		}, {});
	}
	function GetMinMaxValue(Values, search) {
		var result = 0;
		if (search == 'min') {
			result = 10000;
			for (var i = 0; i < Values.length; i++) {
				if (Values[i].diffDays != undefined && Values[i].diffDays <= result) {
					if (Values[i].diffDays != 0) {
						result = Values[i].diffDays;
					}
				}
			}
		} else if (search == 'max') {
			for (var i = 0; i < Values.length; i++) {
				if (Values[i].diffDays != undefined && Values[i].diffDays >= result) {
					result = Values[i].diffDays;
				}
			}
		}
		return result;
	}
	function GetDaysBetweenDate(d1) {
		var date1 = new Date(d1);
		var date2 = new Date();
		var timeDiff = Math.abs(date2.getTime() - date1.getTime());
		var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
		return diffDays;
	}
	function getFormattedDate(date) {
		date = new Date(date);
		var year = date.getFullYear();
		var month = (1 + date.getMonth()).toString();
		month = month.length > 1 ? month : '0' + month;
		var day = date.getDate().toString();
		day = day.length > 1 ? day : '0' + day;
		return month + '/' + day + '/' + year;
	}
	function GetDateOfMonthlyCycle(date) {
		date = new Date(date);
		var year = date.getFullYear();
		var month = (1 + date.getMonth()).toString();
		month = month.length > 1 ? month : '0' + month;
		return month + '/1/' + year;
	}
	function getCustomFormattedDate(date) {
		date = new Date(date);
		var year = date.getFullYear();
		var month = (1 + date.getMonth()).toString();
		var MonthName = Finicitymonths[date.getMonth()];
		month = month.length > 1 ? month : '0' + month;
		var day = date.getDate().toString();
		day = day.length > 1 ? day : '0' + day;
		var customData = {
			Date: month + '/' + day + '/' + year,
			Month: MonthName,
			Year: year
		};
		return customData;
	}
	function RemoveSpace(str) {
		var result = '';
		var allStr = str.split(' ');
		for (var s = 0; s < allStr.length; s++) {
			result += allStr[s].toString();
		}
		return result;
	}
	function FilterOutlier(trans, handleSuccess) {
		handleSuccess(trans);
	}
	function GetRecurringSummaryByCategoryId(accountId, trans, handleSuccess) {
		var recurringSummary = [];
		var mcaCategories = ['Loan Fees and Charges', 'Loan Insurance', 'Loan Interest', 'Loan Payment', 'Loan Principal', 'Loans'];
		var unsureFlag = 3;
		if (trans != null && trans.length > 0) {
			var allDebitTransactions = trans.filter(function (item) {
				return item.amount < 0 && mcaCategories.indexOf(item.categoryId) > -1;
			});
			var moduloGroups = GetGroupByData(allDebitTransactions, 'amount');
			if (moduloGroups != null) {
				for (var property in moduloGroups) {
					if (moduloGroups[property].constructor === Array) {
						var grp = moduloGroups[property];
						if (grp != null && grp != undefined && grp.length < unsureFlag) { }
						else {
							var moduloSubGroups = GetGroupByData(grp, 'description');
							if (moduloSubGroups != null) {
								for (var p in moduloSubGroups) {
									if (moduloSubGroups[p].constructor === Array) {
										var grpSub = moduloSubGroups[p];
										if (grpSub != null && grpSub != undefined && grpSub.length >= unsureFlag) {
											var allMatchingTransactions = [];
											GetTransactionList(grpSub, function (trs) {
												allMatchingTransactions = trs;
											});
											var temp = {
												'Account': accountId,
												'Amount': Math.abs(grpSub[0].amount),
												'RoudingAmount': Math.abs(grpSub[0].amount),
												'Transactions': allMatchingTransactions,
												'ConfidenceLevel': 'High',
												'TotalTransactions': grpSub.length,
												'IsRecurring': true,
												'Status': 'RECURRING'
											};
											recurringSummary.push(temp);
										}
									}
								}
							}
						}
					}
				}
			}
		}
		handleSuccess(recurringSummary);
	}
	function GetRecurringSummary(accountId, trans, handleSuccess) {
		var recurringSummary = [];
		var moduloRange = 5;
		var roundingMethodType = 2;
		var unsureFlag = 3;
		var minTransactionFlag = 5;
		var dayDifferenceFlag = 7;
		if (trans != null && trans.length > 0) {
			var allDebitTransactions = trans.filter(function (item) {
				return item.amount < 0;
			});
			for (var i = 0; i < allDebitTransactions.length; i++) {
				var rNumber = GetRoundingAmount(allDebitTransactions[i].amount, roundingMethodType);
				allDebitTransactions[i].roudingAmount = rNumber;
				allDebitTransactions[i].roudingAmountModulus = rNumber % moduloRange;
			}
			var moduloGroups = GetGroupByData(allDebitTransactions, 'roudingAmountModulus');
			if (moduloGroups != null) {
				for (var property in moduloGroups) {
					if (moduloGroups[property].constructor === Array) {
						var grp = moduloGroups[property];
						if (grp != null && grp != undefined && grp.length <= unsureFlag) { }
						else {
							var moduloSubGroups = GetGroupByData(grp, 'roudingAmount');
							if (moduloSubGroups != null) {
								for (var p in moduloSubGroups) {
									if (moduloSubGroups[p].constructor === Array) {
										var grpSub = moduloSubGroups[p];
										if (grpSub != null && grpSub != undefined && grpSub.length >= minTransactionFlag) {
											FilterOutlier(grpSub, function (d) {
												grpSub = d;
											});
											grpSub.sort((a, b) => {
												var start = +new Date(a.transactionDate);
												var elapsed = +new Date(b.transactionDate) - start;
												return elapsed;
											});
											for (var i = 0; i < grpSub.length; i++) {
												if (grpSub[i + 1] != undefined) {
													var diffDays = GetDaysBetweenTransaction(grpSub[i].transactionDate, grpSub[i + 1].transactionDate);
													grpSub[i].diffDays = diffDays;
												}
											}
											var MinValue = GetMinMaxValue(grpSub, 'min');
											var MaxValue = GetMinMaxValue(grpSub, 'max');
											var rangeDifference = MaxValue - MinValue;
											if (rangeDifference > 0) {
												if (rangeDifference <= dayDifferenceFlag) {
													var allMatchingTransactions = [];
													GetTransactionList(grpSub, function (trs) {
														allMatchingTransactions = trs;
													});
													var temp = {
														'Account': accountId,
														'Amount': Math.abs(grpSub[0].amount),
														'RoudingAmount': Math.abs(grpSub[0].roudingAmount),
														'Transactions': allMatchingTransactions,
														'ConfidenceLevel': 'High',
														'Min': MinValue,
														'Max': MaxValue,
														'MaxMinDiff': rangeDifference,
														'TotalTransactions': grpSub.length,
														'IsRecurring': true,
														'Status': 'RECURRING'
													};
													recurringSummary.push(temp);
												} else { }
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		handleSuccess(recurringSummary);
	}
	var account = event.Data.Account;
	return cashFlowService.getAllTransactions(account.EntityType, account.EntityId, account.Id).then(function (transactions) {
		return RunCashFlow(account, transactions);
	});
	function RunCashFlow(account, transactions) {
		var result = 'Passed';
		var Category_NSF = ['Bank Fee'];
		var Category_Revenue = ['Business Income'];
		var Category_Exclude_Special_Deposite = ['Interest Income', 'Loans'];
		var endingBalance = 0;
		var beginingBalance = 0;
		var StartDate;
		var EndDate;
		var NegativeBalanceCount = 0;
		var allAccountsCashflows = {};
		var calculateRecurringTransaction = true;
		var addMissingMonths = true;
		var myCustomCashFlow = {
			'MonthlyCashFlows': [{}
			],
			'TransactionSummary': {},
			'CategorySummary': [{}
			],
			'TransactionList': [{}
			],
			'RecurringList': [{}
			],
			'MCARecurringList': [{}
			],
			'LastMonthsSummary': {},
			'GridReport': {}

		};
		var FinicityCashFlowViewModel = {
			'AccountHeader': '',
			'AccountID': '',
			'AccountNumber': '',
			'InstitutionName': '',
			'AccountType': '',
			'referenceNumber': '',
			'IsSelected': false,
			'Source': '',
			'CashFlow': myCustomCashFlow
		};
		var cashFlowResult = FinicityCashFlowViewModel;
		endingBalance = account.CurrentBalance;
		beginingBalance = endingBalance;
		var SelectedTransactions = [];
		var MonthlyCashFlowsList = [];
		var CategorySummaryList = [];
		var TransactionList = [];
		var RecurringList = [];
		var MCARecurringList = [];
		var LastFourDigitAccountNumber = account.AccountNumber;
		var transactions = transactions.transaction;
		if (account.AccountNumber != null) {
			LastFourDigitAccountNumber = account.AccountNumber.substr(-4);
		}
		if (transactions != null && transactions.length > 0) {
			SelectedTransactions = transactions.filter(function (trans) {
				return trans.providerAccountId == account.ProviderAccountId;
			});
			if (SelectedTransactions != null && SelectedTransactions.length > 0) {
				var last6MonthTransactions = [];
				var lastTransactionOn = SelectedTransactions[0].transactionDate;
				var before6MonthTransactionOn = FilterTransactions(lastTransactionOn);
				SelectedTransactions.forEach(function (objTransaction) {
					if (new Date(objTransaction.transactionDate) >= new Date(before6MonthTransactionOn)) {
						last6MonthTransactions.push(objTransaction);
					}
					if (objTransaction.categories != null && objTransaction.categories.length > 0) {
						objTransaction.scheduleC = objTransaction.categories[0];
					}
				});
				last6MonthTransactions = [];
				var totalTransactions = SelectedTransactions.length;
				SelectedTransactions.sort((a, b) => {
					var start = +new Date(a.transactionDate);
					var elapsed = +new Date(b.transactionDate) - start;
					return elapsed;
				});
				if (calculateRecurringTransaction == true) {
					GetRecurringSummary(account.Id, SelectedTransactions, function (trs) {
						RecurringList = trs;
					});
					GetRecurringSummaryByCategoryId(account.Id, SelectedTransactions, function (trs) {
						MCARecurringList = trs;
					});
				}
				GetCategorySummary(SelectedTransactions, function (s) {
					CategorySummaryList = s;
				});
				EndDate = SelectedTransactions[0].transactionDate;
				StartDate = SelectedTransactions[totalTransactions - 1].transactionDate;
				var maxTransactionDate = new Date(EndDate);
				var currentMonth = maxTransactionDate.getMonth();
				var currentYear = maxTransactionDate.getFullYear();
				var lastMonth = currentMonth + '-' + currentYear;
				var minTransactionDate = new Date(StartDate);
				var firstMonth = minTransactionDate.getMonth() + '-' + minTransactionDate.getFullYear();
				var isFirstTransaction = true;
				var isLastMonth = true;
				var dateOfLastTransaction;
				var TotalDailyBalanceArray = new Array();
				TotalDailyBalanceArray.push(beginingBalance);
				var TotalDailyDepositArray = new Array();
				var TotalBrokerAGSDepositArray = new Array();
				var AGS_Month = 3;
				var Last_N_Month = GetLastCalendarMonth(AGS_Month);
				var AGSDeposit_N_Month = 0;
				while (SelectedTransactions.length > 0) {
					var currentMonthTransactions = SelectedTransactions.filter(function (item) {
						if (new Date(item.transactionDate).getMonth() == currentMonth && new Date(item.transactionDate).getFullYear() == currentYear) {
							return item;
						}
					});
					if (addMissingMonths == true) {
						if ((currentMonthTransactions == null || currentMonthTransactions == undefined) || (currentMonthTransactions != null && currentMonthTransactions != undefined && currentMonthTransactions.length == 0)) {
							var tempTransaction = {
								'categoryId': 0,
								'amount': 0,
								'transactionDate': (currentMonth + 1) + '/01/' + currentYear,
								'IsDummyRecord': true
							};
							currentMonthTransactions = [];
							currentMonthTransactions.push(tempTransaction);
						}
					}
					if (currentMonthTransactions != null && currentMonthTransactions != undefined && currentMonthTransactions.length > 0) {
						var FinicityMonthlyCashFlowViewModel = {
							Name: '',
							Year: 0,
							BeginingBalance: 0.00,
							EndingBalance: 0.00,
							DepositCount: 0,
							WithdrawalCount: 0,
							TotalDepositAmount: 0.00,
							TotalWithdrawalAmount: 0.00,
							AverageDailyBalance: 0.00,
							FirstTransactionDate: '',
							EndTransactionDate: '',
							MinDepositAmount: 0.00,
							MaxDepositAmount: 0.00,
							MinWithdrawalAmount: 0.00,
							MaxWithdrawalAmount: 0.00,
							NumberOfNSF: 0,
							NSFAmount: 0.00,
							NumberOfNegativeBalance: 0.00,
							CustomAttributes: '',
							DaysBelow100Count: 0,
							TotalMonthlyRevenueAmount: 0.00,
							TotalMonthlyRevenueCount: 0,
							AverageMonthlyDeposit: 0.00,
							AverageMonthlyWithdrawal: 0.00,
							PDFReportName: '',
							NumberOfSpecialCategoryDeposit: 0,
							DateOfMonthlyCycle: ''
						};
						var currentMonthCashFlow = FinicityMonthlyCashFlowViewModel;
						var monthlyDailyBalance = new Array(31);
						var MinDepositAmount = 0,
							MaxDepositAmount = 0,
							MinWithdrawalAmount = 0,
							MaxWithdrawalAmount = 0;
						var DepositCount = 0,
							WithdrawalCount = 0,
							TotalMonthlyDepositAmount = 0,
							TotalMonthlyWithdrawalAmount = 0;
						var TotalMonthlyNSFAmount = 0,
							TotalMonthlyNSFCount = 0;
						var TotalMonthlyRevenueAmount = 0,
							TotalMonthlyRevenueCount = 0;
						var TotalMonthlyNegativeBalanceCount = 0;
						currentMonthCashFlow.Name = Finicitymonths[currentMonth];
						currentMonthCashFlow.Year = currentYear;
						var AverageMonthlyDeposit = 0.00,
							AverageMonthlyWithdrawal = 0.00;
						var monthInNumber = (parseInt(currentMonth) + 1).toString();
						monthInNumber = monthInNumber.length > 1 ? monthInNumber : '0' + monthInNumber;
						currentMonthCashFlow.PDFReportName = LastFourDigitAccountNumber + '_' + monthInNumber + '_' + currentYear + '.pdf';
						currentMonthCashFlow.EndingBalance = FinicitycustomRound(beginingBalance, 2);
						var currentMonthLastTransaction = currentMonthTransactions[0].transactionDate;
						var index = -1;
						currentMonthTransactions.forEach(function (objTransaction) {
							if (objTransaction != undefined) {
								if (objTransaction.categoryId != undefined) {
									if (Category_NSF.indexOf(objTransaction.categoryId) > -1) {
										TotalMonthlyNSFAmount = TotalMonthlyNSFAmount + objTransaction.amount;
										TotalMonthlyNSFCount = TotalMonthlyNSFCount + 1;
										currentMonthCashFlow.NSFAmount = TotalMonthlyNSFAmount;
										currentMonthCashFlow.NumberOfNSF = TotalMonthlyNSFCount;
									} else if (Category_Revenue.indexOf(objTransaction.scheduleC) > -1) {
										TotalMonthlyRevenueAmount = TotalMonthlyRevenueAmount + objTransaction.amount;
										TotalMonthlyRevenueCount = TotalMonthlyRevenueCount + 1;
										currentMonthCashFlow.TotalMonthlyRevenueAmount = Math.abs(TotalMonthlyRevenueAmount);
										currentMonthCashFlow.TotalMonthlyRevenueCount = TotalMonthlyRevenueCount;
									} else if (Category_Exclude_Special_Deposite.indexOf(objTransaction.categoryId) == -1 && Category_Exclude_Special_Deposite.indexOf(objTransaction.scheduleC) == -1) {
										if (objTransaction.amount > 0) {
											currentMonthCashFlow.NumberOfSpecialCategoryDeposit += 1;
										}
									}
								}
								var currentTransactionDate = new Date(objTransaction.transactionDate);
								var day = currentTransactionDate.getDate();
								var calculateDailyEnding = isFirstTransaction ? isFirstTransaction : (getFormattedDate(dateOfLastTransaction) != getFormattedDate(currentTransactionDate));
								if (calculateDailyEnding) {
									endingBalance = beginingBalance;
									monthlyDailyBalance[day - 1] = endingBalance;
									if (endingBalance < 0) {
										NegativeBalanceCount++;
										TotalMonthlyNegativeBalanceCount++;
									}
								}
								dateOfLastTransaction = currentTransactionDate;
								var transactionAmount = objTransaction.amount;
								if (transactionAmount > 0) {
									beginingBalance -= transactionAmount;
									TotalDailyDepositArray.push(transactionAmount);
									var tempDate = new Date(objTransaction.transactionDate).getMonth() + '-' + new Date(objTransaction.transactionDate).getFullYear();
									if (Last_N_Month.indexOf(tempDate) > -1) {
										TotalBrokerAGSDepositArray.push(transactionAmount);
									}
									if (transactionAmount > MaxDepositAmount || MaxDepositAmount == 0) {
										MaxDepositAmount = transactionAmount;
									}
									if (transactionAmount < MinDepositAmount || MinDepositAmount == 0) {
										MinDepositAmount = transactionAmount;
									}
									DepositCount = DepositCount + 1;
									TotalMonthlyDepositAmount = TotalMonthlyDepositAmount + transactionAmount;
								} else if (transactionAmount < 0) {
									var tempAmount = Math.abs(transactionAmount);
									beginingBalance += tempAmount;
									if (tempAmount > MaxWithdrawalAmount || MaxWithdrawalAmount == 0) {
										MaxWithdrawalAmount = tempAmount;
									}
									if (tempAmount < MinWithdrawalAmount || MinWithdrawalAmount == 0) {
										MinWithdrawalAmount = tempAmount;
									}
									WithdrawalCount = WithdrawalCount + 1;
									TotalMonthlyWithdrawalAmount = TotalMonthlyWithdrawalAmount + tempAmount;
								}
								isFirstTransaction = false;
								index = -1;
								index = SelectedTransactions.indexOf(objTransaction, 0);
								if (index > -1) {
									SelectedTransactions.splice(index, 1);
								}
								if (totalTransactions > 1 && index > -1) {
									TotalDailyBalanceArray.push(beginingBalance);
								}
							}
							if (index > -1) {
								totalTransactions -= 1;
							}
						});
						currentMonthCashFlow.NumberOfNegativeBalance = FinicitycustomRound(TotalMonthlyNegativeBalanceCount, 2);
						currentMonthCashFlow.TotalWithdrawalAmount = FinicitycustomRound(TotalMonthlyWithdrawalAmount, 2);
						currentMonthCashFlow.WithdrawalCount = WithdrawalCount;
						currentMonthCashFlow.TotalDepositAmount = FinicitycustomRound(Math.abs(TotalMonthlyDepositAmount), 2);
						currentMonthCashFlow.DepositCount = DepositCount;
						currentMonthCashFlow.MinDepositAmount = Math.abs(MinDepositAmount);
						currentMonthCashFlow.MaxDepositAmount = Math.abs(MaxDepositAmount);
						currentMonthCashFlow.MinWithdrawalAmount = MinWithdrawalAmount;
						currentMonthCashFlow.MaxWithdrawalAmount = MaxWithdrawalAmount;
						currentMonthCashFlow.AverageMonthlyDeposit = FinicitycustomRound(currentMonthCashFlow.TotalDepositAmount / currentMonthCashFlow.DepositCount, 2);
						currentMonthCashFlow.AverageMonthlyWithdrawal = FinicitycustomRound(currentMonthCashFlow.TotalWithdrawalAmount / currentMonthCashFlow.WithdrawalCount, 2);
						if (currentMonthTransactions[0].IsDummyRecord == undefined) {
							currentMonthCashFlow.FirstTransactionDate = FinicitycustomDate(dateOfLastTransaction);
							if (currentMonthTransactions.length > 0) {
								currentMonthCashFlow.EndTransactionDate = FinicitycustomDate(new Date(currentMonthTransactions[0].transactionDate));
							}
						}
						currentMonthCashFlow.BeginingBalance = FinicitycustomRound(beginingBalance, 2);
						var totalDaysInmonth = 0;
						totalDaysInmonth = finicityGetDaysInMonth(dateOfLastTransaction.getMonth(), dateOfLastTransaction.getFullYear());
						var monthCheck = currentMonth + '-' + currentYear;
						if (firstMonth == lastMonth) {
							currentMonthCashFlow.AverageDailyBalance = CalculateADBFirstMonth(monthlyDailyBalance, totalDaysInmonth);
							currentMonthCashFlow.DateOfMonthlyCycle = getFormattedDate(dateOfLastTransaction);
						} else if (lastMonth == monthCheck) {
							currentMonthCashFlow.AverageDailyBalance = CalculateADBLastMonth(monthlyDailyBalance, currentMonthLastTransaction, TotalDailyBalanceArray);
							currentMonthCashFlow.DateOfMonthlyCycle = getFormattedDate(currentMonthLastTransaction);
						} else if (firstMonth == monthCheck) {
							currentMonthCashFlow.AverageDailyBalance = CalculateADBFirstMonth(monthlyDailyBalance, totalDaysInmonth);
							currentMonthCashFlow.DateOfMonthlyCycle = GetDateOfMonthlyCycle(dateOfLastTransaction);
						} else {
							currentMonthCashFlow.AverageDailyBalance = CalculateADB(monthlyDailyBalance, totalDaysInmonth, TotalDailyBalanceArray);
							currentMonthCashFlow.DateOfMonthlyCycle = GetDateOfMonthlyCycle(dateOfLastTransaction);
						}
						currentMonthCashFlow.DaysBelow100Count = GetDaysBelow100Count(monthlyDailyBalance);
						MonthlyCashFlowsList.push(FinicitysortObject(currentMonthCashFlow));
						isLastMonth = false;
						if (Last_N_Month.indexOf(monthCheck) > -1 && TotalBrokerAGSDepositArray != null && TotalBrokerAGSDepositArray.length > 0) {
							AGSDeposit_N_Month = AGSSum(TotalBrokerAGSDepositArray);
						}
					}
					currentMonth -= 1;
					if (currentMonth < 0) {
						currentYear -= 1;
						currentMonth = 11;
					}
				}
				var FinicityTransactionSummaryViewModel = {
					CountOfMonthlyStatement: 0,
					StartDate: '',
					EndDate: '',
					AverageDeposit: 0,
					AnnualCalculatedRevenue: 0.00,
					AverageWithdrawal: 0,
					AverageDailyBalance: 0,
					NumberOfNegativeBalance: 0,
					NumberOfNSF: 0,
					NSFAmount: 0,
					ChangeInDepositVolume: 0,
					TotalCredits: 0,
					TotalDebits: 0,
					TotalCreditsCount: 0,
					TotalDebitsCount: 0,
					AvailableBalance: 0,
					CurrentBalance: 0,
					AverageBalanceLastMonth: 0,
					MedianMonthlyIncome: 0,
					MedianDailyBalance: 0,
					MaxDaysBelow100Count: 0,
					CVOfDailyDeposit: 0,
					CVOfDailyBalance: 0,
					AverageMonthlyRevenue: 0.00,
					TotalRevenueAmount: 0.00,
					TotalRevenueCount: 0,
					PDFReportName: '',
					BrokerAGS: 0,
					CAPAGS: 0,
					BrokerAGSText: ''
				};
				var objTransactionSummary = FinicityTransactionSummaryViewModel;
				objTransactionSummary.PDFReportName = LastFourDigitAccountNumber + '.pdf';
				objTransactionSummary.CountOfMonthlyStatement = MonthlyCashFlowsList.length;
				objTransactionSummary.StartDate = getFormattedDate(StartDate);
				objTransactionSummary.EndDate = getFormattedDate(EndDate);
				var TotalRevenueAmount = 0,
					TotalRevenueCount = 0;
				var TotalDepositAmount = 0;
				var TotalWithdrawalAmount = 0;
				var TotalDailyAverageBalance = 0;
				var TotalNSFAmount = 0;
				var TotalNSFCount = 0;
				var TotalDebitsCount = 0,
					TotalCreditsCount = 0;
				var TotalMonthlyIncomeArray = new Array();
				MonthlyCashFlowsList.forEach(function (cashflow) {
					TotalRevenueAmount += cashflow.TotalMonthlyRevenueAmount;
					TotalRevenueCount += cashflow.TotalMonthlyRevenueCount;
					TotalDepositAmount += cashflow.TotalDepositAmount;
					TotalWithdrawalAmount += cashflow.TotalWithdrawalAmount;
					TotalDailyAverageBalance += cashflow.AverageDailyBalance;
					TotalNSFAmount += cashflow.NSFAmount;
					TotalNSFCount += cashflow.NumberOfNSF;
					TotalCreditsCount += cashflow.DepositCount;
					TotalDebitsCount += cashflow.WithdrawalCount;
					TotalMonthlyIncomeArray.push(cashflow.TotalDepositAmount);
				});
				objTransactionSummary.TotalRevenueAmount = FinicitycustomRound((Math.abs(TotalRevenueAmount)), 2);
				objTransactionSummary.TotalRevenueCount = TotalRevenueCount;
				objTransactionSummary.AverageMonthlyRevenue = FinicitycustomRound((TotalRevenueAmount / MonthlyCashFlowsList.length), 2);
				objTransactionSummary.MedianMonthlyIncome = GetMedianSingleArray(TotalMonthlyIncomeArray);
				objTransactionSummary.MedianDailyBalance = GetMedianSingleArray(TotalDailyBalanceArray);
				objTransactionSummary.MaxDaysBelow100Count = GetMaxDaysBelow100(MonthlyCashFlowsList);
				objTransactionSummary.CVOfDailyBalance = FinicitycustomRound(DailyCoEfficient(TotalDailyBalanceArray), 2);
				objTransactionSummary.CVOfDailyDeposit = FinicitycustomRound(DailyCoEfficient(TotalDailyDepositArray), 2);
				objTransactionSummary.TotalCredits = TotalDepositAmount;
				objTransactionSummary.TotalDebits = TotalWithdrawalAmount;
				objTransactionSummary.TotalCreditsCount = TotalCreditsCount;
				objTransactionSummary.TotalDebitsCount = TotalDebitsCount;
				objTransactionSummary.AvailableBalance = account.CurrentBalance;
				objTransactionSummary.CurrentBalance = account.CurrentBalance;
				objTransactionSummary.AverageBalanceLastMonth = MonthlyCashFlowsList[0].AverageDailyBalance;
				objTransactionSummary.AverageDeposit = FinicitycustomRound((TotalDepositAmount / MonthlyCashFlowsList.length), 2);
				objTransactionSummary.AnnualCalculatedRevenue = FinicitycustomRound((TotalDepositAmount / MonthlyCashFlowsList.length) * 12, 2);
				objTransactionSummary.AverageWithdrawal = FinicitycustomRound((TotalWithdrawalAmount / MonthlyCashFlowsList.length), 2);
				objTransactionSummary.AverageDailyBalance = FinicitycustomRound((TotalDailyAverageBalance / MonthlyCashFlowsList.length), 2);
				objTransactionSummary.NumberOfNegativeBalance = NegativeBalanceCount;
				objTransactionSummary.NumberOfNSF = TotalNSFCount;
				objTransactionSummary.NSFAmount = FinicitycustomRound(TotalNSFAmount, 2);
				if (AGSDeposit_N_Month != 0) {
					var Broker_AverageDepositVolume = AGSDeposit_N_Month / AGS_Month;
					objTransactionSummary.BrokerAGS = FinicitycustomRound(Broker_AverageDepositVolume * 12, 2);
				}
				var BrokerAGSMonthlyLimit = 4;
				if (MonthlyCashFlowsList.length < BrokerAGSMonthlyLimit) {
					objTransactionSummary.BrokerAGS = 0;
					objTransactionSummary.BrokerAGSText = 'Less than 3 months data';
				}
				if (TotalDailyDepositArray != null && TotalDailyDepositArray.length > 0) {
					var CAP_AverageDepositVolume = AGSSum(TotalDailyDepositArray) / MonthlyCashFlowsList.length;
					objTransactionSummary.CAPAGS = FinicitycustomRound(CAP_AverageDepositVolume * 12, 2);
				}
				var t = myCustomCashFlow;
				t.MonthlyCashFlows = MonthlyCashFlowsList;
				t.LastMonthsSummary = CalcultationLastMonthsList(MonthlyCashFlowsList);
				t.CategorySummary = CategorySummaryList;
				t.RecurringList = RecurringList;
				t.GridReport = GetGridReport(MonthlyCashFlowsList, account.AccountType, account.AccountType, account.AccountNumber);
				t.MCARecurringList = MCARecurringList;
				var objTemp = FinicitysortObject(objTransactionSummary);
				t.TransactionSummary = objTemp;
				cashFlowResult.CashFlow = t;
			} else {
				var FinicityTransactionSummaryViewModel = {
					CountOfMonthlyStatement: 0,
					StartDate: '',
					EndDate: '',
					AverageDeposit: 0,
					AnnualCalculatedRevenue: 0.00,
					AverageWithdrawal: 0,
					AverageDailyBalance: 0,
					NumberOfNegativeBalance: 0,
					NumberOfNSF: 0,
					NSFAmount: 0,
					ChangeInDepositVolume: 0,
					TotalCredits: 0,
					TotalDebits: 0,
					TotalCreditsCount: 0,
					TotalDebitsCount: 0,
					AvailableBalance: 0,
					CurrentBalance: 0,
					AverageBalanceLastMonth: 0,
					MedianMonthlyIncome: 0,
					MedianDailyBalance: 0,
					MaxDaysBelow100Count: 0,
					CVOfDailyDeposit: 0,
					CVOfDailyBalance: 0,
					AverageMonthlyRevenue: 0.00,
					TotalRevenueAmount: 0.00,
					TotalRevenueCount: 0,
					PDFReportName: '',
					BrokerAGS: 0,
					CAPAGS: 0,
					BrokerAGSText: ''
				};
				var objTransactionSummary = FinicityTransactionSummaryViewModel;
				objTransactionSummary.AvailableBalance = account.CurrentBalance;
				objTransactionSummary.CurrentBalance = account.CurrentBalance;
				var t = myCustomCashFlow;
				var objTemp = FinicitysortObject(objTransactionSummary);
				t.TransactionSummary = objTemp;
				cashFlowResult.CashFlow = t;
			}
		} else {
			var FinicityTransactionSummaryViewModel = {
				CountOfMonthlyStatement: 0,
				StartDate: '',
				EndDate: '',
				AverageDeposit: 0,
				AnnualCalculatedRevenue: 0.00,
				AverageWithdrawal: 0,
				AverageDailyBalance: 0,
				NumberOfNegativeBalance: 0,
				NumberOfNSF: 0,
				NSFAmount: 0,
				ChangeInDepositVolume: 0,
				TotalCredits: 0,
				TotalDebits: 0,
				TotalCreditsCount: 0,
				TotalDebitsCount: 0,
				AvailableBalance: 0,
				CurrentBalance: 0,
				AverageBalanceLastMonth: 0,
				MedianMonthlyIncome: 0,
				MedianDailyBalance: 0,
				MaxDaysBelow100Count: 0,
				CVOfDailyDeposit: 0,
				CVOfDailyBalance: 0,
				AverageMonthlyRevenue: 0.00,
				TotalRevenueAmount: 0.00,
				TotalRevenueCount: 0,
				PDFReportName: '',
				BrokerAGS: 0,
				CAPAGS: 0,
				BrokerAGSText: ''
			};
			var objTransactionSummary = FinicityTransactionSummaryViewModel;
			objTransactionSummary.AvailableBalance = account.CurrentBalance;
			objTransactionSummary.CurrentBalance = account.CurrentBalance;
			var t = myCustomCashFlow;
			var objTemp = FinicitysortObject(objTransactionSummary);
			t.TransactionSummary = objTemp;
			cashFlowResult.CashFlow = t;
		}
		var formattedAccountNumber = account.BankName;
		cashFlowResult.InstitutionName = account.BankName;
		DataAttributePostFix = account.Id;
		cashFlowResult.AccountID = account.Id;
		cashFlowResult.AccountType = account.AccountType;
		cashFlowResult.AccountHeader = formattedAccountNumber + '-' + LastFourDigitAccountNumber + '-' + account.AccountType;
		cashFlowResult.AccountNumber = account.AccountNumber;
		cashFlowResult.Source = account.Source;
		allAccountsCashflows = cashFlowResult;
		var resultKey = DataAttributePostFix;
		var finalResult = {};
		finalResult[resultKey] = allAccountsCashflows;
		return finalResult;
	}
	function CalcultationLastMonthsList(monthwiseData) {
		var array = [3, 6, 12];
		var dict = {};
		var currentMonth = parseInt((new Date().getMonth() + 1));
		var currentYear = parseInt(new Date().getFullYear());
		for (var i = 0; i < array.length; i++) {
			if (monthwiseData.length >= array[i]) {
				var monthDataList = monthwiseData.filter(function (item) {
					var count = 0;
					if (item.MonthInNumber != currentMonth && item.Year == currentYear && count < array[i]) {
						count++;
						return item;
					}
				});
				var list = [];
				list.push(CalculateLastMonthDataAverage(monthDataList));
				list.push(CalculateLastMonthDataMin(monthDataList));
				list.push(CalculateLastMonthDataMax(monthDataList));
				dict[array[i]] = list;
			}
		}
		function CalculateLastMonthDataAverage(lastMonthlist) {
			var monthValue = lastMonthlist.length;
			var DepositsTemp = 0.00;
			var DepositsCountTemp = 0;
			var DepositAverageTemp = 0.00;
			var BeginingBalanceTemp = 0.00;
			var EndingBalanceTemp = 0.00;
			var IncDecTemp = 0.00;
			var ADBTemp = 0.00;
			var ADBPercentTemp = 0.00;
			var NegativeDaysTemp = 0;
			var NSFTemp = 0;
			for (var i = 0; i < monthValue; i++) {
				DepositsTemp += (lastMonthlist[i].TotalDepositAmount == null ? 0 : lastMonthlist[i].TotalDepositAmount);
				DepositsCountTemp += (lastMonthlist[i].DepositCount == null ? 0 : lastMonthlist[i].DepositCount);
				DepositAverageTemp += (lastMonthlist[i].AverageDeposit == null ? 0 : lastMonthlist[i].AverageDeposit);
				BeginingBalanceTemp += (lastMonthlist[i].BeginingBalance == null ? 0 : lastMonthlist[i].BeginingBalance);
				EndingBalanceTemp += (lastMonthlist[i].EndingBalance == null ? 0 : lastMonthlist[i].EndingBalance);
				IncDecTemp += (lastMonthlist[i].IncDecBalance == null ? 0 : lastMonthlist[i].IncDecBalance);
				ADBTemp += (lastMonthlist[i].AverageDailyBalance == null ? 0 : lastMonthlist[i].AverageDailyBalance);
				NegativeDaysTemp += (lastMonthlist[i].NumberOfNegativeBalance == null ? 0 : lastMonthlist[i].NumberOfNegativeBalance);
				NSFTemp += (lastMonthlist[i].NumberOfNSF == null ? 0 : lastMonthlist[i].NumberOfNSF);
			}
			var avgObject = {
				Name: '',
				GridRowHeader: 'AVG',
				Year: 0,
				BeginingBalance: BeginingBalanceTemp / monthValue,
				EndingBalance: EndingBalanceTemp / monthValue,
				DepositCount: parseInt(DepositsCountTemp / monthValue),
				WithdrawalCount: 0,
				TotalDepositAmount: DepositsTemp / monthValue,
				TotalWithdrawalAmount: 0.00,
				AverageDailyBalance: ADBTemp / monthValue,
				AverageDeposit: DepositAverageTemp / monthValue,
				IncDecBalance: IncDecTemp / monthValue,
				AverageDailyBalancePercent: ((ADBPercentageCalculation(ADBTemp / monthValue, DepositsTemp / monthValue)) == null ? 0 : (ADBPercentageCalculation(ADBTemp / monthValue, DepositsTemp / monthValue))),
				PeriodFrom: '',
				PeriodTo: '',
				MonthInNumber: 0,
				FirstTransactionDate: '',
				EndTransactionDate: '',
				MinDepositAmount: 0.00,
				MaxDepositAmount: 0.00,
				MinWithdrawalAmount: 0.00,
				MaxWithdrawalAmount: 0.00,
				NumberOfNSF: NSFTemp / monthValue,
				NSFAmount: 0.00,
				LoanPaymentAmount: 0.00,
				NumberOfLoanPayment: 0,
				PayrollAmount: 0.00,
				NumberOfPayroll: 0,
				NumberOfNegativeBalance: NegativeDaysTemp / monthValue,
				CustomAttributes: '',
				DaysBelow100Count: 0,
				TotalMonthlyRevenueAmount: 0.00,
				TotalMonthlyRevenueCount: 0,
				PDFReportName: '',
				DateOfMonthlyCycle: ''
			};
			return avgObject;
		}
		function CalculateLastMonthDataMin(lastMonthlist) {
			var monthValue = lastMonthlist.length;
			var DepositsTemp = 0.00;
			var DepositsCountTemp = 0;
			var DepositAverageTemp = 0.00;
			var BeginingBalanceTemp = 0.00;
			var EndingBalanceTemp = 0.00;
			var IncDecTemp = 0.00;
			var ADBTemp = 0.00;
			var ADBPercentTemp = 0.00;
			var NegativeDaysTemp = 0;
			var NSFTemp = 0;
			DepositsTemp = GetMinMaxValueCashflow(lastMonthlist, 'min', 'TotalDepositAmount');
			DepositsCountTemp = GetMinMaxValueCashflow(lastMonthlist, 'min', 'DepositCount');
			DepositAverageTemp = GetMinMaxValueCashflow(lastMonthlist, 'min', 'AverageDeposit');
			BeginingBalanceTemp = GetMinMaxValueCashflow(lastMonthlist, 'min', 'BeginingBalance');
			EndingBalanceTemp = GetMinMaxValueCashflow(lastMonthlist, 'min', 'EndingBalance');
			IncDecTemp = GetMinMaxValueCashflow(lastMonthlist, 'min', 'IncDecBalance');
			ADBTemp = GetMinMaxValueCashflow(lastMonthlist, 'min', 'AverageDailyBalance');
			NegativeDaysTemp = GetMinMaxValueCashflow(lastMonthlist, 'min', 'NumberOfNegativeBalance');
			NSFTemp = GetMinMaxValueCashflow(lastMonthlist, 'min', 'NumberOfNSF');
			ADBPercentTemp = GetMinMaxValueCashflow(lastMonthlist, 'min', 'AverageDailyBalancePercent');
			var avgObject = {
				Name: '',
				GridRowHeader: 'MIN',
				Year: 0,
				BeginingBalance: BeginingBalanceTemp,
				EndingBalance: EndingBalanceTemp,
				DepositCount: DepositsCountTemp,
				WithdrawalCount: 0,
				TotalDepositAmount: DepositsTemp,
				TotalWithdrawalAmount: 0.00,
				AverageDailyBalance: ADBTemp,
				AverageDeposit: DepositAverageTemp,
				IncDecBalance: IncDecTemp,
				AverageDailyBalancePercent: (ADBPercentTemp == null ? 0 : ADBPercentTemp),
				PeriodFrom: '',
				PeriodTo: '',
				MonthInNumber: 0,
				FirstTransactionDate: '',
				EndTransactionDate: '',
				MinDepositAmount: 0.00,
				MaxDepositAmount: 0.00,
				MinWithdrawalAmount: 0.00,
				MaxWithdrawalAmount: 0.00,
				NumberOfNSF: NSFTemp,
				NSFAmount: 0.00,
				LoanPaymentAmount: 0.00,
				NumberOfLoanPayment: 0,
				PayrollAmount: 0.00,
				NumberOfPayroll: 0,
				NumberOfNegativeBalance: NegativeDaysTemp,
				CustomAttributes: '',
				DaysBelow100Count: 0,
				TotalMonthlyRevenueAmount: 0.00,
				TotalMonthlyRevenueCount: 0,
				PDFReportName: '',
				DateOfMonthlyCycle: ''
			};
			return avgObject;
		}
		function CalculateLastMonthDataMax(lastMonthlist) {
			var monthValue = lastMonthlist.length;
			var DepositsTemp = 0.00;
			var DepositsCountTemp = 0;
			var DepositAverageTemp = 0.00;
			var BeginingBalanceTemp = 0.00;
			var EndingBalanceTemp = 0.00;
			var IncDecTemp = 0.00;
			var ADBTemp = 0.00;
			var ADBPercentTemp = 0.00;
			var NegativeDaysTemp = 0;
			var NSFTemp = 0;
			DepositsTemp = GetMinMaxValueCashflow(lastMonthlist, 'max', 'TotalDepositAmount');
			DepositsCountTemp = GetMinMaxValueCashflow(lastMonthlist, 'max', 'DepositCount');
			DepositAverageTemp = GetMinMaxValueCashflow(lastMonthlist, 'max', 'AverageDeposit');
			BeginingBalanceTemp = GetMinMaxValueCashflow(lastMonthlist, 'max', 'BeginingBalance');
			EndingBalanceTemp = GetMinMaxValueCashflow(lastMonthlist, 'max', 'EndingBalance');
			IncDecTemp = GetMinMaxValueCashflow(lastMonthlist, 'max', 'IncDecBalance');
			ADBTemp = GetMinMaxValueCashflow(lastMonthlist, 'max', 'AverageDailyBalance');
			NegativeDaysTemp = GetMinMaxValueCashflow(lastMonthlist, 'max', 'NumberOfNegativeBalance');
			NSFTemp = GetMinMaxValueCashflow(lastMonthlist, 'max', 'NumberOfNSF');
			ADBPercentTemp = GetMinMaxValueCashflow(lastMonthlist, 'max', 'AverageDailyBalancePercent');
			var avgObject = {
				Name: '',
				GridRowHeader: 'MAX',
				Year: 0,
				BeginingBalance: (BeginingBalanceTemp == null ? 0 : BeginingBalanceTemp),
				EndingBalance: (EndingBalanceTemp == null ? 0 : EndingBalanceTemp),
				DepositCount: (DepositsCountTemp == null ? 0 : DepositsCountTemp),
				WithdrawalCount: 0,
				TotalDepositAmount: (DepositsTemp == null ? 0 : DepositsTemp),
				TotalWithdrawalAmount: 0.00,
				AverageDailyBalance: (ADBTemp == null ? 0 : ADBTemp),
				AverageDeposit: (DepositAverageTemp == null ? 0 : DepositAverageTemp),
				IncDecBalance: (IncDecTemp == null ? 0 : IncDecTemp),
				AverageDailyBalancePercent: (ADBPercentTemp == null ? 0 : ADBPercentTemp),
				PeriodFrom: '',
				PeriodTo: '',
				MonthInNumber: 0,
				FirstTransactionDate: '',
				EndTransactionDate: '',
				MinDepositAmount: 0.00,
				MaxDepositAmount: 0.00,
				MinWithdrawalAmount: 0.00,
				MaxWithdrawalAmount: 0.00,
				NumberOfNSF: NSFTemp,
				NSFAmount: 0.00,
				LoanPaymentAmount: 0.00,
				NumberOfLoanPayment: 0,
				PayrollAmount: 0.00,
				NumberOfPayroll: 0,
				NumberOfNegativeBalance: (NegativeDaysTemp == null ? 0 : NegativeDaysTemp),
				CustomAttributes: '',
				DaysBelow100Count: 0,
				TotalMonthlyRevenueAmount: 0.00,
				TotalMonthlyRevenueCount: 0,
				PDFReportName: '',
				DateOfMonthlyCycle: ''
			};
			return avgObject;
		}
		function GetMinMaxValueCashflow(Values, search, item) {
			var result = 0;
			if (search == 'min') {
				result = Values[0][item];
				for (var i = 0; i < Values.length; i++) {
					if (Values[i][item] != undefined && Values[i][item] <= result && Values[i][item] != null) {
						result = Values[i][item];
					}
				}
			} else if (search == 'max') {
				for (var i = 0; i < Values.length; i++) {
					if (Values[i][item] != undefined && Values[i][item] >= result && Values[i][item] != null) {
						result = Values[i][item];
					}
				}
			}
			return result;
		}
		return dict;
	}
	function ADBPercentageCalculation(averageDailyBalance, totalDepositAmount) {
		if (averageDailyBalance == null) {
			averageDailyBalance = 0;
		}
		if (totalDepositAmount == 0 || totalDepositAmount == null)
			return 0;
		var resultADBPercentage = ((averageDailyBalance * 100) / totalDepositAmount);
		return FinicitycustomRound(resultADBPercentage, 2);
	}
	function GetLastCalendarMonth(counter) {
		var today = new Date();
		var lastNMonths = [];
		for (var i = 1; i <= counter; i++) {
			var month = today.getMonth() - i;
			var year = today.getFullYear();
			if (month < 0) {
				month = month + 12;
				year = year - 1;
			}
			lastNMonths.push(month + '-' + year);
		}
		return lastNMonths;
	}
	function GetGridReportFormat() {
		var gridReport = {
			'GridRowHeader': '',
			'GridRowOrder': 100,
			'ColumnValue': {
				'Deposits': 0.00,
				'DepositCount': 0,
				'DepositAverage': 0.00,
				'BeginningBalance': 0.00,
				'EndingBalance': 0.00,
				'IncDec': 0.00,
				'ADB': 0.00,
				'ADBPercentage': 0.00,
				'NegativeDays': 0,
				'NSF': 0,
				'Name': '',
				'Year': ''
			}
		};
		return gridReport;
	}
	function GetTotalForGridReport(arrayData, type) {
		var totalDepositsAmount = 0.00;
		var totalDepositsCount = 0;
		var totalADB = 0.00;
		var totalNegativeDays = 0;
		var totalNSF = 0;
		var beginningBalance = 0.00;
		var endingBalance = 0.00;
		var IncDec = 0.00;
		var ADBPercentage = 0.00;
		var DepositAverage = 0.00;
		var result = {
			totalDepositsAmount: 0.00,
			totalDepositsCount: 0,
			totalADB: 0.00,
			totalNegativeDays: 0,
			totalNSF: 0,
			beginningBalance: 0.00,
			endingBalance: 0.00,
			IncDec: 0.00,
			ADBPercentage: 0.00,
			DepositAverage: 0.00
		};
		if (type == 'AVG') {
			arrayData.forEach(function (objData) {
				totalDepositsAmount += objData.ColumnValue.Deposits;
				totalDepositsCount += objData.ColumnValue.DepositCount;
				DepositAverage += objData.ColumnValue.DepositAverage;
				totalADB += objData.ColumnValue.ADB;
				totalNegativeDays += objData.ColumnValue.NegativeDays;
				totalNSF += objData.ColumnValue.NSF;
				beginningBalance += objData.ColumnValue.BeginningBalance;
				endingBalance += objData.ColumnValue.EndingBalance;
				ADBPercentage += objData.ColumnValue.ADBPercentage;
				IncDec += objData.ColumnValue.IncDec;
			});
		} else if (type == 'MIN' || type == 'MAX') {
			totalDepositsAmount = arrayData[0].ColumnValue.Deposits;
			totalDepositsCount = arrayData[0].ColumnValue.DepositCount;
			DepositAverage = arrayData[0].ColumnValue.DepositAverage;
			totalADB = arrayData[0].ColumnValue.ADB;
			totalNegativeDays = arrayData[0].ColumnValue.NegativeDays;
			totalNSF = arrayData[0].ColumnValue.NSF;
			beginningBalance = arrayData[0].ColumnValue.BeginningBalance;
			endingBalance = arrayData[0].ColumnValue.EndingBalance;
			ADBPercentage = arrayData[0].ColumnValue.ADBPercentage;
			IncDec = arrayData[0].ColumnValue.IncDec;
			if (type == 'MIN') {
				for (var X = 0; X < arrayData.length; X++) {
					if (totalDepositsAmount > arrayData[X].ColumnValue.Deposits) {
						totalDepositsAmount = arrayData[X].ColumnValue.Deposits;
					}
					if (totalDepositsCount > arrayData[X].ColumnValue.DepositCount) {
						totalDepositsCount = arrayData[X].ColumnValue.DepositCount;
					}
					if (DepositAverage > arrayData[X].ColumnValue.DepositAverage) {
						DepositAverage = arrayData[X].ColumnValue.DepositAverage;
					}
					if (totalADB > arrayData[X].ColumnValue.ADB) {
						totalADB = arrayData[X].ColumnValue.ADB;
					}
					if (totalNegativeDays > arrayData[X].ColumnValue.NegativeDays) {
						totalNegativeDays = arrayData[X].ColumnValue.NegativeDays;
					}
					if (totalNSF > arrayData[X].ColumnValue.NSF) {
						totalNSF = arrayData[X].ColumnValue.NSF;
					}
					if (beginningBalance > arrayData[X].ColumnValue.BeginningBalance) {
						beginningBalance = arrayData[X].ColumnValue.BeginningBalance;
					}
					if (endingBalance > arrayData[X].ColumnValue.EndingBalance) {
						endingBalance = arrayData[X].ColumnValue.EndingBalance;
					}
					if (ADBPercentage > arrayData[X].ColumnValue.ADBPercentage) {
						ADBPercentage = arrayData[X].ColumnValue.ADBPercentage;
					}
					if (IncDec > arrayData[X].ColumnValue.IncDec) {
						IncDec = arrayData[X].ColumnValue.IncDec;
					}
				}
			} else if (type == 'MAX') {
				for (var Y = 0; Y < arrayData.length; Y++) {
					if (totalDepositsAmount < arrayData[Y].ColumnValue.Deposits) {
						totalDepositsAmount = arrayData[Y].ColumnValue.Deposits;
					}
					if (totalDepositsCount < arrayData[Y].ColumnValue.DepositCount) {
						totalDepositsCount = arrayData[Y].ColumnValue.DepositCount;
					}
					if (DepositAverage < arrayData[Y].ColumnValue.DepositAverage) {
						DepositAverage = arrayData[Y].ColumnValue.DepositAverage;
					}
					if (totalADB < arrayData[Y].ColumnValue.ADB) {
						totalADB = arrayData[Y].ColumnValue.ADB;
					}
					if (totalNegativeDays < arrayData[Y].ColumnValue.NegativeDays) {
						totalNegativeDays = arrayData[Y].ColumnValue.NegativeDays;
					}
					if (totalNSF < arrayData[Y].ColumnValue.NSF) {
						totalNSF = arrayData[Y].ColumnValue.NSF;
					}
					if (beginningBalance < arrayData[Y].ColumnValue.BeginningBalance) {
						beginningBalance = arrayData[Y].ColumnValue.BeginningBalance;
					}
					if (endingBalance < arrayData[Y].ColumnValue.EndingBalance) {
						endingBalance = arrayData[Y].ColumnValue.EndingBalance;
					}
					if (ADBPercentage < arrayData[Y].ColumnValue.ADBPercentage) {
						ADBPercentage = arrayData[Y].ColumnValue.ADBPercentage;
					}
					if (IncDec < arrayData[Y].ColumnValue.IncDec) {
						IncDec = arrayData[Y].ColumnValue.IncDec;
					}
				}
			}
		}
		result.totalDepositsAmount = totalDepositsAmount;
		result.totalDepositsCount = totalDepositsCount;
		result.DepositAverage = DepositAverage;
		result.totalADB = totalADB;
		result.totalNegativeDays = totalNegativeDays;
		result.totalNSF = totalNSF;
		result.beginningBalance = beginningBalance;
		result.endingBalance = endingBalance;
		result.ADBPercentage = ADBPercentage;
		result.IncDec = IncDec;
		return result;
	}
	function GetLastNMonthGridReport(gridData) {
		var fixedGridRowHeader = ['AVG', 'MIN', 'MAX'];
		var objGridDataNMonth = [];
		var groupData = gridData.filter(function (data) {
			return (data.GridRowOrder > 0);
		});
		if (groupData != null && groupData.length > 0) {
			fixedGridRowHeader.forEach(function (objRow) {
				var groupReport = GetGridReportFormat();
				groupReport.GridRowHeader = objRow;
				var AVGData = GetTotalForGridReport(groupData, objRow);
				if (objRow == 'AVG') {
					groupReport.ColumnValue.Deposits = AVGData.totalDepositsAmount > 0 ? FinicitycustomRound(AVGData.totalDepositsAmount / groupData.length, 2) : 0.00;
					groupReport.ColumnValue.DepositCount = AVGData.totalDepositsCount > 0 ? FinicitycustomRound(AVGData.totalDepositsCount / groupData.length, 2) : 0;
					groupReport.ColumnValue.DepositAverage = AVGData.DepositAverage ? FinicitycustomRound(AVGData.DepositAverage / groupData.length, 2) : 0.00;
					groupReport.ColumnValue.ADB = AVGData.totalADB > 0 ? FinicitycustomRound(AVGData.totalADB / groupData.length, 2) : 0.00;
					groupReport.ColumnValue.NegativeDays = AVGData.totalNegativeDays > 0 ? FinicitycustomRound(AVGData.totalNegativeDays / groupData.length, 2) : 0;
					groupReport.ColumnValue.NSF = AVGData.totalNSF > 0 ? FinicitycustomRound(AVGData.totalNSF / groupData.length, 2) : 0;
					groupReport.ColumnValue.BeginningBalance = AVGData.beginningBalance > 0 ? FinicitycustomRound(AVGData.beginningBalance / groupData.length, 2) : 0;
					groupReport.ColumnValue.EndingBalance = AVGData.endingBalance > 0 ? FinicitycustomRound(AVGData.endingBalance / groupData.length, 2) : 0;
					groupReport.ColumnValue.ADBPercentage = AVGData.ADBPercentage > 0 ? FinicitycustomRound(AVGData.ADBPercentage / groupData.length, 2) : 0;
					groupReport.ColumnValue.IncDec = AVGData.IncDec > 0 ? FinicitycustomRound(AVGData.IncDec / groupData.length, 2) : 0;
					groupReport.ColumnValue.GridRowOrder = 1;
				} else {
					groupReport.ColumnValue.Deposits = FinicitycustomRound(AVGData.totalDepositsAmount, 2);
					groupReport.ColumnValue.DepositCount = FinicitycustomRound(AVGData.totalDepositsCount, 2);
					groupReport.ColumnValue.DepositAverage = FinicitycustomRound(AVGData.DepositAverage, 2);
					groupReport.ColumnValue.ADB = FinicitycustomRound(AVGData.totalADB, 2);
					groupReport.ColumnValue.NegativeDays = FinicitycustomRound(AVGData.totalNegativeDays, 2);
					groupReport.ColumnValue.NSF = FinicitycustomRound(AVGData.totalNSF, 2);
					groupReport.ColumnValue.BeginningBalance = FinicitycustomRound(AVGData.beginningBalance, 2);
					groupReport.ColumnValue.EndingBalance = FinicitycustomRound(AVGData.endingBalance, 2);
					groupReport.ColumnValue.ADBPercentage = FinicitycustomRound(AVGData.ADBPercentage, 2);
					groupReport.ColumnValue.IncDec = FinicitycustomRound(AVGData.IncDec, 2);
				}
				objGridDataNMonth.push(groupReport);
			});
		}
		return objGridDataNMonth;
	}
	function GetGridReport(monthlyReportList, accountName, accountType, accountNumber) {
		var objGridReport = {
			'AccountHolderName': '',
			'AccountType': '',
			'PDFReportName': '',
			'ReportList': [{
				'ReportHeader': '',
				'GridData': [{}
				]
			}
			]
		};
		var objGridDataAll = [];
		var MTDComparisionReport = {};
		var MTDPercentageReport = {};
		var MTDMonthFlag = 6;
		var monthlyReportGroup = [4, 7, 13];
		var reportList = [];
		var currentDate = new Date();
		var currentMonth = Finicitymonths[currentDate.getMonth()];
		var currentYear = currentDate.getFullYear();
		var skipMonth = currentMonth + '-' + currentYear;
		var monthCounter = 0;
		if (monthlyReportList !== undefined && monthlyReportList !== null && monthlyReportList.length > 0) {
			monthlyReportList.forEach(function (objMonthSummary) {
				if (objMonthSummary !== undefined) {
					var currentMonthReport = GetGridReportFormat();
					var gridNamePreFix = '';
					if ((objMonthSummary.Name + '-' + objMonthSummary.Year) == skipMonth) {
						gridNamePreFix = 'MTD ';
						currentMonthReport.GridRowOrder = monthCounter;
					}
					else {
						currentMonthReport.GridRowOrder = monthCounter + 2;
					}
					currentMonthReport.GridRowHeader = gridNamePreFix + objMonthSummary.Name + ' ' + objMonthSummary.Year;
					currentMonthReport.ColumnValue.Deposits = FinicitycustomRound(objMonthSummary.TotalDepositAmount, 2);
					currentMonthReport.ColumnValue.DepositCount = objMonthSummary.DepositCount;
					currentMonthReport.ColumnValue.BeginningBalance = FinicitycustomRound(objMonthSummary.BeginingBalance, 2);
					currentMonthReport.ColumnValue.EndingBalance = FinicitycustomRound(objMonthSummary.EndingBalance, 2);
					currentMonthReport.ColumnValue.IncDec = FinicitycustomRound(objMonthSummary.EndingBalance - objMonthSummary.BeginingBalance, 2);
					currentMonthReport.ColumnValue.ADB = FinicitycustomRound(objMonthSummary.AverageDailyBalance, 2);
					currentMonthReport.ColumnValue.NegativeDays = objMonthSummary.NumberOfNegativeBalance;
					currentMonthReport.ColumnValue.NSF = objMonthSummary.NumberOfNSF;
					currentMonthReport.ColumnValue.Name = objMonthSummary.Name;
					currentMonthReport.ColumnValue.Year = objMonthSummary.Year;
					var ADBPercentage = 0;
					var DepositAverage = 0;
					if (objMonthSummary.TotalDepositAmount != null && objMonthSummary.TotalDepositAmount != undefined && objMonthSummary.TotalDepositAmount > 0) {
						ADBPercentage = (objMonthSummary.AverageDailyBalance * 100) / objMonthSummary.TotalDepositAmount;
						DepositAverage = objMonthSummary.TotalDepositAmount / objMonthSummary.DepositCount;
					}
					currentMonthReport.ColumnValue.DepositAverage = FinicitycustomRound(DepositAverage, 2);
					currentMonthReport.ColumnValue.ADBPercentage = FinicitycustomRound(ADBPercentage, 2);
					objGridDataAll.push(currentMonthReport);
					monthCounter = monthCounter + 1;
					if (monthCounter === MTDMonthFlag) {
						var MTDData = objGridDataAll.filter(function (data) {
							return (data.GridRowOrder == 0);
						});
						if (MTDData != null && MTDData != undefined && MTDData.length > 0) {
							MTDData = MTDData[0];
						}
						else {
							MTDData = null;
						}
						var MTDToCompareData = objGridDataAll.filter(function (data) {
							return (data.GridRowOrder > 0);
						});
						var MTDTotal = GetTotalForGridReport(MTDToCompareData, 'AVG');
						MTDComparisionReport = GetGridReportFormat();
						MTDPercentageReport = GetGridReportFormat();
						MTDComparisionReport.GridRowHeader = 'MTD COMPARISION';
						MTDComparisionReport.ColumnValue.Deposits = MTDTotal.totalDepositsAmount > 0 ? FinicitycustomRound(MTDTotal.totalDepositsAmount / MTDToCompareData.length, 2) : 0.00;
						MTDComparisionReport.ColumnValue.DepositCount = MTDTotal.totalDepositsCount > 0 ? FinicitycustomRound(MTDTotal.totalDepositsCount / MTDToCompareData.length, 2) : 0;
						MTDComparisionReport.ColumnValue.DepositAverage = MTDTotal.totalDepositsAmount ? FinicitycustomRound(MTDTotal.totalDepositsAmount / MTDTotal.totalDepositsCount, 2) : 0.00;
						MTDComparisionReport.ColumnValue.ADB = MTDTotal.totalADB > 0 ? FinicitycustomRound(MTDTotal.totalADB / MTDToCompareData.length, 2) : 0.00;
						MTDComparisionReport.ColumnValue.NegativeDays = MTDTotal.totalNegativeDays > 0 ? FinicitycustomRound(MTDTotal.totalNegativeDays / MTDToCompareData.length, 2) : 0;
						MTDComparisionReport.ColumnValue.NSF = MTDTotal.totalNSF > 0 ? FinicitycustomRound(MTDTotal.totalNSF / MTDToCompareData.length, 2) : 0;
						MTDComparisionReport.ColumnValue.BeginningBalance = MTDTotal.beginningBalance > 0 ? FinicitycustomRound(MTDTotal.beginningBalance / MTDToCompareData.length, 2) : 0;
						MTDComparisionReport.ColumnValue.EndingBalance = MTDTotal.endingBalance > 0 ? FinicitycustomRound(MTDTotal.endingBalance / MTDToCompareData.length, 2) : 0;
						MTDComparisionReport.ColumnValue.IncDec = MTDTotal.IncDec > 0 ? FinicitycustomRound(MTDTotal.IncDec / MTDToCompareData.length, 2) : 0;
						MTDComparisionReport.ColumnValue.ADBPercentage = MTDTotal.ADBPercentage > 0 ? FinicitycustomRound(MTDTotal.ADBPercentage / MTDToCompareData.length, 2) : 0;
						MTDComparisionReport.ColumnValue.BeginningBalance = MTDTotal.beginningBalance > 0 ? FinicitycustomRound(MTDTotal.beginningBalance / MTDToCompareData.length, 2) : 0;
						MTDComparisionReport.ColumnValue.EndingBalance = MTDTotal.endingBalance > 0 ? FinicitycustomRound(MTDTotal.endingBalance / MTDToCompareData.length, 2) : 0;
						MTDComparisionReport.ColumnValue.IncDec = MTDTotal.IncDec > 0 ? FinicitycustomRound(MTDTotal.IncDec / MTDToCompareData.length, 2) : 0;
						MTDComparisionReport.ColumnValue.ADBPercentage = MTDTotal.ADBPercentage > 0 ? FinicitycustomRound(MTDTotal.ADBPercentage / MTDToCompareData.length, 2) : 0;

						MTDComparisionReport.GridRowOrder = 1;
						MTDPercentageReport.GridRowHeader = 'MTD COMPARISION %';
						if (MTDData != null && MTDData != undefined) {
							MTDPercentageReport.ColumnValue.Deposits = MTDTotal.totalDepositsAmount > 0 && MTDComparisionReport.ColumnValue.Deposits > 0 ? FinicitycustomRound((MTDData.ColumnValue.Deposits * 100) / MTDComparisionReport.ColumnValue.Deposits, 2) : 0.00;
							MTDPercentageReport.ColumnValue.DepositCount = MTDTotal.totalDepositsCount > 0 && MTDComparisionReport.ColumnValue.DepositCount > 0 ? FinicitycustomRound((MTDData.ColumnValue.DepositCount * 100) / MTDComparisionReport.ColumnValue.DepositCount, 2) : 0;
							MTDPercentageReport.ColumnValue.DepositAverage = MTDTotal.totalDepositsCount > 0 && MTDComparisionReport.ColumnValue.DepositAverage > 0 ? FinicitycustomRound((MTDData.ColumnValue.DepositAverage * 100) / MTDComparisionReport.ColumnValue.DepositAverage, 2) : 0.00;
							MTDPercentageReport.ColumnValue.ADB = MTDTotal.totalADB > 0 && MTDComparisionReport.ColumnValue.ADB > 0 ? FinicitycustomRound((MTDData.ColumnValue.ADB * 100) / MTDComparisionReport.ColumnValue.ADB, 2) : 0.00;
							MTDPercentageReport.ColumnValue.NegativeDays = MTDTotal.totalNegativeDays > 0 && MTDComparisionReport.ColumnValue.NegativeDays > 0 ? FinicitycustomRound((MTDData.ColumnValue.NegativeDays * 100) / MTDComparisionReport.ColumnValue.NegativeDays, 2) : 0;
							MTDPercentageReport.ColumnValue.NSF = MTDTotal.totalNSF > 0 && MTDComparisionReport.ColumnValue.NSF > 0 ? FinicitycustomRound((MTDData.ColumnValue.NSF * 100) / MTDComparisionReport.ColumnValue.NSF, 2) : 0;
							MTDPercentageReport.ColumnValue.NSF = MTDTotal.totalNSF > 0 && MTDComparisionReport.ColumnValue.NSF > 0 ? FinicitycustomRound((MTDData.ColumnValue.NSF * 100) / MTDComparisionReport.ColumnValue.NSF, 2) : 0;
							MTDPercentageReport.ColumnValue.NSF = MTDTotal.totalNSF > 0 && MTDComparisionReport.ColumnValue.NSF > 0 ? FinicitycustomRound((MTDData.ColumnValue.NSF * 100) / MTDComparisionReport.ColumnValue.NSF, 2) : 0;
							MTDPercentageReport.ColumnValue.NSF = MTDTotal.totalNSF > 0 && MTDComparisionReport.ColumnValue.NSF > 0 ? FinicitycustomRound((MTDData.ColumnValue.NSF * 100) / MTDComparisionReport.ColumnValue.NSF, 2) : 0;

							MTDPercentageReport.ColumnValue.BeginningBalance = MTDTotal.beginningBalance > 0 && MTDComparisionReport.ColumnValue.NSF > 0 ? FinicitycustomRound((MTDData.ColumnValue.BeginningBalance * 100) / MTDComparisionReport.ColumnValue.BeginningBalance, 2) : 0;
							MTDPercentageReport.ColumnValue.EndingBalance = MTDTotal.endingBalance > 0 && MTDComparisionReport.ColumnValue.EndingBalance > 0 ? FinicitycustomRound((MTDData.ColumnValue.EndingBalance * 100) / MTDComparisionReport.ColumnValue.EndingBalance, 2) : 0;
							MTDPercentageReport.ColumnValue.IncDec = MTDTotal.IncDec > 0 && MTDComparisionReport.ColumnValue.IncDec > 0 ? FinicitycustomRound((MTDData.ColumnValue.IncDec * 100) / MTDComparisionReport.ColumnValue.IncDec, 2) : 0;
							MTDPercentageReport.ColumnValue.ADBPercentage = MTDTotal.ADBPercentage > 0 && MTDComparisionReport.ColumnValue.ADBPercentage > 0 ? FinicitycustomRound((MTDData.ColumnValue.ADBPercentage * 100) / MTDComparisionReport.ColumnValue.ADBPercentage, 2) : 0;
						}
						MTDPercentageReport.GridRowOrder = 2;
					}
					if (monthlyReportGroup.indexOf(monthCounter) > -1) {
						var reportItem = {
							'ReportHeader': '',
							'GridData': [{}
							],
							'GridRowOrder': 1
						};
						reportItem.ReportHeader = 'Last ' + (monthCounter - 1).toString() + ' Months';
						reportItem.GridData = GetLastNMonthGridReport(objGridDataAll);
						reportList.push(reportItem);
					}
				}
			});
		}
		objGridDataAll.push(MTDComparisionReport);
		objGridDataAll.push(MTDPercentageReport);
		objGridReport.AccountHolderName = accountName;
		objGridReport.AccountType = accountType;
		objGridReport.PDFReportName = accountNumber + '_Monthly_Grid.pdf';
		objGridDataAll = objGridDataAll.sort((a, b) => {
			return a.GridRowOrder - b.GridRowOrder;
		});
		var reportItem = {
			'ReportHeader': '',
			'GridData': [{}
			],
			'GridRowOrder': 0
		};
		reportItem.ReportHeader = accountName;
		reportItem.GridData = objGridDataAll;
		reportList.push(reportItem);
		reportList = reportList.sort((a, b) => {
			return a.GridRowOrder - b.GridRowOrder;
		});
		objGridReport.ReportList = reportList;
		return objGridReport;
	}
}