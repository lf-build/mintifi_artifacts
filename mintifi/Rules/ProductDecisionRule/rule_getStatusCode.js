function rule_getStatusCode(WorkflowName) {
	var statusList = [{
			'Flow': 'applicationapprovalflow',
			'Status': '500.80'
		}, {
			'Flow': 'renewalapplicationapprovalflow',
			'Status': '800.90'
		}, {
			'Flow': 'locapprovalflow',
			'Status': '700.60'
		}, {
			'Flow': 'renewallocapprovalflow',
			'Status': '900.80'
		}
	];
	if (WorkflowName != null && typeof(WorkflowName) != 'undefined') {
		var objResult = statusList.find(o => o.Flow.toLowerCase() == WorkflowName.toLowerCase());
		var statusCode = objResult.Status;
		return statusCode;
	}
}