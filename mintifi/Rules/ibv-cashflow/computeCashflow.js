function ComputeCashflow(payload) {
	var moment = require('moment-timezone');
	var linksource = '';
	var startDate = '';
	var endDate = '';
	if (typeof (payload) != 'undefined') {
		var inputData = payload.eventData;
		linksource = inputData.accounts.source.toLowerCase();
		startDate = payload.FromDate;
		endDate = payload.toDate;
	}
	var self = this;
	var baseUrl = 'http://instantbankverification:5000';
	var cashflowService = self.call('cashflowibv');
	var Plaidmonths = cashflowService.getPlaidMonths();
	var result = 'Passed';
	if (linksource == 'finicity') {
		var Category_Payroll = cashflowService.getFinicityCategories('payroll');
		var Category_LoanPayment = cashflowService.getFinicityCategories('loanpayment');
		var Category_NSF = cashflowService.getFinicityCategories('nsf');
		var Category_SkipDeposit = cashflowService.getFinicityCategories('skipdeposits');
		var Category_Revenue = cashflowService.getFinicityCategories('revenue');
		var allowedCategory = cashflowService.getFinicityCategories('allowedcategories');
	}
	else {
		var Category_Payroll = cashflowService.getPlaidCategories('payroll');
		var Category_LoanPayment = cashflowService.getPlaidCategories('loanpayment');
		var Category_NSF = cashflowService.getPlaidCategories('nsf');
		var Category_SkipDeposit = cashflowService.getPlaidCategories('skipdeposits');
		var Category_Revenue = cashflowService.getPlaidCategories('revenue');
		var allowedCategory = cashflowService.getPlaidCategories('allowedcategories');
	}

	var endingBalance = 0;
	var beginingBalance = 0;
	var StartDate;
	var EndDate;
	var allAccountsCashflows = {};
	var ExcludeWeekEnds = false;
	var addMissingMonths = true;
	var calculateRecurringTransaction = true;
	if (typeof (payload) != 'undefined') {
		var entityId = payload.entityId;
		var entityType = payload.entityType;
		var cashFlowInput = payload.eventData;
		var filter = payload.filterType;
		var account = cashFlowInput.accounts;
		var fromDate = payload.fromDate;
		var toDate = payload.toDate;
		return cashflowService.getTransactionsResult(baseUrl, entityType, entityId, account.id, fromDate, toDate).then(function (allTrans) {
			var AllTransactions = allTrans.eventData.transactions;
			var myCustomCashFlow = cashflowService.GetMyCashflowModel();
			var PlaidCashFlowViewModel = cashflowService.GetPlaidCashFlowViewModel();
			var cashFlowResult = PlaidCashFlowViewModel;
			endingBalance = account.currentBalance;
			beginingBalance = endingBalance;
			var SelectedTransactions = [];
			var MonthlyCashFlowsList = [];
			var CategorySummaryList = [];
			var TransactionList = [];
			var RecurringList = [];
			var MCARecurringList = [];
			var LastFourDigitAccountNumber = account.accountNumber;
			if (account.accountNumber != null) {
				LastFourDigitAccountNumber = account.accountNumber.substr(-4);
				LastFourDigitAccountNumber = LastFourDigitAccountNumber;
			}
			if (AllTransactions != null && AllTransactions.length > 0) {
				SelectedTransactions = cashflowService.filterAndSortDescending(AllTransactions, account.providerAccountId);
				if (SelectedTransactions != null && SelectedTransactions.length > 0) {
					var totalTransactions = SelectedTransactions.length;
					if (calculateRecurringTransaction == true) {
						GetRecurringSummary(account.id, SelectedTransactions, function (trs) {
							RecurringList = trs;
						});
						GetRecurringSummaryByCategoryId(account.id, SelectedTransactions, linksource, function (trs) {
							MCARecurringList = trs;
						});
					}
					cashflowService.GetCategorySummary(SelectedTransactions, linksource, function (s) {
						CategorySummaryList = s;
					});
					EndDate = SelectedTransactions[0].transactionDate;
					StartDate = SelectedTransactions[totalTransactions - 1].transactionDate;
					var maxTransactionDate = new Date(EndDate);
					var currentDate = new Date();
					var currentMonth = currentDate.getMonth();
					var currentYear = currentDate.getFullYear();
					var lastMonth = currentMonth + '-' + currentYear;
					var minTransactionDate = new Date(StartDate);
					var firstMonth = minTransactionDate.getMonth() + '-' + minTransactionDate.getFullYear();
					var isFirstTransaction = true;
					var isLastMonth = true;
					var dateOfLastTransaction;
					var TotalDailyBalanceArray = new Array();
					TotalDailyBalanceArray.push(beginingBalance);
					var MonthlyTotalDailyBalanceArray = new Array();
					MonthlyTotalDailyBalanceArray.push(beginingBalance);
					var MonthlyTotalDailyDepositArray = new Array();
					while (SelectedTransactions.length > 0) {
						var currentMonthTransactions = cashflowService.filterCurrentMonthTransaction(SelectedTransactions, currentMonth, currentYear);
						if (addMissingMonths == true) {
							if ((currentMonthTransactions == null || currentMonthTransactions == undefined) || (currentMonthTransactions != null && currentMonthTransactions != undefined && currentMonthTransactions.length == 0)) {
								var tempTransaction = cashflowService.GetDummyTransactionRecord(currentMonth, currentYear);
								currentMonthTransactions = [];
								currentMonthTransactions.push(tempTransaction);
							}
						}
						if (currentMonthTransactions != null && currentMonthTransactions != undefined && currentMonthTransactions.length > 0) {
							var currentMonthCashFlow = cashflowService.GetPlaidMonthlyCashFlowViewModel();
							var monthlyDailyBalance = new Array(31);
							var MinDepositAmount = 0,
								MaxDepositAmount = 0,
								MinWithdrawalAmount = 0,
								MaxWithdrawalAmount = 0;
							var DepositCount = 0,
								WithdrawalCount = 0,
								TotalMonthlyDepositAmount = 0,
								TotalMonthlyWithdrawalAmount = 0;
							var TotalMonthlyLoanPaymentAmount = 0,
								TotalMonthlyLoanPaymentCount = 0;
							var TotalMonthlyPayrollAmount = 0,
								TotalMonthlyPayrollCount = 0;
							var TotalMonthlyNSFAmount = 0,
								TotalMonthlyNSFCount = 0;
							var TotalMonthlyRevenueAmount = 0,
								TotalMonthlyRevenueCount = 0;
							currentMonthCashFlow.Name = Plaidmonths[currentMonth];
							currentMonthCashFlow.Year = currentYear;
							var monthInNumber = (parseInt(currentMonth) + 1).toString();
							monthInNumber = monthInNumber.length > 1 ? monthInNumber : '0' + monthInNumber;
							currentMonthCashFlow.PDFReportName = LastFourDigitAccountNumber + '-' + monthInNumber + '-' + currentYear + '.pdf';
							currentMonthCashFlow.EndingBalance = cashflowService.PlaidcustomRound(beginingBalance, 2);
							var index = -1;
							currentMonthTransactions.forEach(function (objTransaction) {
								if (objTransaction != undefined) {
									if (objTransaction.categoryId != undefined) {
										if (objTransaction.categoryId == Category_Payroll) {
											TotalMonthlyPayrollAmount = TotalMonthlyPayrollAmount + objTransaction.amount;
											TotalMonthlyPayrollCount = TotalMonthlyPayrollCount + 1;
											currentMonthCashFlow.PayrollAmount = Math.abs(TotalMonthlyPayrollAmount);
											currentMonthCashFlow.NumberOfPayroll = TotalMonthlyPayrollCount;
										} else if (objTransaction.categoryId == Category_NSF) {
											TotalMonthlyNSFAmount = TotalMonthlyNSFAmount + objTransaction.amount;
											TotalMonthlyNSFCount = TotalMonthlyNSFCount + 1;
											currentMonthCashFlow.NSFAmount = TotalMonthlyNSFAmount;
											currentMonthCashFlow.NumberOfNSF = TotalMonthlyNSFCount;
										} else if (objTransaction.categoryId == Category_LoanPayment) {
											TotalMonthlyLoanPaymentAmount = TotalMonthlyLoanPaymentAmount + objTransaction.amount;
											TotalMonthlyLoanPaymentCount = TotalMonthlyLoanPaymentCount + 1;
											currentMonthCashFlow.LoanPaymentAmount = TotalMonthlyLoanPaymentAmount;
											currentMonthCashFlow.NumberOfLoanPayment = TotalMonthlyLoanPaymentCount;
										} else if (Category_Revenue.indexOf(objTransaction.categoryId) > -1) {
											TotalMonthlyRevenueAmount = TotalMonthlyRevenueAmount + objTransaction.amount;
											TotalMonthlyRevenueCount = TotalMonthlyRevenueCount + 1;
											currentMonthCashFlow.TotalMonthlyRevenueAmount = Math.abs(TotalMonthlyRevenueAmount);
											currentMonthCashFlow.TotalMonthlyRevenueCount = TotalMonthlyRevenueCount;
										}
									}
									var currentTransactionDate = new Date(objTransaction.transactionDate);
									var day = currentTransactionDate.getDate();
									var calculateDailyEnding = isFirstTransaction ? isFirstTransaction : (cashflowService.GetFormattedDate(dateOfLastTransaction) != cashflowService.GetFormattedDate(currentTransactionDate));
									if (calculateDailyEnding) {
										endingBalance = beginingBalance;
										monthlyDailyBalance[day - 1] = endingBalance;
									}
									dateOfLastTransaction = currentTransactionDate;
									var transactionAmount = objTransaction.amount;
									if (transactionAmount < 0) {
										beginingBalance -= Math.abs(transactionAmount);
										MonthlyTotalDailyDepositArray.push(Math.abs(transactionAmount));
										if (transactionAmount < MaxDepositAmount || MaxDepositAmount == 0) {
											MaxDepositAmount = transactionAmount;
										}
										if (transactionAmount > MinDepositAmount || MinDepositAmount == 0) {
											MinDepositAmount = transactionAmount;
										}
										DepositCount = DepositCount + 1;
										TotalMonthlyDepositAmount = TotalMonthlyDepositAmount + transactionAmount;
									} else if (transactionAmount > 0) {
										beginingBalance += transactionAmount;
										if (transactionAmount > MaxWithdrawalAmount || MaxWithdrawalAmount == 0) {
											MaxWithdrawalAmount = transactionAmount;
										}
										if (transactionAmount < MinWithdrawalAmount || MinWithdrawalAmount == 0) {
											MinWithdrawalAmount = transactionAmount;
										}
										WithdrawalCount = WithdrawalCount + 1;
										TotalMonthlyWithdrawalAmount = TotalMonthlyWithdrawalAmount + transactionAmount;
									}
									isFirstTransaction = false;
									index = -1;
									index = SelectedTransactions.indexOf(objTransaction, 0);
									if (index > -1) {
										SelectedTransactions.splice(index, 1);
									}
									if (totalTransactions > 1 && index > -1) {
										TotalDailyBalanceArray.push(beginingBalance);
										MonthlyTotalDailyBalanceArray.push(beginingBalance);
									}
								}
								if (index > -1) {
									totalTransactions -= 1;
								}
							});
							currentMonthCashFlow.TotalWithdrawalAmount = cashflowService.PlaidcustomRound(TotalMonthlyWithdrawalAmount, 2);
							currentMonthCashFlow.WithdrawalCount = WithdrawalCount;
							currentMonthCashFlow.TotalDepositAmount = cashflowService.PlaidcustomRound(Math.abs(TotalMonthlyDepositAmount), 2);
							currentMonthCashFlow.DepositCount = DepositCount;
							currentMonthCashFlow.MinDepositAmount = Math.abs(MinDepositAmount);
							currentMonthCashFlow.MaxDepositAmount = Math.abs(MaxDepositAmount);
							currentMonthCashFlow.MinWithdrawalAmount = MinWithdrawalAmount;
							currentMonthCashFlow.MaxWithdrawalAmount = MaxWithdrawalAmount;
							currentMonthCashFlow.MonthlyTotalDailyBalanceArray = MonthlyTotalDailyBalanceArray;
							currentMonthCashFlow.MonthlyTotalDailyDepositArray = MonthlyTotalDailyDepositArray;
							MonthlyTotalDailyBalanceArray = new Array();
							MonthlyTotalDailyDepositArray = new Array();
							if (currentMonthTransactions[0].IsDummyRecord == undefined) {
								currentMonthCashFlow.FirstTransactionDate = cashflowService.PlaidcustomDate(dateOfLastTransaction);
								if (currentMonthTransactions.length > 0) {
									currentMonthCashFlow.EndTransactionDate = cashflowService.PlaidcustomDate(new Date(currentMonthTransactions[0].transactionDate));
								}
							}
							currentMonthCashFlow.BeginingBalance = cashflowService.PlaidcustomRound(beginingBalance, 2);
							var totalDaysInmonth = 0;
							totalDaysInmonth = cashflowService.plaidGetDaysInMonth(dateOfLastTransaction.getMonth(), dateOfLastTransaction.getFullYear(), ExcludeWeekEnds);
							var monthCheck = currentMonth + '-' + currentYear;
							var MonthlyResponse = {};
							if (firstMonth == lastMonth) {
								MonthlyResponse = cashflowService.CalculateADBFirstMonth(monthlyDailyBalance, totalDaysInmonth);
							} else if (lastMonth == monthCheck) {
								MonthlyResponse = cashflowService.CalculateADB(monthlyDailyBalance, totalDaysInmonth, TotalDailyBalanceArray);
							} else if (firstMonth == monthCheck) {
								MonthlyResponse = cashflowService.CalculateADBFirstMonth(monthlyDailyBalance, totalDaysInmonth);
							} else {
								MonthlyResponse = cashflowService.CalculateADB(monthlyDailyBalance, totalDaysInmonth, TotalDailyBalanceArray);
							}

							currentMonthCashFlow.TotalDailyBalance = MonthlyResponse.TotalDailyBalance;
							currentMonthCashFlow.AverageDailyBalance = MonthlyResponse.ADB;
							currentMonthCashFlow.NumberOfNegativeBalance = cashflowService.PlaidcustomRound(MonthlyResponse.NegativeDaysCount, 2);
							currentMonthCashFlow.DaysBelow100Count = cashflowService.GetDaysBelow100Count(monthlyDailyBalance);

							MonthlyCashFlowsList.push(cashflowService.PlaidsortObject(currentMonthCashFlow));
							isLastMonth = false;
						}
						currentMonth -= 1;
						if (currentMonth < 0) {
							currentYear -= 1;
							currentMonth = 11;
						}
					}
					var t = myCustomCashFlow;
					t.MonthlyCashFlows = MonthlyCashFlowsList;
					t.CategorySummary = CategorySummaryList;
					t.RecurringList = RecurringList;
					t.MCARecurringList = MCARecurringList;
					t.GridReport = GetGridReport(MonthlyCashFlowsList, account.accountType, account.accountType, account.accountNumber);
					cashFlowResult.CashFlow = t;
				} else {
					var t = myCustomCashFlow;
					cashFlowResult.CashFlow = t;
				}
			}
			else {
				var dateStart = moment(fromDate);
				var dateEnd = moment(toDate);
				var timeValues = [];
				
				while (dateEnd > dateStart || dateStart.format('M') === dateEnd.format('M')) {
					timeValues.push(dateStart.format('MMMM-YYYY'));
					dateStart.add(1, 'month');
				}
				myCustomCashFlow.MonthlyCashFlows = [];
				timeValues.forEach(function (monthVal) {
					//myCustomCashFlow.MonthlyCashFlows = [];
					var currentMonthCashFlow = cashflowService.GetPlaidMonthlyCashFlowViewModel();
					
					currentMonthCashFlow.Name = monthVal.split('-')[0];
					currentMonthCashFlow.Year = monthVal.split('-')[1];
					
					myCustomCashFlow.MonthlyCashFlows.push(cashflowService.PlaidsortObject(currentMonthCashFlow));
					myCustomCashFlow.GridReport = GetGridReport(myCustomCashFlow.MonthlyCashFlows, account.accountType, account.accountType, account.accountNumber);
				});
				var t = myCustomCashFlow;
				cashFlowResult.CashFlow = t;
			}
			var formattedAccountNumber = account.bankName;
			cashFlowResult.InstitutionName = account.bankName;
			cashFlowResult.referenceNumber = payload.eventData.referenceNumber;
			cashFlowResult.AccountID = account.id;
			cashFlowResult.AccountType = account.accountType;
			cashFlowResult.AccountHeader = formattedAccountNumber + '-' + account.accountNumber + '-' + account.accountType;
			cashFlowResult.AccountNumber = account.accountNumber;
			cashFlowResult.Source = account.source;
			cashFlowResult.NameOnAccount = account.nameOnAccount;
			cashFlowResult.OfficialAccountName = account.officialAccountName;
			allAccountsCashflows = cashFlowResult;
			var Data = allAccountsCashflows;
			return {
				'result': result,
				'detail': null,
				'data': Data,
				'rejectcode': '',
				'exception': []
			};
		});
	} else {
		return {
			'result': 'Failed',
			'detail': null,
			'data': null,
			'rejectcode': '',
			'exception': []
		};
	}
	function FilterOutlier(trans, handleSuccess) {
		handleSuccess(trans);
	}
	function GetRecurringSummaryByCategoryId(accountId, trans, linksource, handleSuccess) {
		var recurringSummary = [];
		if (linksource == 'finicity') {
			var categoryId = 'Loan Payment';
		} else {
			var categoryId = '21002000';
		}
		var daySpanFlag = 15;
		var unsureFlag = 3;
		if (trans != null && trans.length > 0) {
			var allDebitTransactions = trans.filter(function (item) {
				return item.amount > 0 && item.categoryId == categoryId && cashflowService.GetDaysBetweenDate(item.transactionDate) <= daySpanFlag;
			});
			var moduloGroups = cashflowService.GetGroupByData(allDebitTransactions, 'amount');
			if (moduloGroups != null) {
				for (var property in moduloGroups) {
					if (moduloGroups[property].constructor === Array) {
						var grp = moduloGroups[property];
						if (grp != null && grp != undefined && grp.length < unsureFlag) { }
						else {
							var moduloSubGroups = cashflowService.GetGroupByData(grp, 'description');
							if (moduloSubGroups != null) {
								for (var p in moduloSubGroups) {
									if (moduloSubGroups[p].constructor === Array) {
										var grpSub = moduloSubGroups[p];
										if (grpSub != null && grpSub != undefined && grpSub.length >= unsureFlag) {
											var allMatchingTransactions = [];
											GetTransactionList(grpSub, function (trs) {
												allMatchingTransactions = trs;
											});
											var temp = {
												'Account': accountId,
												'Amount': grpSub[0].amount,
												'RoudingAmount': grpSub[0].amount,
												'Transactions': allMatchingTransactions,
												'ConfidenceLevel': 'High',
												'TotalTransactions': grpSub.length,
												'IsRecurring': true,
												'Status': 'RECURRING'
											};
											recurringSummary.push(temp);
										}
									}
								}
							}
						}
					}
				}
			}
		}
		handleSuccess(recurringSummary);
	}
	function GetRecurringSummary(accountId, trans, handleSuccess) {
		var recurringSummary = [];
		var moduloRange = 5;
		var roundingMethodType = 2;
		var unsureFlag = 3;
		var minTransactionFlag = 5;
		var dayDifferenceFlag = 7;
		if (trans != null && trans.length > 0) {
			var allDebitTransactions = trans.filter(function (item) {
				return item.amount > 0;
			});
			for (var i = 0; i < allDebitTransactions.length; i++) {
				var rNumber = cashflowService.GetRoundingAmount(allDebitTransactions[i].amount, roundingMethodType);
				allDebitTransactions[i].RoudingAmount = rNumber;
				allDebitTransactions[i].RoudingAmountModulus = rNumber % moduloRange;
			}
			var moduloGroups = cashflowService.GetGroupByData(allDebitTransactions, 'RoudingAmountModulus');
			if (moduloGroups != null) {
				for (var property in moduloGroups) {
					if (moduloGroups[property].constructor === Array) {
						var grp = moduloGroups[property];
						if (grp != null && grp != undefined && grp.length <= unsureFlag) { }
						else {
							var moduloSubGroups = cashflowService.GetGroupByData(grp, 'RoudingAmount');
							if (moduloSubGroups != null) {
								for (var p in moduloSubGroups) {
									if (moduloSubGroups[p].constructor === Array) {
										var grpSub = moduloSubGroups[p];
										if (grpSub != null && grpSub != undefined && grpSub.length >= minTransactionFlag) {
											FilterOutlier(grpSub, function (d) {
												grpSub = d;
											});
											grpSub.sort((a, b) => {
												var start = +new Date(a.transactionDate);
												var elapsed = +new Date(b.transactionDate) - start;
												return elapsed;
											});
											for (var i = 0; i < grpSub.length; i++) {
												if (grpSub[i + 1] != undefined) {
													var diffDays = cashflowService.GetDaysBetweenTransaction(grpSub[i].transactionDate, grpSub[i + 1].transactionDate);
													grpSub[i].diffDays = diffDays;
												}
											}
											var MinValue = cashflowService.GetMinMaxValue(grpSub, 'min');
											var MaxValue = cashflowService.GetMinMaxValue(grpSub, 'max');
											var rangeDifference = MaxValue - MinValue;
											if (rangeDifference > 0) {
												if (rangeDifference <= dayDifferenceFlag) {
													var allMatchingTransactions = [];
													GetTransactionList(grpSub, function (trs) {
														allMatchingTransactions = trs;
													});
													var temp = {
														'Account': accountId,
														'Amount': grpSub[0].amount,
														'RoudingAmount': grpSub[0].RoudingAmount,
														'Transactions': allMatchingTransactions,
														'ConfidenceLevel': 'High',
														'Min': MinValue,
														'Max': MaxValue,
														'MaxMinDiff': rangeDifference,
														'TotalTransactions': grpSub.length,
														'IsRecurring': true,
														'Status': 'RECURRING'
													};
													recurringSummary.push(temp);
												} else { }
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		handleSuccess(recurringSummary);
	}
	function GetTransactionList(trans, handleSuccess) {
		var TransactionList = [];
		if (trans != null) {
			for (var t = 0; t < trans.length; t++) {
				var customDate = cashflowService.getCustomFormattedDate(trans[t].transactionDate);
				var transItem = {
					'Date': customDate.Date,
					'Description': trans[t].description.trim(),
					'Amount': Math.abs(trans[t].amount),
					'TransactionType': trans[t].amount < 0 ? 'Credit' : 'Debit',
					'RunningBalance': 0,
					'Month': customDate.Month,
					'Year': customDate.Year
				};
				TransactionList.push(transItem);
			}
		}
		handleSuccess(TransactionList);
	}

	function GetGridReport(monthlyReportList, accountName, accountType, accountNumber) {
		var objGridReport = {
			'AccountHolderName': '',
			'AccountType': '',
			'PDFReportName': '',
			'ReportList': [{
				'ReportHeader': '',
				'GridData': [{}]
			}
			]
		};
		var objGridDataAll = [];
		var MTDComparisionReport = {};
		var MTDPercentageReport = {};
		var MTDMonthFlag = 6;
		var monthlyReportGroup = [3, 6, 12];
		var reportList = [];
		var currentDate = new Date();
		var currentMonth = Plaidmonths[currentDate.getMonth()];
		var currentYear = currentDate.getFullYear();
		var skipMonth = currentMonth + '-' + currentYear;
		var monthCounter = 0;
		var currentMonthDataExist = false;
		var currentMonthFirstDate = cashflowService.getMonthStartDate(currentDate.getMonth(), currentYear);
		var totalDays = 0;
		var totalDailyBalance = 0;

		if ((monthlyReportList[0].Name + '-' + monthlyReportList[0].Year) == skipMonth) {
			monthlyReportList = monthlyReportList.slice(1);
		}
		if (monthlyReportList !== undefined && monthlyReportList !== null && monthlyReportList.length > 0) {

			monthlyReportList.forEach(function (objMonthSummary) {
				if (objMonthSummary !== undefined) {
					var currentMonthReport = GetGridReportFormat();
					var gridNamePreFix = '';
					if ((objMonthSummary.Name + '-' + objMonthSummary.Year) == skipMonth) {
						currentMonthDataExist = true;
						gridNamePreFix = 'MTD ';
						currentMonthReport.GridRowOrder = monthCounter;
					}
					else {
						currentMonthReport.GridRowOrder = monthCounter + 3;
					}

					currentMonthReport.GridRowHeader = gridNamePreFix + objMonthSummary.Name + ' ' + objMonthSummary.Year;
					currentMonthReport.ColumnValue.Deposits = cashflowService.PlaidcustomRound(objMonthSummary.TotalDepositAmount, 2);
					currentMonthReport.ColumnValue.DepositCount = objMonthSummary.DepositCount;
					currentMonthReport.ColumnValue.BeginningBalance = cashflowService.PlaidcustomRound(objMonthSummary.BeginingBalance, 2);
					currentMonthReport.ColumnValue.EndingBalance = cashflowService.PlaidcustomRound(objMonthSummary.EndingBalance, 2);
					currentMonthReport.ColumnValue.IncDec = cashflowService.PlaidcustomRound(objMonthSummary.EndingBalance - objMonthSummary.BeginingBalance, 2);
					currentMonthReport.ColumnValue.ADB = cashflowService.PlaidcustomRound(objMonthSummary.AverageDailyBalance, 2);
					currentMonthReport.ColumnValue.NegativeDays = objMonthSummary.NumberOfNegativeBalance;
					currentMonthReport.ColumnValue.NSF = objMonthSummary.NumberOfNSF;
					currentMonthReport.ColumnValue.Name = objMonthSummary.Name;
					currentMonthReport.ColumnValue.Year = objMonthSummary.Year;
					currentMonthReport.ColumnValue.LoanPaymentAmount = cashflowService.PlaidcustomRound(objMonthSummary.LoanPaymentAmount, 2);
					currentMonthReport.ColumnValue.LoanWithdrawalAmount = cashflowService.PlaidcustomRound(objMonthSummary.TotalWithdrawalAmount, 2);

					var ADBPercentage = 0;
					var DepositAverage = 0;
					if (objMonthSummary.TotalDepositAmount != null && objMonthSummary.TotalDepositAmount != undefined && objMonthSummary.TotalDepositAmount > 0) {
						ADBPercentage = (objMonthSummary.AverageDailyBalance * 100) / objMonthSummary.TotalDepositAmount;
						DepositAverage = objMonthSummary.TotalDepositAmount / objMonthSummary.DepositCount;
					}
					currentMonthReport.ColumnValue.DepositAverage = cashflowService.PlaidcustomRound(DepositAverage, 2);
					currentMonthReport.ColumnValue.ADBPercentage = cashflowService.PlaidcustomRound(ADBPercentage, 2);
					objGridDataAll.push(currentMonthReport);
					monthCounter = monthCounter + 1;

					if (monthlyReportGroup.indexOf(monthCounter) > -1) {
						var reportItem = {
							'ReportHeader': '',
							'GridData': [{}
							],
							'GridRowOrder': 1
						};
						reportItem.ReportHeader = 'Last ' + monthCounter.toString() + ' Months';
						reportItem.GridData = GetLastNMonthGridReport(objGridDataAll);
						reportList.push(reportItem);
					}
				}
			});
			if (!currentMonthDataExist) {
				var currentMonthReport = GetGridReportFormat();
				var gridNamePreFix = 'MTD ';
				currentMonthReport.GridRowOrder = 0;
				currentMonthReport.GridRowHeader = gridNamePreFix + currentMonth + ' ' + currentYear;
				currentMonthReport.ColumnValue.Deposits = 0;
				currentMonthReport.ColumnValue.DepositCount = 0;
				currentMonthReport.ColumnValue.BeginningBalance = 0;
				currentMonthReport.ColumnValue.EndingBalance = 0;
				currentMonthReport.ColumnValue.IncDec = 0;
				currentMonthReport.ColumnValue.ADB = 0;
				currentMonthReport.ColumnValue.NegativeDays = 0;
				currentMonthReport.ColumnValue.NSF = 0;
				currentMonthReport.ColumnValue.Name = currentMonth;
				currentMonthReport.ColumnValue.Year = currentYear;
				currentMonthReport.ColumnValue.DepositAverage = 0;
				currentMonthReport.ColumnValue.ADBPercentage = 0;

			}
		}

		objGridReport.AccountHolderName = accountName;
		objGridReport.AccountType = accountType;
		objGridReport.PDFReportName = accountNumber + '_Monthly_Grid.pdf';
		objGridDataAll = objGridDataAll.sort((a, b) => {
			return a.GridRowOrder - b.GridRowOrder;
			PDFReportName
		});
		var reportItem = {
			'ReportHeader': '',
			'GridData': [{}
			],
			'GridRowOrder': 0
		};
		reportItem.ReportHeader = accountName;
		reportItem.GridData = objGridDataAll;
		reportList.push(reportItem);
		reportList = reportList.sort((a, b) => {
			return a.GridRowOrder - b.GridRowOrder;
		});
		objGridReport.ReportList = reportList;
		return objGridReport;
	}

	function GetGridReportFormat() {
		var gridReport = {
			'GridRowHeader': '',
			'GridRowOrder': 100,
			'ColumnValue': {
				'Deposits': 0.00,
				'DepositCount': 0,
				'DepositAverage': 0.00,
				'BeginningBalance': 0.00,
				'EndingBalance': 0.00,
				'IncDec': 0.00,
				'ADB': 0.00,
				'ADBPercentage': 0.00,
				'NegativeDays': 0,
				'NSF': 0,
				'Name': '',
				'Year': '',
				'LoanPaymentAmount': 0.00,
				'LoanWithdrawalAmount': 0.00
			}
		};
		return gridReport;
	}
	function GetTotalForGridReport(arrayData, type) {
		var totalDepositsAmount = 0.00;
		var totalDepositsCount = 0;
		var totalADB = 0.00;
		var totalNegativeDays = 0;
		var totalNSF = 0;
		var beginningBalance = 0.00;
		var endingBalance = 0.00;
		var IncDec = 0.00;
		var ADBPercentage = 0.00;
		var DepositAverage = 0.00;
		var RunningBalance = 0.00;
		var totalLoanPayment = 0.00;
		var totalWithdrawalAmount = 0.00;
		var result = {
			totalDepositsAmount: 0.00,
			totalDepositsCount: 0,
			totalLoanPayment: 0,
			totalWithdrawalAmount: 0,
			totalADB: 0.00,
			totalNegativeDays: 0,
			totalNSF: 0,
			beginningBalance: 0.00,
			endingBalance: 0.00,
			IncDec: 0.00,
			ADBPercentage: 0.00,
			DepositAverage: 0.00,
			RunningBalance: 0.00
		};
		if (type == 'AVG') {
			arrayData.forEach(function (objData) {
				totalDepositsAmount += objData.ColumnValue.Deposits;
				totalDepositsCount += objData.ColumnValue.DepositCount;
				totalLoanPayment += objData.ColumnValue.LoanPaymentAmount;
				totalWithdrawalAmount += objData.ColumnValue.LoanWithdrawalAmount;
				DepositAverage += objData.ColumnValue.DepositAverage;
				totalADB += objData.ColumnValue.ADB;
				totalNegativeDays += objData.ColumnValue.NegativeDays;
				totalNSF += objData.ColumnValue.NSF;
				beginningBalance += objData.ColumnValue.BeginningBalance;
				endingBalance += objData.ColumnValue.EndingBalance;
				ADBPercentage += objData.ColumnValue.ADBPercentage;
				IncDec += objData.ColumnValue.IncDec;
				RunningBalance += objData.ColumnValue.RunningBalance;

			});
		} else if (type == 'MIN' || type == 'MAX') {
			totalDepositsAmount = arrayData[0].ColumnValue.Deposits;
			totalDepositsCount = arrayData[0].ColumnValue.DepositCount;
			totalLoanPayment = arrayData[0].ColumnValue.LoanPaymentAmount;
			totalWithdrawalAmount = arrayData[0].ColumnValue.LoanWithdrawalAmount;
			DepositAverage = arrayData[0].ColumnValue.DepositAverage;
			totalADB = arrayData[0].ColumnValue.ADB;
			totalNegativeDays = arrayData[0].ColumnValue.NegativeDays;
			totalNSF = arrayData[0].ColumnValue.NSF;
			beginningBalance = arrayData[0].ColumnValue.BeginningBalance;
			endingBalance = arrayData[0].ColumnValue.EndingBalance;
			ADBPercentage = arrayData[0].ColumnValue.ADBPercentage;
			IncDec = arrayData[0].ColumnValue.IncDec;
			if (type == 'MIN') {
				for (var X = 0; X < arrayData.length; X++) {
					if (totalDepositsAmount > arrayData[X].ColumnValue.Deposits) {
						totalDepositsAmount = arrayData[X].ColumnValue.Deposits;
					}
					if (totalDepositsCount > arrayData[X].ColumnValue.DepositCount) {
						totalDepositsCount = arrayData[X].ColumnValue.DepositCount;
					}
					if (DepositAverage > arrayData[X].ColumnValue.DepositAverage) {
						DepositAverage = arrayData[X].ColumnValue.DepositAverage;
					}
					if (totalADB > arrayData[X].ColumnValue.ADB) {
						totalADB = arrayData[X].ColumnValue.ADB;
					}
					if (totalNegativeDays > arrayData[X].ColumnValue.NegativeDays) {
						totalNegativeDays = arrayData[X].ColumnValue.NegativeDays;
					}
					if (totalNSF > arrayData[X].ColumnValue.NSF) {
						totalNSF = arrayData[X].ColumnValue.NSF;
					}
					if (beginningBalance > arrayData[X].ColumnValue.BeginningBalance) {
						beginningBalance = arrayData[X].ColumnValue.BeginningBalance;
					}
					if (endingBalance > arrayData[X].ColumnValue.EndingBalance) {
						endingBalance = arrayData[X].ColumnValue.EndingBalance;
					}
					if (ADBPercentage > arrayData[X].ColumnValue.ADBPercentage) {
						ADBPercentage = arrayData[X].ColumnValue.ADBPercentage;
					}
					if (IncDec > arrayData[X].ColumnValue.IncDec) {
						IncDec = arrayData[X].ColumnValue.IncDec;
					}
					if (totalLoanPayment > arrayData[X].ColumnValue.LoanPaymentAmount) {
						totalLoanPayment = arrayData[X].ColumnValue.LoanPaymentAmount;
					}
					if (totalWithdrawalAmount > arrayData[X].ColumnValue.LoanWithdrawalAmount) {
						totalWithdrawalAmount = arrayData[X].ColumnValue.LoanWithdrawalAmount;
					}
				}
			} else if (type == 'MAX') {
				for (var Y = 0; Y < arrayData.length; Y++) {
					if (totalDepositsAmount < arrayData[Y].ColumnValue.Deposits) {
						totalDepositsAmount = arrayData[Y].ColumnValue.Deposits;
					}
					if (totalDepositsCount < arrayData[Y].ColumnValue.DepositCount) {
						totalDepositsCount = arrayData[Y].ColumnValue.DepositCount;
					}
					if (DepositAverage < arrayData[Y].ColumnValue.DepositAverage) {
						DepositAverage = arrayData[Y].ColumnValue.DepositAverage;
					}
					if (totalADB < arrayData[Y].ColumnValue.ADB) {
						totalADB = arrayData[Y].ColumnValue.ADB;
					}
					if (totalNegativeDays < arrayData[Y].ColumnValue.NegativeDays) {
						totalNegativeDays = arrayData[Y].ColumnValue.NegativeDays;
					}
					if (totalNSF < arrayData[Y].ColumnValue.NSF) {
						totalNSF = arrayData[Y].ColumnValue.NSF;
					}
					if (beginningBalance < arrayData[Y].ColumnValue.BeginningBalance) {
						beginningBalance = arrayData[Y].ColumnValue.BeginningBalance;
					}
					if (endingBalance < arrayData[Y].ColumnValue.EndingBalance) {
						endingBalance = arrayData[Y].ColumnValue.EndingBalance;
					}
					if (ADBPercentage < arrayData[Y].ColumnValue.ADBPercentage) {
						ADBPercentage = arrayData[Y].ColumnValue.ADBPercentage;
					}
					if (IncDec < arrayData[Y].ColumnValue.IncDec) {
						IncDec = arrayData[Y].ColumnValue.IncDec;
					}
					if (totalLoanPayment < arrayData[Y].ColumnValue.LoanPaymentAmount) {
						totalLoanPayment = arrayData[Y].ColumnValue.LoanPaymentAmount;
					}
					if (totalWithdrawalAmount < arrayData[Y].ColumnValue.LoanWithdrawalAmount) {
						totalWithdrawalAmount = arrayData[Y].ColumnValue.LoanWithdrawalAmount;
					}
				}
			}
		}
		result.totalDepositsAmount = totalDepositsAmount;
		result.totalDepositsCount = totalDepositsCount;
		result.DepositAverage = DepositAverage;
		result.totalADB = totalADB;
		result.totalNegativeDays = totalNegativeDays;
		result.totalNSF = totalNSF;
		result.beginningBalance = beginningBalance;
		result.endingBalance = endingBalance;
		result.ADBPercentage = ADBPercentage;
		result.IncDec = IncDec;
		result.RunningBalance = RunningBalance;
		result.totalLoanPayment = totalLoanPayment;
		result.totalWithdrawalAmount = totalWithdrawalAmount;
		return result;
	}
	function GetLastNMonthGridReport(gridData) {
		var fixedGridRowHeader = ['AVG', 'MIN', 'MAX'];
		var objGridDataNMonth = [];
		var groupData = gridData.filter(function (data) {
			return (data.GridRowOrder > 0);
		});
		if (groupData != null && groupData.length > 0) {
			fixedGridRowHeader.forEach(function (objRow) {
				var groupReport = GetGridReportFormat();
				groupReport.GridRowHeader = objRow;
				var AVGData = GetTotalForGridReport(groupData, objRow);
				if (objRow == 'AVG') {
					groupReport.ColumnValue.Deposits = AVGData.totalDepositsAmount > 0 ? cashflowService.PlaidcustomRound(AVGData.totalDepositsAmount / groupData.length, 2) : 0.00;
					groupReport.ColumnValue.DepositCount = AVGData.totalDepositsCount > 0 ? cashflowService.PlaidcustomRound(AVGData.totalDepositsCount / groupData.length, 2) : 0;
					groupReport.ColumnValue.DepositAverage = AVGData.DepositAverage ? cashflowService.PlaidcustomRound(AVGData.DepositAverage / groupData.length, 2) : 0.00;
					groupReport.ColumnValue.ADB = AVGData.totalADB > 0 ? cashflowService.PlaidcustomRound(AVGData.totalADB / groupData.length, 2) : 0.00;
					groupReport.ColumnValue.NegativeDays = AVGData.totalNegativeDays > 0 ? cashflowService.PlaidcustomRound(AVGData.totalNegativeDays / groupData.length, 2) : 0;
					groupReport.ColumnValue.NSF = AVGData.totalNSF > 0 ? cashflowService.PlaidcustomRound(AVGData.totalNSF / groupData.length, 2) : 0;
					groupReport.ColumnValue.BeginningBalance = AVGData.beginningBalance > 0 ? cashflowService.PlaidcustomRound(AVGData.beginningBalance / groupData.length, 2) : 0;
					groupReport.ColumnValue.EndingBalance = AVGData.endingBalance > 0 ? cashflowService.PlaidcustomRound(AVGData.endingBalance / groupData.length, 2) : 0;
					groupReport.ColumnValue.ADBPercentage = AVGData.ADBPercentage > 0 ? cashflowService.PlaidcustomRound(AVGData.ADBPercentage / groupData.length, 2) : 0;
					groupReport.ColumnValue.IncDec = AVGData.IncDec > 0 ? cashflowService.PlaidcustomRound(AVGData.IncDec / groupData.length, 2) : 0;
					groupReport.ColumnValue.LoanPaymentAmount = cashflowService.PlaidcustomRound(AVGData.totalLoanPayment / groupData.length, 2);
					groupReport.ColumnValue.LoanWithdrawalAmount = cashflowService.PlaidcustomRound(AVGData.totalWithdrawalAmount / groupData.length, 2);
					groupReport.ColumnValue.GridRowOrder = 1;
				} else {
					groupReport.ColumnValue.Deposits = cashflowService.PlaidcustomRound(AVGData.totalDepositsAmount, 2);
					groupReport.ColumnValue.DepositCount = cashflowService.PlaidcustomRound(AVGData.totalDepositsCount, 2);
					groupReport.ColumnValue.DepositAverage = cashflowService.PlaidcustomRound(AVGData.DepositAverage, 2);
					groupReport.ColumnValue.ADB = cashflowService.PlaidcustomRound(AVGData.totalADB, 2);
					groupReport.ColumnValue.NegativeDays = cashflowService.PlaidcustomRound(AVGData.totalNegativeDays, 2);
					groupReport.ColumnValue.NSF = cashflowService.PlaidcustomRound(AVGData.totalNSF, 2);
					groupReport.ColumnValue.BeginningBalance = cashflowService.PlaidcustomRound(AVGData.beginningBalance, 2);
					groupReport.ColumnValue.EndingBalance = cashflowService.PlaidcustomRound(AVGData.endingBalance, 2);
					groupReport.ColumnValue.ADBPercentage = cashflowService.PlaidcustomRound(AVGData.ADBPercentage, 2);
					groupReport.ColumnValue.IncDec = cashflowService.PlaidcustomRound(AVGData.IncDec, 2);
					groupReport.ColumnValue.LoanPaymentAmount = cashflowService.PlaidcustomRound(AVGData.totalLoanPayment, 2);
					groupReport.ColumnValue.LoanWithdrawalAmount = cashflowService.PlaidcustomRound(AVGData.totalWithdrawalAmount, 2);
				}
				objGridDataNMonth.push(groupReport);
			});
		}
		return objGridDataNMonth;
	}
}
