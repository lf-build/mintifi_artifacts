function cashflowibv() {
	var self = this;	
	return { 
		getTransactions: function (baseUrl, entityType, entityId, accountId) {
			var url = [baseUrl, entityType, entityId, 'transaction', accountId].join('/');
			return new Promise(function (resolve, reject) {
				try {
					self.http.get(url).catch(function (error) {
						reject('cashflow get failed for1 ' + entityType + ':' + entityId);
					})
						.then(function (response) {
							resolve(response);
						});
				} catch (exception) {
					reject('cashflow get failed for2 ' + entityType + ':' + entityId + ':' + exception);
				}
			});
		},
		getTransactionsWithFilter: function (baseUrl, entityType, entityId, filter, accountId) {
			var url = [baseUrl, entityType, entityId, 'transaction', filter, accountId].join('/');
			return new Promise(function (resolve, reject) {
				try {
					self.http.get(url).catch(function (error) {
						reject('cashflow get failed for1 ' + entityType + ':' + entityId);
					})
						.then(function (response) {
							resolve(response);
						});
				} catch (exception) {
					reject('cashflow get failed for2 ' + entityType + ':' + entityId + ':' + exception);
				}
			});
		},
		getTransactionsResult: function (baseUrl, entityType, entityId, accountId, fromDate, toDate) {
			var url = '';
			if (fromDate != undefined && fromDate != null && toDate != undefined && toDate != null) {
				url = [baseUrl, entityType, entityId, 'transactionResult', accountId, fromDate, toDate].join('/');
			} else {
				url = [baseUrl, entityType, entityId, 'transactionResult', accountId].join('/');
			}
			return new Promise(function (resolve, reject) {
				try {
					self.http.get(url).catch(function (error) {
						reject('cashflow get failed for1 ' + entityType + ':' + entityId);
					})
						.then(function (response) {
							resolve(response);
						});
				} catch (exception) {
					reject('cashflow get failed for2 ' + entityType + ':' + entityId + ':' + exception);
				}
			});
		},
		getCashflow: function (entityType, entityId, accountId) {
			var baseUrl = 'http://cashflow-ibv:5000';
			var url = [baseUrl, entityType, entityId, 'cashflowData', accountId].join('/');
			return self.http.get(url);
		},
		getSampleTransactions: function (data) {
			return new Promise(function (resolve, reject) {
				resolve(data);
			});
		},
		getPlaidMonths: function () {
			return ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
		},
		getMonthIndex: function (month) {
			var totalMonths = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
	    return totalMonths.indexOf(month);
		},
    getMonthStartDate : function (month , year) {   
      return  new Date(year,month, 1);
		},	
		getMonthLastDate : function (month , year) {   
      return  new Date(year,month + 1, 0);
		},	
		getPlaidCategories: function (val) {
			var resultVal;
			switch (val) {
				case 'payroll':
					resultVal = '21009000';
					break;
				case 'loanpayment':
					resultVal = '16003000';
					break;
				case 'nsf':
					resultVal = '10007000';
					break;
				case 'skipdeposits':
					resultVal = ['21000000', '21001000'];
					break;
				case 'revenue':
					resultVal = ['21002000', '21004000', '21005000', '21007000', '21007001', '21007002', '21010000', '21010001', '21010002', '21010003', '21010004', '21010005', '21010006', '21010007', '21010008', '21010009', '21010010', '21010011', '16001000', '21012000', '21012001', '21012002'];
					break;
				case 'allowedcategories':
					resultVal = ['21009000', '21002000', '10001000', '16003000', '10007000'];
					break;
			}
			return resultVal;
		},
		getFinicityCategories: function (val) {
			var resultVal;
			switch (val) {
			case 'payroll':
				resultVal = 'Paycheck';
				break;
			case 'loanpayment':
				resultVal = 'Loan Payment';
				break;
			case 'nsf':
				resultVal = 'Bank Fee';
				break;
			case 'skipdeposits':
				resultVal = ['21000000', '21001000'];
				break;
			case 'revenue':
				resultVal = ['Income', 'Interest Income', 'Paycheck', 'Allowance', 'Bonus', 'Dividend & Cap Gains'];
				break;
			case 'allowedcategories':
				resultVal = ['Paycheck', 'Income', 'Bank Fee', 'Loan Fees and Charges', 'Loan Insurance', 'Loan Interest', 'Loan Payment', 'Loan Principal', 'Loans'];
				break;
			}
			return resultVal;
		},
		filterAndSortDescending: function (trans, providerAccountId) {
			var returnTrans = trans.filter(function (val) {
				return val.providerAccountId == providerAccountId;
			});
			if (returnTrans != null && returnTrans.length > 0) {
				returnTrans.sort((a, b) => {
					var start = +new Date(a.transactionDate);
					var elapsed = +new Date(b.transactionDate) - start;
					return elapsed;
				});
			}
			return returnTrans;
		},
		filterCurrentMonthTransaction: function (trans, currentMonth, currentYear) {
			return trans.filter(function (val) {
				if (new Date(val.transactionDate).getMonth() == currentMonth && new Date(val.transactionDate).getFullYear() == currentYear) {
					return val;
				}
			});
		},
		GetRoundingAmount: function (amount, methodType) {
			var result = amount;
			switch (methodType) {
				case 1:
					result = Math.floor(amount);
					break;
				case 2:
					result = Math.ceil(amount);
					break;
			}
			return result;
		},
		GetGroupByData: function (data, prop) {
			return data.reduce(function (groups, item) {
				var val = item[prop];
				groups[val] = groups[val] || [];
				groups[val].push(item);
				return groups;
			}, {});
		},
		GetMinMaxValue: function (vList, search) {
			var result = 0;
			if (search == 'min') {
				result = 10000;
				for (var i = 0; i < vList.length; i++) {
					if (vList[i].diffDays != undefined && vList[i].diffDays <= result) {
						if (vList[i].diffDays != 0) {
							result = vList[i].diffDays;
						}
					}
				}
			} else if (search == 'max') {
				for (var i = 0; i < vList.length; i++) {
					if (vList[i].diffDays != undefined && vList[i].diffDays >= result) {
						result = vList[i].diffDays;
					}
				}
			}
			return result;
		},
		GetCategoryLookupPlaid: function () {
			var myCat = [{
					'CategoryId': '21002000',
					'Name': 'ACH Credit',
					'CustomCategoryId': '5'
				}, {
					'CategoryId': '21002000',
					'Name': 'ACH Debit',
					'CustomCategoryId': '4'
				}, {
					'CategoryId': '10001000',
					'Name': 'Overdraft',
					'CustomCategoryId': '3'
				}, {
					'CategoryId': '21009000',
					'Name': 'Payroll',
					'CustomCategoryId': '1'
				}, {
					'CategoryId': '16003000',
					'Name': 'Loan Payment',
					'CustomCategoryId': '2'
				}, {
					'CategoryId': '10007000',
					'Name': 'NSF',
					'CustomCategoryId': '6'
				}
			];
			return myCat;
		},
		GetCategoryLookupFinicity: function () {
			var myCat = [{
					'CategoryId': 'ACH Credit',
					'Name': 'ACH Credit',
					'CustomCategoryId': '5'
				}, {
					'CategoryId': 'ACH Debit',
					'Name': 'ACH Debit',
					'CustomCategoryId': '4'
				}, {
					'CategoryId': 'Overdraft',
					'Name': 'Overdraft',
					'CustomCategoryId': '3'
				}, {
					'CategoryId': 'Paycheck',
					'Name': 'Payroll',
					'CustomCategoryId': '1'
				}, {
					'CategoryId': 'Loan Payment',
					'Name': 'Loan Payment',
					'CustomCategoryId': '2'
				}, {
					'CategoryId': 'Bank Fee',
					'Name': 'NSF',
					'CustomCategoryId': '6'
				}
			];
			return myCat;
		},
		AGSSum: function (numbersArray) {
			if (numbersArray != null && numbersArray.length > 0) {
				return numbersArray.reduce(function (a, b) {
					return parseFloat(a) + parseFloat(b)
				});
			} else {
				return 0;
			}
		},
		GetDaysBelow100Count: function (data) {
			var count = 0;
			data = this.RemoveUndefinedData(data);
			for (var c = 0; c < data.length; c++) {
				if (data[c] < 1000) {
					count = count + 1;
				}
			}
			return count;
		},
		RemoveUndefinedData: function (data) {
			data = data.filter(function (element) {
				return element !== undefined;
			});
			return data;
		},
		GetMedianSingleArray: function (data) {
			data = this.RemoveUndefinedData(data);
			var m = data.sort(function (a, b) {
				return parseFloat(a) - parseFloat(b);
			});
			var middle = Math.floor((m.length - 1) / 2);
			if (m.length % 2) {
				return m[middle];
			} else {
				return (parseFloat(m[middle]) + parseFloat(m[middle + 1])) / 2.0;
			}
		},
		GetMaxDaysBelow100: function (data) {
			return Math.max.apply(Math, data.map(function (o) {
				return o.DaysBelow100Count;
			}));
		},
		GetPlaidMonthlyCashFlowViewModel: function () {
			var PlaidMonthlyCashFlowViewModel = {
				Name: '',
				Year: 0,
				BeginingBalance: 0.00,
				EndingBalance: 0.00,
				DepositCount: 0,
				WithdrawalCount: 0,
				TotalDailyBalance: 0,
				TotalDepositAmount: 0.00,
				TotalWithdrawalAmount: 0.00,
				AverageDailyBalance: 0.00,
				FirstTransactionDate: '',
				EndTransactionDate: '',
				MinDepositAmount: 0.00,
				MaxDepositAmount: 0.00,
				MinWithdrawalAmount: 0.00,
				MaxWithdrawalAmount: 0.00,
				NumberOfNSF: 0,
				NSFAmount: 0.00,
				LoanPaymentAmount: 0.00,
				NumberOfLoanPayment: 0,
				PayrollAmount: 0.00,
				NumberOfPayroll: 0,
				NumberOfNegativeBalance: 0.00,
				CustomAttributes: '',
				DaysBelow100Count: 0,
				TotalMonthlyRevenueAmount: 0.00,
				TotalMonthlyRevenueCount: 0,
				PDFReportName: '',
				MonthlyTotalDailyBalanceArray: [],
				MonthlyTotalDailyDepositArray: []
			};
			return PlaidMonthlyCashFlowViewModel;
		},
		GetPlaidTransactionSummaryViewModel: function () {
			var PlaidTransactionSummaryViewModel = {
				CountOfMonthlyStatement: 0,
				StartDate: '',
				EndDate: '',
				AverageDeposit: 0,
				AnnualCalculatedRevenue: 0.00,
				AverageWithdrawal: 0,
				AverageDailyBalance: 0,
				NumberOfNegativeBalance: 0,
				NumberOfNSF: 0,
				NSFAmount: 0,
				LoanPaymentAmount: 0,
				NumberOfLoanPayment: 0,
				PayrollAmount: 0,
				NumberOfPayroll: 0,
				ChangeInDepositVolume: 0,
				TotalCredits: 0,
				TotalDebits: 0,
				TotalCreditsCount: 0,
				TotalDebitsCount: 0,
				AvailableBalance: 0,
				CurrentBalance: 0,
				AverageBalanceLastMonth: 0,
				MedianMonthlyIncome: 0,
				MedianDailyBalance: 0,
				MaxDaysBelow100Count: 0,
				CVOfDailyDeposit: 0,
				CVOfDailyBalance: 0,
				AverageMonthlyRevenue: 0.00,
				TotalRevenueAmount: 0.00,
				TotalRevenueCount: 0,
				PDFReportName: '',
				BrokerAGS: 0,
				CAPAGS: 0,
				BrokerAGSText: ''
			};
			return PlaidTransactionSummaryViewModel;
		},
		GetMyCashflowModel: function () {
			return {
				'MonthlyCashFlows': [{}
				],
				'CategorySummary': [{}
				],
				'RecurringList': [{}
				],
				'MCARecurringList': [{}
				],
				'GridReport': {}
			};
		},
		GetPlaidCashFlowViewModel: function () {
			return {
				'AccountHeader': '',
				'AccountID': '',
				'AccountNumber': '',
				'InstitutionName': '',
				'AccountType': '',
				'referenceNumber': '',
				'IsSelected': false,
				'Source': '',
				'NameOnAccount':'',
				'OfficialAccountName':'',
				'CashFlow': this.GetMyCashflowModel()
			};
		},
		GetDummyTransactionRecord: function (currentMonth, currentYear) {
			return {
				'categoryId': 0,
				'amount': 0,
				'transactionDate': (currentMonth + 1) + '/01/' + currentYear,
				'IsDummyRecord': true
			};
		},
		GetFormattedDate: function (date) {
			date = new Date(date);
			var year = date.getFullYear();
			var month = (1 + date.getMonth()).toString();
			month = month.length > 1 ? month : '0' + month;
			var day = date.getDate().toString();
			day = day.length > 1 ? day : '0' + day;
			return month + '/' + day + '/' + year;
		},
		GetDaysBetweenTransaction: function (d1, d2) {
			var date1 = new Date(d1);
			var date2 = new Date(d2);
			var timeDiff = Math.abs(date2.getTime() - date1.getTime());
			var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
			return diffDays;
		},
		GetDaysBetweenDate: function (d1) {
			var date1 = new Date(d1);
			var date2 = new Date();
			var timeDiff = Math.abs(date2.getTime() - date1.getTime());
			var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
			return diffDays;
		},
		GetLastCalendarMonth: function (counter) {
			var today = new Date();
			var lastNMonths = [];
			for (var i = 0; i < counter; i++) {
				var month = today.getMonth() - i;
				var year = today.getFullYear();
				if (month < 0) {
					month = month + 12;
					year = year - 1;
				}
				lastNMonths.push(month + '-' + year);
			}
			return lastNMonths;
		},
		RemoveSpace: function (str) {
			var result = '';
			var allStr = str.split(' ');
			for (var s = 0; s < allStr.length; s++) {
				result += allStr[s].toString();
			}
			return result;
		},
		DailyCoEfficient: function (data, sum, count) {
			Math.mean = function (array) {
				return array.reduce(function (a, b) {
					return a + b;
				}) / array.length;
			};
			Math.stDeviation = function (array, mean) {
				var dev = array.map(function (itm) {
					return (itm - mean) * (itm - mean);
				});
				return Math.sqrt(dev.reduce(function (a, b) {
					return a + b;
				}) / (array.length - 1));
			};
			data = this.RemoveUndefinedData(data);
			if (data != null && data.length > 0) {
				var average = Math.mean(data);
				var standardDeviation = Math.stDeviation(data, average);
				if (standardDeviation != undefined && !isNaN(standardDeviation)) {
					return (standardDeviation / average) * 100;
				}
				return 0;
			} else {
				return 0;
			}
		},
		getCustomFormattedDate: function (date) {
			date = new Date(date);
			var year = date.getFullYear();
			var month = (1 + date.getMonth()).toString();
			var plaidMonths = this.getPlaidMonths();
			var MonthName = plaidMonths[date.getMonth()];
			month = month.length > 1 ? month : '0' + month;
			var day = date.getDate().toString();
			day = day.length > 1 ? day : '0' + day;
			var customData = {
				Date: month + '/' + day + '/' + year,
				Month: MonthName,
				Year: year
			};
			return customData;
		},
		isWeekday: function (year, month, day) {
			var day = new Date(year, month, day).getDay();
			return day != 0 && day != 6;
		},
		plaidGetDaysInMonth: function (month, year, excludeWeekEnds) {
			var TotalDaysInMonth = new Date(year, month + 1, 0).getDate();
			if (excludeWeekEnds == false) {
				return TotalDaysInMonth;
			} else {
				var weekdays = 0;
				for (var i = 0; i < TotalDaysInMonth; i++) {
					if (isWeekday(year, month, i + 1))
						weekdays++;
				}
				return weekdays;
			}
		},
		PlaidcustomRound: function (value, precision) {
			var multiplier = Math.pow(10, precision || 0);
			return Math.round(value * multiplier) / multiplier;
		},
		PlaidsortObject: function (o) {
			var sorted = {},
				key,
				a = [];
			for (key in o) {
				if (o.hasOwnProperty(key)) {
					a.push(key);
				}
			}
			a.sort();
			for (key = 0; key < a.length; key++) {
				sorted[a[key]] = o[a[key]];
			}
			return sorted;
		},
		Plaidpad: function (s) {
			return (s < 10) ? '0' + s : s;
		},
		PlaidcustomDate: function (d) {
			return [this.Plaidpad(d.getMonth() + 1), this.Plaidpad(d.getDate()), d.getFullYear()].join('/');
		},
		PlaidcalculateAvgBalanceOfMonth: function (dailyBalanceList, numberOfDaysInMonth) {
			var totalMonthlyDailyBalance = 0;
			var emptyCount = 1;
			for (var index = 30; index >= 0; index--) {
				if (dailyBalanceList[index] == undefined) {
					emptyCount++;
				} else {
					totalMonthlyDailyBalance += (dailyBalanceList[index] * emptyCount);
					emptyCount = 1;
				}
			}
			return this.PlaidcustomRound((totalMonthlyDailyBalance / numberOfDaysInMonth), 2);
		},
		FindFirstIndexValue: function (arry) {
			var value = 0;
			var index = 0;
			arry.some(function (v, i) {
				value = v;
				index = i;
				return true;
			});

			return value;
		},
		CalculateADB: function (dailyBalanceList, numberOfDaysInMonth, totalDailyBalance) {
			var response = {};
			var totalMonthlyDailyBalance = 0;
			var negativeDaysCount = 0;
			var daysCounter = numberOfDaysInMonth - 1;
			var emptyCount = 1;
			var IsFirstTransactionNegative = false;
			if (this.FindFirstIndexValue(dailyBalanceList) < 0) {
				IsFirstTransactionNegative = true;
			}
			for (var index = daysCounter; index >= 0; index--) {
				if (dailyBalanceList[index] == undefined) {
					emptyCount++;
				} else {
					if (dailyBalanceList[index] < 0) {
						negativeDaysCount = negativeDaysCount + emptyCount;
					}
					totalMonthlyDailyBalance += (dailyBalanceList[index] * emptyCount);
					emptyCount = 1;
				}
			}
			if (emptyCount > 1) {
				var lastDailyBalance = totalDailyBalance[totalDailyBalance.length - 1] * (emptyCount - 1);
				totalMonthlyDailyBalance += lastDailyBalance;
				if (IsFirstTransactionNegative == true) {
					negativeDaysCount = negativeDaysCount + (emptyCount - 1);
				}
			}
			response =
				{
					TotalDailyBalance :  this.PlaidcustomRound(totalMonthlyDailyBalance, 2),
					ADB: this.PlaidcustomRound((totalMonthlyDailyBalance / numberOfDaysInMonth), 2),
					NegativeDaysCount: negativeDaysCount
				};
			return response;
		},
		CalculateADBFirstMonth: function (dailyBalanceList, numberOfDaysInMonth) {
			var response = {};
			var totalMonthlyDailyBalance = 0;
			var negativeDaysCount = 0;
			var emptyCount = 1;
			var daysCounter = numberOfDaysInMonth - 1;
			for (var index = daysCounter; index >= 0; index--) {
				if (dailyBalanceList[index] == undefined) {
					emptyCount++;
				} else {
					if (dailyBalanceList[index] < 0) {
						negativeDaysCount = negativeDaysCount + emptyCount;
					}
					totalMonthlyDailyBalance += (dailyBalanceList[index] * emptyCount);
					emptyCount = 1;
				}
			}
			if (emptyCount > 1) {
				numberOfDaysInMonth = (numberOfDaysInMonth - emptyCount) + 1;
			}
			response =
				{
					TotalDailyBalance :  this.PlaidcustomRound(totalMonthlyDailyBalance, 2),
					ADB: this.PlaidcustomRound((totalMonthlyDailyBalance / numberOfDaysInMonth), 2),
					NegativeDaysCount: negativeDaysCount
				};
			return response;
		},
		CalculateADBLastMonth: function (dailyBalanceList, numberOfDaysInMonth) {
			var totalMonthlyDailyBalance = 0;
			var emptyCount = 1;
			var lastDailyBalance = 0;
			var IsLastBalance = false;
			var daysCounter = numberOfDaysInMonth - 1;
			for (var index = daysCounter; index >= 0; index--) {
				if (dailyBalanceList[index] == undefined) {
					emptyCount++;
				} else {
					if (IsLastBalance == false) {
						IsLastBalance = true;
						lastDailyBalance = dailyBalanceList[index];
					}
					totalMonthlyDailyBalance += (dailyBalanceList[index] * emptyCount);
					emptyCount = 1;
				}
			}
			if (emptyCount > 1) {
				emptyCount = emptyCount - 1;
				totalMonthlyDailyBalance += lastDailyBalance * emptyCount;
			}
			return this.PlaidcustomRound((totalMonthlyDailyBalance / numberOfDaysInMonth), 2);
		},
	
		GetCategorySummary: function (trans, linksource, handleSuccess) {
			var self = this;
			var predefinedCategoryTransactions = [];
			var CategorySummaryList = [];
			if (linksource == 'finicity') {
				var allowedCategory = this.getFinicityCategories('allowedcategories');
			} else			{
				var allowedCategory = this.getPlaidCategories('allowedcategories');
			}

			for (var c = 0; c < trans.length; c++) {
				if (allowedCategory.indexOf(trans[c].categoryId) > -1) {
					predefinedCategoryTransactions.push(trans[c]);
				}
			}
			var temp = [];
			var dayRange = 30;
			if (linksource == 'finicity') {
				var allData = this.GetCategoryLookupFinicity();
			} else {
				var allData = this.GetCategoryLookupPlaid();
			}
			for (var n = 0; n < allData.length; n++) {
				var tempIndex = allData[n].CustomCategoryId;
				temp[tempIndex] = {
					TransactionCount: 0,
					CategoryName: allData[n].Name,
					TransactionTotal: 0.00,
					CategoryId: allData[n].CategoryId,
					CustomCategoryId: tempIndex,
					LastMonthTransactionCount: 0,
					LastMonthTransactionTotal: 0.00
				};
				CategorySummaryList.push(temp[tempIndex]);
			}
			predefinedCategoryTransactions.reduce(function (res, value) {
				var tIndex = value.categoryId;
				var objTemp = temp.filter(function (e) {
					return e.CategoryId == tIndex;
				});
				var customIndex = 0;
				if (objTemp != null) {
					if (objTemp.length == 1) {
						customIndex = objTemp[0].CustomCategoryId;
					} else {
						if (value.amount > 0) {
							customIndex = 4;
						} else {
							customIndex = 5;
						}
					}
					temp[customIndex].TransactionTotal += Math.abs(parseFloat(value.amount));
					temp[customIndex].TransactionCount += 1;
					if (self.GetDaysBetweenDate(value.transactionDate) <= dayRange) {
						temp[customIndex].LastMonthTransactionTotal += Math.abs(parseFloat(value.amount));
						temp[customIndex].LastMonthTransactionCount += 1;
					}
				}
				return temp;
			}, {});
			handleSuccess(CategorySummaryList);
		}
	};
}
