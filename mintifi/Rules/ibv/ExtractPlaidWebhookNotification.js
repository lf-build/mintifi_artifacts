function ExtractPlaidWebhookNotification(payload) {
	
    var data = {
        EntityId: '',
        EntityType: '',
        ErrorCode: '',
        ErrorMessage: '',
        ErrorType: '',
        Status: 0,
        ItemId: ''
    };
    try {
        if (payload != null) {
            var dataresponse = payload.body;
            if (dataresponse != null) {
                data.ItemId = dataresponse.item_id;
                var result = dataresponse.error;
                if (result != null) {
                    data.ErrorCode = result.error_code;
                    data.ErrorMessage = result.error_message;
                    data.ErrorType = result.error_type;
                    data.Status = result.status;
                }
            }
        } else {
            data.ErrorMessage = 'sanju';
        }
    } catch (e) {
        return null;
    }
    return data = {
        'NotificationDetails': data
    };


}