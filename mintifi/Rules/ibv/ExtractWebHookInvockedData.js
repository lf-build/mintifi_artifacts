function ExtractWebHookInvockedData(input) {
	function GetFinicityData(input) {
		var errorData = [];
		try {
			var lstAccounts = [];
			var lstTransactions = [];
			var data = {};
			if (input != null) {
				var cashflowResponse = input.Response;
				var cashflowRequest = input.Request;
				if (cashflowResponse != null) {
					if (cashflowResponse.body != null) {
						if (cashflowResponse.body.event != null) {
							if (cashflowResponse.body.event.class == 'account') {
								if (cashflowResponse.body.event.records != null && cashflowResponse.body.event.records.length > 0) {
									cashflowResponse.body.event.records.forEach(function (account) {
										var objAccountreq = {
											ProviderAccountId: '',
											AvailableBalance: 0,
											CurrentBalance: 0,
											BankName: '',
											Institutionid: '',
											AccountType: '',
											AccountNumber: '',
											Source: '',
											IsCashflowAccount: true,
											IsFundingAccount: false,
											BalanceDate: '',
											ProviderErrorCode: 0,
											ProviderErrorDescription: '',
											LastRefreshDate: ''
										};
										objAccountreq.Source = 'Finicity';
										objAccountreq.ProviderAccountId = account.id;
										objAccountreq.Institutionid = account.Institutionid;
										objAccountreq.AccountType = account.type;
										objAccountreq.AccountNumber = account.number;
										objAccountreq.CustomerId = account.customerId;
										if (account.detail != null && account.detail.availableBalanceAmount != null) {
											objAccountreq.AvailableBalance = account.detail.availableBalanceAmount;
										}
										objAccountreq.CurrentBalance = account.balance;
										if (account.balanceDate != null) {
											objAccountreq.BalanceDate = ConvertEPOCDate(account.balanceDate)
										}
										lstAccounts.push(objAccountreq);
									});
								}
							}
							if (cashflowResponse.body.event.class == 'transaction') {
								if (cashflowResponse.body.event.records != null && cashflowResponse.body.event.records.length > 0) {
									cashflowResponse.body.event.records.forEach(function (transaction) {
										var objcategories = [];
										var objTransaction = {
											TransactionId: '',
											CustomerId: '',
											ProviderAccountId: '',
											AccountId: '',
											Amount: 0,
											Categories: '',
											CategoryId: '',
											Description: '',
											Meta: '',
											Pending: 'False',
											TransactionDate: ''
										};
										objTransaction.TransactionId = transaction.id;
										objTransaction.CustomerId = transaction.customerId;
										objTransaction.ProviderAccountId = transaction.accountId;
										objTransaction.amount = transaction.amount;
										objTransaction.TransactionDate = ConvertEPOCDate(transaction.transactionDate);
										objTransaction.Description = transaction.description;
										if (transaction.categorization != null) {
											objTransaction.CategoryId = transaction.categorization.category;
											objcategories.push(transaction.categorization.scheduleCName);
											if (objcategories != null && objcategories.length > 0) {
												objTransaction.Categories = objcategories;
											}
										}
										if (transaction.status == 'pending') {
											objTransaction.Pending = 'True';
										}
										lstTransactions.push(objTransaction);
									})
								}
							}
						}
					}
				}
			}
		} catch (e) {
			return null
		}
		return data = {
			Accounts: lstAccounts,
			Transactions: lstTransactions
		};
	}
	return GetFinicityData(input);
	function ConvertEPOCDate(d) {
		var date = new Date(d * 1e3);
		date = date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear();
		return date
	}
}