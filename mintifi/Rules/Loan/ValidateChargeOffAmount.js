function ValidateChargeOffAmount(payload) {
    var result = {Result : false , EventName : [] };
    if (payload != null && payload.Accrual != null && payload.ChargeOff != null && payload.Fees != null ) {
        var principalOutstanding = payload.Accrual.PBOT != null ? payload.Accrual.PBOT.TotalPrincipalOutStanding:0;
        var interestOutStanding = payload.Accrual.PBOT != null ? payload.Accrual.PBOT.TotalInterestOutstanding:0;
        var feeOutStanding =  payload.Accrual.PBOT != null ? payload.Accrual.PBOT.TotalFeeOutstanding:0;
        var sum = 0;
        if(payload.ChargeOff.Fees != null)
        {
        for (var i = 0; i < payload.ChargeOff.Fees.length; i++) {
          sum += payload.ChargeOff.Fees[i].FeeAmount;
        }
        }
        if(payload.ChargeOff.ChargeOffAmount == 0 && payload.ChargeOff.InterestOutStanding == 0 && sum == 0 )
        {
            return {Result : true , EventName : ['RejectChargeOff'] };
        }
        if(payload.ChargeOff.ChargeOffAmount != principalOutstanding)
        {
            return {Result : false , EventName : [] };
        }
        if(payload.ChargeOff.InterestOutStanding != interestOutStanding)
        {
            return {Result : false , EventName : [] };
        }
        if(sum != feeOutStanding ||payload.Fees.length != payload.ChargeOff.Fees.length )
        {
            return {Result : false , EventName : [] };
        }
        if(payload.Fees.length > 0 && payload.ChargeOff.Fees.length>0 && sum == feeOutStanding && payload.Fees.length == payload.ChargeOff.Fees.length)
        {
            var IsChargeOff = false;
            payload.ChargeOff.Fees.forEach(function(entry) {               
            var chargeOff = payload.Fees.filter(function(number) {
            if(number.FeeName == entry.FeeName && number.FeeType == entry.FeeType && number.FeeAmount == entry.FeeAmount)
            {
                return number;
            }                              
            });
            if(chargeOff ==  null || chargeOff.length == 0)
            {
                IsChargeOff = true;
                return;
            }
            });
            if(IsChargeOff == true)
            {
                return result;
            }
        }

        return {Result :true , EventName : ['ChargeOff'] }; 

    }
return {Result :false , EventName : [] }; 
}