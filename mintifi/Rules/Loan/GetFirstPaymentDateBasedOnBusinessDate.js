function GetFirstPaymentDateBasedOnBusinessDate(payload) {
	var self = this;
	var achService = self.call('ach');
	return achService.getFunder(payload.FunderId).then(function (response) {
		var fundedDate = new Date(payload.Schedule.FundedDate);
		var businessDate = payload.BillingDate;
		var businessDateType = payload.BusinessDateType;
		var firstPaymentDateDetails = GetDate(businessDateType, businessDate, fundedDate, response.brokenPeriodTreatment);
		return firstPaymentDateDetails;
	}).catch(function (exception) {
		throw new Error('Funder ' + payload.FunderId + ' not found' + exception);
	});
	function GetDate(businessDateType, businessDate, fundedDate, brokenPeriodTreatment) {
		if (businessDate == 0) {
			return {
				BrokenPeriodTreatment: brokenPeriodTreatment,
				BusinessDateType: businessDateType,
				BusinessDate: businessDate
			};
		}
		var month = fundedDate.getUTCMonth();
		var date = fundedDate.getUTCDate();
		var year = fundedDate.getUTCFullYear();
		var firstPaymentDate = null;
		var originalPaymentDate = null;
		if (businessDateType.toLowerCase() == 'dayofmonth') {
			if (date == businessDate) {
				brokenPeriodTreatment = 'notset';
				originalPaymentDate = new Date(year, month + 1, businessDate, 0, 0, 0, 0);
				firstPaymentDate = new Date(year, month + 1, businessDate, 0, 0, 0, 0);
			} else if (date < businessDate) {
				originalPaymentDate = new Date(year, month, businessDate, 0, 0, 0, 0);
				if (brokenPeriodTreatment.toLowerCase() == 'firstemi' || brokenPeriodTreatment.toLowerCase() == 'ignore') {
					if (brokenPeriodTreatment.toLowerCase() == 'firstemi') {
						firstPaymentDate = getDMIDate(year, month, date, businessDate);
					} else {
						firstPaymentDate = new Date(year, month + 1, businessDate, 0, 0, 0, 0);
					}
				} else {
					firstPaymentDate = new Date(year, month, businessDate, 0, 0, 0, 0);
				}
			} else {
				originalPaymentDate = new Date(year, month + 1, businessDate, 0, 0, 0, 0);
				if (brokenPeriodTreatment.toLowerCase() == 'firstemi' || brokenPeriodTreatment.toLowerCase() == 'ignore') {
					if (brokenPeriodTreatment.toLowerCase() == 'firstemi') {
						firstPaymentDate = getDMIDate(year, month, date, businessDate);
					} else {
						firstPaymentDate = new Date(year, month + 2, businessDate, 0, 0, 0, 0);
					}
				} else {
					firstPaymentDate = new Date(year, month + 1, businessDate, 0, 0, 0, 0);
				}
			}
			return {
				FirstPaymentDate: firstPaymentDate,
				BrokenPeriodTreatment: brokenPeriodTreatment,
				BusinessDateType: businessDateType,
				BusinessDate: businessDate,
				OriginalPaymentDate: originalPaymentDate
			};
		} else {
			var day = fundedDate.getDay();
			if (day == businessDate) {
				firstPaymentDate = new Date(fundedDate.setDate(fundedDate.getDate() + parseInt(7)));
			} else {
				originalPaymentDate = fundedDate;
				var dayOfWeek = businessDate + 7;
				if (brokenPeriodTreatment.toLowerCase() == 'firstemi') {
					dayOfWeek = businessDate + 14;
				} else if (brokenPeriodTreatment.toLowerCase() == 'ignore') {
					dayOfWeek = businessDate + 14;
				}
				firstPaymentDate = getNextDayOfWeek(fundedDate, dayOfWeek);
			}
			return {
				FirstPaymentDate: firstPaymentDate,
				BrokenPeriodTreatment: brokenPeriodTreatment,
				BusinessDateType: businessDateType,
				BusinessDate: businessDate,
				OriginalPaymentDate: originalPaymentDate
			};
		}
	}
	function getDMIDate(year, month, date, businessDate) {
		if (date > 20) {
			return new Date(year, month + 2, businessDate, 0, 0, 0, 0);
		}
		return new Date(year, month + 1, businessDate, 0, 0, 0, 0);
	}
	function getNextDayOfWeek(date, dayOfWeek) {
		var resultDate = new Date(date);
		resultDate.setDate(resultDate.getDate() + (dayOfWeek + 7 - resultDate.getDay()) % 7);
		return resultDate;
	}
}
