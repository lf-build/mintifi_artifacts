function ruleForVerificationChecklist(entityType,entityId) {
	var self = this;
	var factService = self.call('factVerification');
	function getVerificationFactDetails(input, factName) {
		for (var i = 0; i < input.length; i++) {
			if (input[i]['factName'] == factName) {
				return input[i];
			}
		}
	}
	if (typeof (entityType) != 'undefined') {
		return factService.get(entityType, entityId).then(function (response) {
			var result = 'Failed';
			var counter = 0;
			var factList = ['BusinessSocialVerification', 'MerchantVerification', 'PersonalCreditVerification'];
			for (var i = 0; i < factList.length; i++) {
				var verificationList = getVerificationFactDetails(response, factList[i]);
				if (typeof (verificationList) != 'undefined') {
					if (verificationList.currentStatus == 'Completed') {
						counter = counter + 1;
					}
				}
			}
			if (counter == factList.length) {
				return {
					'result': 'Passed',
					'detail': ''
				};
			} else {
				return {
					'result': 'Failed',
					'detail': ['Verification Pending']
				};
			}
		}).catch(function (error) {
			return {
				'result': 'Failed',
				'detail': error.message
			};
		});
	} else {
		return {
			'result': 'Failed',
			'detail': ['Verification Pending']
		};
	}
}