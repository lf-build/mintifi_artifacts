function docusign_receipient_data__extraction(input) {
    var response = {
        'Status': null,
        'DocumentBytes': null,
        'ReceipientStatus': null,
        'EntityId': null,
        'EntityType': 'application'
    };
    var Status = '';
    var ReceipientStatus = new Array();
    var ReceipientData = null;
    var ReceipientId = '';
    var Id = '';
    var EntityId = '';
    if (input !== null && input !== undefined) {
        if (input.DocuSignEnvelopeInformation !== undefined && input.DocuSignEnvelopeInformation !== null) {
            var docuSignEnvelopeInformation = input.DocuSignEnvelopeInformation;
            if (docuSignEnvelopeInformation.EnvelopeStatus !== undefined && docuSignEnvelopeInformation.EnvelopeStatus !== null) {
                var envelopeStatus = docuSignEnvelopeInformation.EnvelopeStatus;
                Status = envelopeStatus.Status;
                if (envelopeStatus.RecipientStatuses !== undefined && envelopeStatus.RecipientStatuses !== null) {
                    var recipientStatuses = envelopeStatus.RecipientStatuses;
                    if (recipientStatuses.RecipientStatus !== undefined && recipientStatuses.RecipientStatus !== null) {
                        if (recipientStatuses.RecipientStatus.constructor === Array) {
                            recipientStatuses.RecipientStatus.forEach(function(obj) {
                                if (obj.userName != 'Lendfoundry') {
                                    if (obj.CustomFields !== null && obj.CustomFields !== undefined) {
                                        Id = obj.CustomFields.CustomField;
                                        ReceipientId = Id.replace(/-/g, '');
                                    }
                                    ReceipientData = {
                                        'Name': obj.UserName,
                                        'Email': obj.Email,
                                        'RecipientId': ReceipientId,
                                        'Status': obj.Status,
                                        'Id': Id,
                                        'SignedOn':obj.Signed
                                    };
                                    ReceipientStatus.push(ReceipientData);
                                }
                            });
                        } else {
                            ReceipientData = {
                                'Name': recipientStatuses.RecipientStatus.UserName,
                                'Email': recipientStatuses.RecipientStatus.Email,
                                'RecipientId': recipientStatuses.RecipientStatus.CustomFields,
                                'Status': recipientStatuses.RecipientStatus.Status
                            };
                            ReceipientStatus.push(ReceipientData);
                        }
                    }
                }
                if (envelopeStatus.CustomFields !== undefined && envelopeStatus.CustomFields !== null) {
                    if (envelopeStatus.CustomFields.CustomField !== undefined && envelopeStatus.CustomFields.CustomField !== null) {
                        EntityId = envelopeStatus.CustomFields.CustomField.Value;
                    }
                }
            }
        }
    }
    response = {
        'ReceipientStatus': ReceipientStatus,
        'Status': Status,
        'EntityId': EntityId,
        'EntityType': 'application'
    };
    return response;
}