function verification_rule_cashflowverificationmanual(payload) {
    var objData = {};
    try {
        var result = 'Failed';
        if (payload != null && payload != undefined) {
            objData = payload.cashflowVerificationManualData[0];
            if (objData != null && objData.ProductList != '' && objData.ProductList != null) {
                result = 'Passed';
            }
        }
        return {
            'result': result,
            'detail': null,
            'data': objData,
            'rejectcode': '',
            'exception': []
        };
    } catch (e) {
        return {
            'result': 'Failed',
            'detail': 'Unable to verify',
            'data': objData,
            'rejectcode': '',
            'exception': []
        };
    }
}