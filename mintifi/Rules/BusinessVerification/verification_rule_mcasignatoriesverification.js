function verification_rule_mcasignatoriesverification(payload) {
	var result = 'Passed';
	if (payload != null && payload != undefined) {
		var objData = payload.probeCompanyHistory[0];
		if (objData == null) {
			result = 'Failed';
		}
		
		return {
			'result': result,
			'detail': null,
			'data': objData,
			'rejectcode': '',
			'exception': []
		};
	}
}