function verification_rule_personal_credit(payload) {
	var objData = null;
	var SSNFlag = false;
	var CreditHistoryMonthsFlag = false;
	var BankruptcyMonthsFlag = false;
	var CreditScoreFlag = false;
	var CreditScoreLimit = 450;
	var CreditHistoryMonthsLimit = 36;
	var BankruptcyandLineMonthsLimit = 36;
	var CreditScore = '';
	var CreditHistory = '';
	var OFAC = null;
	var IsNotSSNFraudVictim = false;
	var CreditHistoryCheck = '';
	var OFACFlag = false;
	var Lines = null;
	var Linesflag = true;
	var Bankruptcyflag = true;
	var Bankruptcy = null;
	var BankruptcyDate = null;
	var BankruptcyCodes = ['c', 'd', 'g', 'i', 'v'];
	var BankruptcyDispostionflag = true;
	try {
		var result = 'Failed';
		if (payload != null && payload != undefined && payload.equifaxDetail != null && payload.equifaxDetail != undefined) {
			objData = payload.equifaxDetail[0].EquifaxDetail.Report.consumerCreditReport[0];
			if (objData != null && objData.usConsumerCreditReportType != null) {
				if (objData.usConsumerCreditReportType[0].usfico != null) {
					CreditScore = objData.usConsumerCreditReportType[0].usfico.ficoScore;
				}
				if (objData.usConsumerCreditReportType[0].usHeader.usCreditFile.fileSinceDate != null) {
					CreditHistory = objData.usConsumerCreditReportType[0].usHeader.usCreditFile.fileSinceDate.value;
				}
				IsNotSSNFraudVictim = objData.usConsumerCreditReportType[0].usHeader.usCreditFile.fraudVictimIndicator == null ? true : false;
				OFAC = objData.usConsumerCreditReportType[0].usofacAlerts;
				Bankruptcy = objData.usConsumerCreditReportType[0].usBankruptcies;
				Lines = objData.usConsumerCreditReportType[0].usTaxLiens;
			}
			if (Lines != null) {
				for (var i = 0; i < Lines.length; i++) {
					var monthdiff = GetMonthDiff(new Date(Lines[i].dateFiled.value), new Date());
					if (monthdiff <= BankruptcyandLineMonthsLimit) {
						Linesflag = false;
						break;
					}
				}
			}
			if (Bankruptcy != null) {
				for (var i = 0; i < Bankruptcy.length; i++) {
					var date = '01/' + Bankruptcy[i].dateFiled.value;
					BankruptcyDate = new Date(date);
					var monthdiff = GetMonthDiff(new Date(date), new Date());
					if (monthdiff <= BankruptcyandLineMonthsLimit) {
						Bankruptcyflag = false;
						break;
					}
					var disposition = Bankruptcy[i].disposition;
					if (disposition != null) {
						if (BankruptcyCodes.indexOf(disposition.code.toLowerCase()) > -1) {
							BankruptcyDispostionflag = false;
							break;
						}
					}
				}
			}
			if (CreditHistory != null && CreditHistory != undefined) {
				CreditHistoryCheck = GetMonthDiff(new Date(CreditHistory), new Date());
			}
			if (objData != null) {
				if (parseInt(CreditScore) > CreditScoreLimit) {
					CreditScoreFlag = true;
				}
				if (parseInt(CreditHistoryCheck) > CreditHistoryMonthsLimit) {
					CreditHistoryMonthsFlag = true;
				}
				if (OFAC === null) {
					OFACFlag = true;
				} else {
					var OFACAlert = OFAC[0];
					if (OFACAlert != null) {
						if (OFACAlert.responseCode.code == 'O') {
							OFACFlag = false;
						} else {
							OFACFlag = true;
						}
					}
				}
				SSNFlag = IsNotSSNFraudVictim;
			}
			if (CreditScoreFlag == true && CreditHistoryMonthsFlag == true && OFACFlag == true && SSNFlag == true && Linesflag == true && Bankruptcyflag == true && BankruptcyDispostionflag == true) {
				result = 'Passed';
			}
		}
		function GetMonthDiff(d1, d2) {
			var months;
			months = (d2.getFullYear() - d1.getFullYear()) * 12;
			months -= d1.getMonth() + 1;
			months += d2.getMonth();
			return months <= 0 ? 0 : months;
		}
		var Data = {
			'CreditScore': CreditScore,
			'CreditHistory': CreditHistory,
			'OFAC': OFAC,
			'CreditHistoryCheck': CreditHistoryCheck,
			'IsNotSSNFraudVictim': IsNotSSNFraudVictim,
			'Linesflag': Linesflag,
			'Bankruptcyflag': Bankruptcyflag,
			'CreditScoreFlag': CreditScoreFlag,
			'OFACFlag': OFACFlag,
			'BankruptcyDate': BankruptcyDate,
			'BankruptcyDispostionflag' : BankruptcyDispostionflag
		};
		return {
			'result': result,
			'detail': null,
			'data': Data,
			'rejectcode': '',
			'exception': []
		};
	} catch (e) {
		return {
			'result': 'Failed',
			'detail': ['Unable to verify'],
			'data': null,
			'rejectcode': '',
			'exception': [e.message]
		};
	}
}