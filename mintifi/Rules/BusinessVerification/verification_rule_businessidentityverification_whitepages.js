function verification_rule_businessidentityverification_whitepages(payload) {
	var objData = null;
	var PhoneIsPrepaid = false;
	var IsValid = false;
	var EmailFirstSeenDays = false;
	var AddressIsValid = false;
	var EmailIsDisposable = false;
	var DistanceFromAddress = false;
	var MatchResponse = ['match', 'name found'];
	var IsNameMatch = false;
	var PhoneCountryCode = false;
	var PhoneLine = true;
	var linetypes = ['non-fixed voip', 'toll free'];
	var CountryCodeIsValid = false;
	try {
		var result = 'Failed';
		if (payload != null && payload != undefined) {
			objData = payload.whitepagesIdentityCheck[0];
			if (objData != null) {
				if (parseInt(objData.DistanceFromAddress) < 1000) {
					DistanceFromAddress = true;
				}
				if (parseInt(objData.EmailFirstSeenDays) > 90) {
					EmailFirstSeenDays = true;
				}
				if (linetypes.indexOf(objData.PhoneLineType.toLowerCase()) > -1) {
					PhoneLine = false;
				}
				if (objData.PhoneIsPrepaid == false) {
					PhoneIsPrepaid = true;
				}
				if (MatchResponse.indexOf(objData.PhoneToName.toLowerCase()) > -1 && MatchResponse.indexOf(objData.EmailToName.toLowerCase()) > -1 && MatchResponse.indexOf(objData.AddressToName.toLowerCase()) > -1) {
					IsNameMatch = true;
				}
				if (MatchResponse.indexOf(objData.PhoneToAddress.toLowerCase()) > -1) {
					PhoneCountryCode = true;
				}
				if (objData.PrimaryAddressIsValid == true && objData.AddressIsCommercial == true) {
					AddressIsValid = true;
				}
				if (objData.PrimaryPhoneIsValid == true && objData.EmailIsValid == true) {
					IsValid = true;
				}
				var countryCode = objData.GeolocationCountryCode;
				if (countryCode.toLowerCase() == 'us') {
					CountryCodeIsValid = true;
				}
				if (DistanceFromAddress == true && EmailFirstSeenDays == true && PhoneIsPrepaid == true && PhoneCountryCode == true && AddressIsValid == true && IsValid == true && objData.EmailIsDisposable == false && PhoneLine == true && objData.IpIsProxy == false && IsNameMatch == true && CountryCodeIsValid == true) {
					result = 'Passed';
				}
			}
		}
		var Data = {
			'DistanceFromAddress': DistanceFromAddress,
			'EmailFirstSeenDays': EmailFirstSeenDays,
			'PhoneIsPrepaid': PhoneIsPrepaid,
			'IsNameMatch': IsNameMatch,
			'PhoneCountryCode': PhoneCountryCode,
			'AddressIsValid': AddressIsValid,
			'IsValid': IsValid,
			'PhoneLine': PhoneLine,
			'CountryCodeIsValid': CountryCodeIsValid
		};
		return {
			'result': result,
			'detail': null,
			'data': Data,
			'rejectcode': '',
			'exception': []
		};
	} catch (e) {
		return {
			'result': 'Failed',
			'detail': ['Unable to verify'],
			'data': objData,
			'rejectcode': '',
			'exception': []
		};
	}
}