function verification_rule_plaidcashflow_v2(payload) {

	var self = this;
	var result = 'Passed';
	var IsNumberOfDepositPerMonth = true;
	var IsNumberOfNegativeDays = true;
	var IsNumberOfNSF = true;
	var IsAverageBalance = true;
	var IsMonthlyDepositAmount = true;
	var IsAverageDepositSize = true;
	var IsNumberOfDeposit = true;
	var IsZeroBalance = false;
	var MinNumberOfDepositPerMonth = 3;
	var MaxNSF = 12;
	var MinAverageDailyBalance = 1000;
	var MinNumberOfDeposit = 3;
	var AvgMonthlyDeposits = 10000;
	var ZeroBalance = 0;
	var AccountID = '';
	var NumberOfNSF = '';
	var TotalCreditsCount = '';
	var AverageDailyBalance = '';
	var AccountType = '';
	var errorData = [];
	var errorMessage;
	var Data = {};
	var InstitutionName = '';
	var AverageDeposit = '';
	var AverageDepositCount = '';
	var AverageNSFCount = '';
	var CurrentBalance = '';
	var TotalMonthsCount = '';
	var cashflowService = self.call('cashflowibv');
	var plaidCashflowVerification = function (data) {
	
		var entityId = data.entityId;
		var entityType = data.entityType;
		var accountId = data.cashflowVerificationv2Data[0].SelectedAccountdata.IntermediateData.report.AccountID;

		return cashflowService.getCashflow(entityType, entityId, accountId).then(function (cashflowData) {
		
			if (cashflowData.cashFlow != null && cashflowData.cashFlow.transactionSummary != null) {
				var input = cashflowData.cashFlow.transactionSummary;
				NumberOfNSF = input.numberOfNSF;
				TotalCreditsCount = input.totalCreditsCount;
				AverageDailyBalance = input.averageDailyBalance;
				AccountID = accountId;
				InstitutionName = input.institutionName;
				AccountType = input.accountType;
				AverageDeposit = input.averageDeposit;
				AverageDepositCount = input.averageDepositCount;
				AverageNSFCount = input.averageNSFCount;
				CurrentBalance = input.currentBalance;
				TotalMonthsCount = input.totalMonthsCount;
				if (input != null) {
					if (input.averageDeposit < AvgMonthlyDeposits) {
						IsAverageDepositSize = false;
						result = 'Failed';
					}
					if (input.averageDepositCount < MinNumberOfDeposit) {
						IsNumberOfDeposit = false;
						result = 'Failed';
					}
					if (input.averageDailyBalance < MinAverageDailyBalance) {
						IsAverageBalance = false;
						result = 'Failed';
					}
					if (input.averageNSFCount > 0) {
						if (input.AverageNSFCount > MaxNSF) {
							IsNumberOfNSF = false;
							result = 'Failed';
						}
					}
					if (input.currentBalance <= ZeroBalance) {
						IsZeroBalance = true;
						result = 'Failed';
					}
					Data = {
						'IsNumberOfDepositPerMonth': IsNumberOfDepositPerMonth,
						'IsNumberOfNSF': IsNumberOfNSF,
						'IsAverageBalance': IsAverageBalance,
						'IsMonthlyDepositAmount': IsMonthlyDepositAmount,
						'IsAverageDepositSize': IsAverageDepositSize,
						'IsNumberOfDeposit': IsNumberOfDeposit,
						'IsZeroBalance': IsZeroBalance,
						'CurrentBalance': CurrentBalance,
						'AccountID': AccountID,
						'InstitutionName': 'test',
						'AccountType': AccountType,
						'AverageMonthlyDeposit': AverageDeposit,
						'AverageDepositCount': AverageDepositCount,
						'AverageDailyBalance': AverageDailyBalance,
						'AverageNSFCount': AverageNSFCount,
						'TotalMonthsCount': TotalMonthsCount,
						'NumberOfNSF': NumberOfNSF,
						'TotalDepositCount': TotalCreditsCount
					};
				} else {
					errorMessage = 'plaidCashflow information is Null : Unable to verify';
					errorData.push(errorMessage);
					result = 'Failed';
				}
			} else {
				errorMessage = 'plaidCashflow information is Null : Unable to verify';
				errorData.push(errorMessage);
				result = 'Failed';
			}
		});
	};
	if (payload != null) {
		try {
			return plaidCashflowVerification(payload).then(function () {
			
				return {
					'result': result,
					'detail': '',
					'data': Data,
					'rejectcode': '',
					'exception': errorData
				};
			});
		} catch (e) {
			errorMessage = 'Unable to verify : ' + e.message;
			errorData.push(errorMessage);
			result = 'Failed';
		}
	} else {
		result = 'Failed';
		errorMessage = 'Payload is not available';
		errorData.push(errorMessage);
	}
	return {
		'result': result,
		'detail': '',
		'data': Data,
		'rejectcode': '',
		'exception': errorData
	};
}