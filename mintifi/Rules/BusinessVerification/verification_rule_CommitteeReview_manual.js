function verification_rule_CommitteeReview_manual(payload) {
	try {
		var result = 'Failed';
		if (payload != null && payload != undefined) {
			var objData = payload.committeeReviewVerificationManualResult[0];
			if (objData.committeePassed == true) {
				result = true;
			} else {
				result = 'Failed';
			}
		}
		return {
			'result': result,
			'detail': null,
			'data': objData,
			'rejectcode': '',
			'exception': []
		};
	} catch (e) {
		return {
			'result': 'Failed',
			'detail': 'Unable to verify',
			'data': objData,
			'rejectcode': '',
			'exception': []
		};
	}
}