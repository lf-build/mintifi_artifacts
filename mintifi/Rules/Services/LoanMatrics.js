function LoanMatrics() {
	var baseUrl = 'http://{{internal_ip}}:7094/matrics';
	var self = this;

	return {
		getLoanMatrics: function (loanNumber) {
			var url = [baseUrl, loanNumber].join('/');
			return self.http.get(url);
		}
	}
}
