function loanAccounting() {
    var self = this;
    var baseUrl = 'http://{{internal_ip}}:7093';

    return {
        GetAccrualByLoCNumber: function (locNumber) {
            var url = [baseUrl, 'accrual',locNumber].join('/');
            return self.http.get(url);
        }
    };
}