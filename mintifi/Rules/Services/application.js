
function application() {
    var self = this;
    var baseUrl = 'http://business-application:5000';

    return {
        getByApplicationNumber: function (applicationNumber) {
            var url = [baseUrl, applicationNumber].join('/');
            return self.http.get(url);
        },
        add: function (application) {
            return self.http.post(baseUrl, application);
        },
		 getWithApplicant: function (applicationNumber) {
            var applicantService = self.call('applicant');
            return this.getByApplicationNumber(applicationNumber).then(function (applicationData) {
                return applicantService.get(applicationData.applicantId).then(function (data) {
                    return {
                        application: applicationData,
                        applicant: data
                    }
                })
            })
        }
    };
}
