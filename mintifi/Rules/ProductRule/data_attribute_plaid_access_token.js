function data_attribute_plaid_access_token(payload) {
	function ExtractAccessToken(payload) {
		var result = 'Failed';
		var Data = {};
		try {
			if (typeof (payload) != 'undefined') {
				var accessTokenInput = payload.eventData.Response;
				var publicTokenInput = payload.eventData.Request;
				if (accessTokenInput != null && publicTokenInput != null) {
					result = 'Passed';
					var temp = {
						'AccessToken': accessTokenInput.AccessToken,
						'PublicToken': publicTokenInput.publictoken
					};
					Data['plaidAccessToken'] = temp;


					return {
						'result': result,
						'detail': null,
						'data': Data,
						'rejectcode': '',
						'exception': []
					};
				}
			}
			return {
				'result': result,
				'detail': null,
				'data': Data,
				'rejectcode': '',
				'exception': []
			};
		}
		catch (e) {
			return {
				'result': result,
				'detail': null,
				'data': null,
				'exception': [e.message],
				'rejectCode': ''
			};

		}
	}

	return ExtractAccessToken(payload);
}