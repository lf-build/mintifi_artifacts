function data_attribute_paynet_companyreport(payload) {
	var result = "Passed";
	var IsMatch = true;
	var past_Due_31_Plus_Occurrences = "";
	var past_due_31_plus_amt = "";
	var MasterScore = "";
	var TimeInBusiness = "";
	var UCCfilingdates = "";
	var BusinessStatus = "";
	var BusinessEstablishedDate = "";
	var IsGoodStanding = true;
	var BusinessStates = null;
	if (typeof payload != "undefined") {
		try {
			if (payload.eventData.Response.xmlField != null) {
				var paynetResponse = payload.eventData.Response.xmlField.reportData;
				if (paynetResponse != null) {
					past_Due_31_Plus_Occurrences = parseInt(paynetResponse.past_Due_31_Plus_Occurrences);
					past_due_31_plus_amt = paynetResponse.pastDue_31_Plus_Amt;
					MasterScore = paynetResponse.masterScore;
					TimeInBusiness = paynetResponse.businessBackground.yearsInBusiness;
					UCCfilingdates = paynetResponse.uccFilings.uccDetail.uccFiling.filingDate;
					BusinessStatus = paynetResponse.legalNames[0].legalStatus;
					BusinessEstablishedDate = paynetResponse.legalNames[0].legalDateIncorporated;
					if (BusinessStatus != "Active" && BusinessStatus != "In Good Standing") {
						IsGoodStanding = false
					}
				}
			}
			var Data = {
				paynetReport: {
					paynetResult: "match",
					Past_due_31_plus_occurrences: past_Due_31_Plus_Occurrences,
					Past_due_31_plus_amt: past_due_31_plus_amt,
					MasterScore: MasterScore,
					TimeInBusiness: TimeInBusiness,
					UCCfilingdates: UCCfilingdates,
					BusinessStatus: BusinessStatus,
					BusinessEstablishedDate: BusinessEstablishedDate,
					IsGoodStanding: IsGoodStanding,
					referenceNumber: payload.eventData.referenceNumber
				}
			};
			return {
				result: result,
				detail: null,
				data: Data,
				rejectcode: "",
				exception: []
			}
		} catch (e) {
			return {
				result: "Failed",
				detail: ["Error in Rule" + e.message],
				data: e.message,
				rejectcode: "",
				exception: [e.message]
			}
		}
	}
}
