function data_attribute_tloxp_business_search(payload) {
	var BusinessRecord='';
	var TimeInBusiness='';
	var IncorporationDate;
	var CreditScore='';
	var LienFilings='';
	var JudgmentFilings='';
	var UCCFilings='';
	var ActiveFromSos='';	
	var BankruptcyFilings='';
	var PropertyForeclosures='';
	var SIC='';
	var TIB = 0;
	var result = 'Passed';
	if (typeof (payload) != 'undefined'&&payload!=null) {
		var businessdetailResponse = payload.eventData.Response;
		if (businessdetailResponse != null) {
			BusinessRecord=businessdetailResponse.BusinessRecord;
			CreditScore=businessdetailResponse.CreditScore;	
			if(businessdetailResponse.IncorporationDate!=null){
				IncorporationDate=businessdetailResponse.IncorporationDate;
				TIB = GetMonthDiff(new Date(parseInt(businessdetailResponse.IncorporationDate.Year),parseInt(businessdetailResponse.IncorporationDate.Month),parseInt(businessdetailResponse.IncorporationDate.Day)), new Date());
				TimeInBusiness = parseInt(TIB / 12) + ' Years ' + parseInt(TIB % 12) + ' Months';
			}
			if(businessdetailResponse.LienFilings!=null&&businessdetailResponse.LienFilings!= 'undefined'){		
				LienFilings=LatestRecord(businessdetailResponse.LienFilings);
			}
			if(businessdetailResponse.JudgmentFilings!=null&&businessdetailResponse.UCCFilings!= 'undefined'){		
				JudgmentFilings=LatestRecord(businessdetailResponse.JudgmentFilings);
			}
			if(businessdetailResponse.UCCFilings!=null&&businessdetailResponse.UCCFilings!= 'undefined'){		
				UCCFilings=LatestRecord(businessdetailResponse.UCCFilings);
			}
			ActiveFromSos=businessdetailResponse.ActiveFromSos;
			if(businessdetailResponse.BankruptcyFilings!=null&&businessdetailResponse.BankruptcyFilings!= 'undefined'){		
				BankruptcyFilings=LatestRecord(businessdetailResponse.BankruptcyFilings);
				if(BankruptcyFilings!=null)
				{
					if(BankruptcyFilings.StatusDate!=null){
					BankruptcyFilings.StatusDate=Bankruptcies.StatusDate.Day+"/"+Bankruptcies.StatusDate.Month+"/"+Bankruptcies.StatusDate.Year;
					}
				}
			}
			if(businessdetailResponse.PropertyForeclosures!=null&&businessdetailResponse.PropertyForeclosures!= 'undefined'){		
				PropertyForeclosures=LatestRecord(businessdetailResponse.PropertyForeclosures);
			}
			if(businessdetailResponse.CoorporateFilings!=null&&businessdetailResponse.CoorporateFilings!="undefined"&&BusinessRecord.FEINumber!=""){		
				SIC=CompareFeinAndRetriveSIC(businessdetailResponse.CoorporateFilings,BusinessRecord.FEINumber);
			}
		}
	}
	var Data = {
		'tloxpBusinessReport': {
			'BusinessRecord':BusinessRecord,
			'TimeInBusiness':TimeInBusiness,
			'IncorporationDate': IncorporationDate,
			'CreditScore':CreditScore,
			'LienFilings': LienFilings,
			'JudgmentFilings': JudgmentFilings,
			'UCCFilings': UCCFilings,
			'ActiveFromSos':ActiveFromSos,
			'BankruptcyFilings': BankruptcyFilings,
			'PropertyForeclosures': PropertyForeclosures,
			'SIC':SIC,
			'referenceNumber': payload.eventData.ReferenceNumber
		}
	};
	return {
		'result': result,
		'detail': null,
		'data': Data,
		'rejectcode': '',
		'exception': []
	};
	function GetMonthDiff(d1, d2) {
        var months;
        months = (d2.getFullYear() - d1.getFullYear()) * 12;
        months -= d1.getMonth() + 1;
        months += d2.getMonth();
        return months <= 0 ? 0 : months;
	}
	function LatestRecord(data){
		var latestDate='';
		var currentDate='';
		if(data.length>0){
			var latestData=data[0];
			if(data[0].FilingDate!=null&&data[0].FilingDate.Day!=null){
			latestDate=new Date(data[0].FilingDate.Year,parseInt(data[0].FilingDate.Month),data[0].FilingDate.Day);
			}
			for (var i=1;i<data.length;i++){
				if(data[i].FilingDate!=null){
					if(latestDate==""){
						latestDate=new Date(data[i].FilingDate.Year,parseInt(data[i].FilingDate.Month-1),data[i].FilingDate.Day);
					}
				currentDate=new Date(data[i].FilingDate.Year,parseInt(data[i].FilingDate.Month)-1,data[i].FilingDate.Day);
				if(currentDate>=latestDate&&data[i].FilingDate.Day!=null){
					latestData=data[i];
					latestDate=currentDate;
				}
			}
			}
			if(latestDate!=null){
			latestData.FilingDateF=latestDate.getDate()+"/"+(parseInt(latestDate.getMonth())+1)+"/"+latestDate.getFullYear();			
			}
			return latestData;
		}
	}
	
	function CompareFeinAndRetriveSIC(data,feinNo){
		var sic='';
		for(var i=0;i<data.length;i++){
			if(data[i].fEINumber==feinNo){
				sic=data[i].sIC;
				return sic;
			}
		}
		return sic;
	}
}
