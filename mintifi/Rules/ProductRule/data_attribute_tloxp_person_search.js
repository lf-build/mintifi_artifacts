function data_attribute_tloxp_person_search(payload) {
    var CreditScore = '';
    var Liens = '';
    var Judgments = '';
    var UCCFilings = '';
    var ActiveFromSos = '';
    var Bankruptcies = '';
    var CriminalDetails = '';
    var WarrantDetails = '';
    var PropertyForeclosures = '';
    var result = 'Passed';
    var key = '';
    var owner = null;
    var Data = {};
    if (typeof(payload) !== 'undefined' && payload !== null) {
       
        var persondetailResponse = payload.eventData.Response.Report;
        key=payload.eventData.Request.OwnerId;
        if (persondetailResponse !== null) {
            CreditScore = persondetailResponse.CreditScore;
            if (persondetailResponse.liens !== null && persondetailResponse.liens !== 'undefined') {
                if (persondetailResponse.liens.length > 0) {
                    Liens = LatestRecord(persondetailResponse.liens);
                    if (Liens !== null && Liens !== undefined) {
                        if (Liens.TaxLienDate !== null) {
                            Liens.TaxLienDate = Liens.taxLienDate.day + '/' + Liens.taxLienDate.month + '/' + Liens.taxLienDate.year;
                        }
                    }
                }
            }
            if (persondetailResponse.judgments !== null && persondetailResponse.judgments !== 'undefined') {
                if (persondetailResponse.judgments.length > 0) {
                    Judgments = LatestRecord(persondetailResponse.judgments);
                }
            }
            if (persondetailResponse.uccFilings !== null && persondetailResponse.uccFilings !== 'undefined') {
                if (persondetailResponse.uccFilings.length > 0) {
                    UCCFilings = LatestRecord(persondetailResponse.uccFilings);
                }
            }
            ActiveFromSos = persondetailResponse.ActiveFromSos;
            if (persondetailResponse.bankruptcies !== null && persondetailResponse.bankruptcies !== 'undefined') {
                if (persondetailResponse.bankruptcies.length > 0) {
                    Bankruptcies = LatestRecordBankruptcy(persondetailResponse.bankruptcies);
                    if (Bankruptcies !== null && Bankruptcies !== undefined) {
                        if (Bankruptcies.dischargeDate !== null) {
                            Bankruptcies.dischargeDate = Bankruptcies.dischargeDate.day + '/' + Bankruptcies.dischargeDate.month + '/' + Bankruptcies.dischargeDate.year;
                        }
                    }
                }
            }
            if (persondetailResponse.criminalRecordsMatch !== null && persondetailResponse.criminalRecordsMatch !== 'undefined') {
                CriminalDetails = persondetailResponse.criminalRecordsMatch;
            }
            if (persondetailResponse.propertyForeclosures !== null && persondetailResponse.propertyForeclosures !== 'undefined') {
                if (persondetailResponse.propertyForeclosures.length > 0) {
                    PropertyForeclosures = LatestRecord(persondetailResponse.propertyForeclosures);
                }
            }
        }
    }
    var records = {
        'CreditScore': CreditScore,
        'Liens': Liens,
        'Judgments': Judgments,
        'UCCFilings': UCCFilings,
        'ActiveFromSos': ActiveFromSos,
        'Bankruptcies': Bankruptcies,
        'CriminalDetails': CriminalDetails,
        'PropertyForeclosures': PropertyForeclosures,
        'SecondaryName': key,
        'referenceNumber': payload.eventData.ReferenceNumber,
        'owner': owner

    };
    Data[key] = records;
    return {
        'result': result,
        'detail': null,
        'data': Data,
        'rejectcode': '',
        'exception': []
    };

    function LatestRecord(data) {
        var latestDate = '';
        var currentDate = '';
        if (data.length > 0) {
            var latestData = data[0];
            if (data[0].filingDate !== null && data[0].filingDate.day !== null) {
                latestDate = new Date(data[0].filingDate.year, parseInt(data[0].filingDate.month), data[0].filingDate.day);
            }
            for (var i = 1; i < data.length; i++) {
                if (data[i].filingDate !== null) {
                    if (latestDate === '') {
                        latestDate = new Date(data[i].filingDate.year, parseInt(data[i].filingDate.month - 1), data[i].filingDate.day);
                    }
                    currentDate = data[i].filingDate.year +'/'+data[i].filingDate.month+'/'+data[i].filingDate.day;
                }
            }
            
            if (latestData !== null) {
                
                latestData.filingDate = currentDate;
            }
            return latestData;
        }
    }

    function LatestRecordBankruptcy(data) {
        var latestDate = '';
        var currentDate = '';
        if (data.length > 0) {
            var latestData = data[0];
            if (data[0].fileDate !== null && data[0].fileDate.day !== null) {
                latestDate = new Date(data[0].fileDate.year, parseInt(data[0].fileDate.month), data[0].fileDate.day);
            }
            for (var i = 1; i < data.length; i++) {
                if (data[i].fileDate !== null && data[i].fileDate.Day !== null) {
                    if (latestDate === '') {
                        latestDate = new Date(data[i].fileDate.year, parseInt(data[i].fileDate.month - 1), data[i].fileDate.day);
                    }
                    currentDate = data[i].fileDate.year + '/' +data[i].fileDate.month + '/' + data[i].fileDate.day;
                    if (currentDate >= latestDate) {
                        latestData = data[i];
                      
                    }
                }
            }
            if (latestData !== null) {
                latestData.fileDate = currentDate;
            }
            return latestData;
        }
    }
}