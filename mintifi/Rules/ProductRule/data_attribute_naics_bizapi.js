function data_attribute_naics_bizapi(payload) {
    var result = 'Passed';
    if (payload == null) {
        return 'null';
    }
    var self = this;
    var fourDigitSIC1 = null;
    var fourDigitSIC1Description = null;
    var fourDigitSIC2 = null;
    var fourDigitSIC2Description = null;
    var matchGrade = null;
    var confidenceCode = null;
    var appendedData = null;
    var matchingData = null;
    var sic1 = null;
    var sic2 = null;
    var sic1Matches = false;
    var sic2Matches = false;
    var lookupResult = {};
    var lookupService = self.call('lookup');
    var referenceNumber = '';
    if (payload.eventData != null && payload.eventData.Response != null) {
        appendedData = payload.eventData.Response.AppendedData;
        matchingData = payload.eventData.Response.MatchingData;
        referenceNumber = payload.eventData.ReferenceNumber
    }
    if (appendedData != null) {
        fourDigitSIC1 = appendedData.FourDigitSIC1;
        fourDigitSIC1Description = appendedData.FourDigitSIC1Description;
        fourDigitSIC2 = appendedData.FourDigitSIC2;
        fourDigitSIC2Description = appendedData.FourDigitSIC2Description;
    }
    if (matchingData != null) {
        matchGrade = matchingData.MatchGrade;
        confidenceCode = matchingData.ConfidenceCode;
    }
    sic1 = fourDigitSIC1;
    sic2 = fourDigitSIC2;
    return lookupService.get('prohibitedSIC').then(function (response) {
        lookupResult = response;

        if (lookupResult != null && lookupResult != undefined) {
            for (var key in lookupResult) {
                if (sic1 != null && sic1 != '' && sic1 == lookupResult[key]) {
                    sic1Matches = true;
                    result = 'Failed';

                }

                if (sic2 != null && sic2 != '' && sic2 == lookupResult[key]) {
                    sic2Matches = true;
                    result = 'Failed';

                }
            };
        }
        var Data = {
            'bizApiSearchReport': {
                'FourDigitSIC1': fourDigitSIC1,
                'FourDigitSIC1Description': fourDigitSIC1Description,
                'FourDigitSIC2': fourDigitSIC2,
                'FourDigitSIC2Description': fourDigitSIC2Description,
                'MatchGrade': matchGrade,
                'ConfidenceCode': confidenceCode,
                'Sic1Matches': sic1Matches,
                'Sic2Matches': sic2Matches,
                'referenceNumber': referenceNumber

            }
        };
        return {
            'result': result,
            'detail': null,
            'data': Data,
            'rejectcode': '',
            'exception': []
        };
    });

}
