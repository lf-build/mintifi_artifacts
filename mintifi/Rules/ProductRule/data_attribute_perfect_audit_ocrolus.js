function data_attribute_perfect_audit_ocrolus(payload) {
	var result = 'Passed';
	var average_daily_balance = '';
	var average_deposit_by_month = '';
	var estimated_revenue_by_month = '';
	var info = {};
	if (typeof (payload) != 'undefined') {
		var ocrolusResponse = payload.eventData.Response.response;
		if (ocrolusResponse != null && ocrolusResponse != undefined) {
			var arraybankaccount = [];
			if (ocrolusResponse.bank_accounts != undefined && ocrolusResponse.bank_accounts != null) {
				arraybankaccount = ocrolusResponse.bank_accounts;
				for (var i = 0; i < ocrolusResponse.bank_accounts.length; i++) {
					ocrolusResponse.bank_accounts[i].avgByMonth = Object.values(ocrolusResponse.bank_accounts[i].average_by_month)[0];
					ocrolusResponse.bank_accounts[i].avgDepositByMonth = Object.values(ocrolusResponse.bank_accounts[i].average_deposit_by_month)[0];
				}
				info = { 'info': arraybankaccount };
			}
			average_daily_balance = ocrolusResponse.average_daily_balance;
			average_deposit_by_month = Object.values(ocrolusResponse.average_deposit_by_month)[0];
			estimated_revenue_by_month = Object.values(ocrolusResponse.estimated_revenue_by_month)[0];
		}
		var Data = {
			'ocrolusData': {
				'OcrolusInfo': info,
				'AverageDailyBalance': average_daily_balance,
				'AverageDepositByMonth': average_deposit_by_month,
				'EstimatedRevenueByMonth': estimated_revenue_by_month
			}
		};
		return {
			'result': result,
			'detail': null,
			'data': Data,
			'rejectcode': '',
			'exception': []
		};
	}
}