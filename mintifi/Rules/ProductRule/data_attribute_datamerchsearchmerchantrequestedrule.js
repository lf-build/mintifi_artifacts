function data_attribute_datamerchsearchmerchantrequestedrule(payload) {
	var result = 'Passed';
	var fein = '';
	var legalName = '';
	var dba = '';
	var address = null;
	var street1 = '';
	var street2 = '';
	var city = '';
	var state = '';
	var businessPhone = '';
	var businessStartdate = '';
	var industry = '';
	var merchantAddress = null;
	var merchantNotes = {};
	var merchantNotesList = '';
	var isGoodStanding = true;
	var Category = 'Not Listed';
	var restrictedCategory = ['Slow Pay', 'Fraudulent', 'Split Payer', 'Stacking History', 'Default Account', 'Criminal History', 'Other'];
	if (typeof (payload) != 'undefined') {
		var datamerchSearchMerchantResponse = payload.eventData.Response.Merchants[0];
		fein = datamerchSearchMerchantResponse.Merchant.Fein;
		legalName = datamerchSearchMerchantResponse.Merchant.LegalName;
		dba = datamerchSearchMerchantResponse.Merchant.Dba;
		merchantAddress = datamerchSearchMerchantResponse.Merchant.Address;
		street1 = datamerchSearchMerchantResponse.Merchant.Street1;
		street2 = datamerchSearchMerchantResponse.Merchant.Street2;
		city = datamerchSearchMerchantResponse.Merchant.City;
		state = datamerchSearchMerchantResponse.Merchant.State;
		businessPhone = datamerchSearchMerchantResponse.Merchant.BusinessPhone;
		businessStartdate = datamerchSearchMerchantResponse.Merchant.BusinessStartdate;
		industry = datamerchSearchMerchantResponse.Merchant.Industry;
		merchantNotesList = datamerchSearchMerchantResponse.Merchant.MerchantNotes;
		var notes = [];
		if (street1 != null && street2 != null) {
			address = street1 + '' + street2;
		}
		if (merchantNotesList) {
			for (var i = 0; i < merchantNotesList.length; i++) {
				var note = {
					Category: null,
					Note: null,
					CreatedAt: null,
					AddedBy: null
				};
				note.Category = merchantNotesList[i].MerchantNote.Category;
				note.Note = merchantNotesList[i].MerchantNote.Note;
				note.CreatedAt = merchantNotesList[i].MerchantNote.CreatedAt;
				note.AddedBy = merchantNotesList[i].MerchantNote.AddedBy;
				notes.push(note);
			}
		}
		for (var i = 0; i < notes.length; i++) {
			if (restrictedCategory.indexOf(notes[i].Category) > -1) {
				Category = notes[i].Category;
				isGoodStanding = false;
				break;
			}
		}
		merchantNotes = {
			Notes: notes
		};
		var Data = {
			'datamerchReport': {
				'Fein': fein,
				'LegalName': legalName,
				'Dba': dba,
				'Address': address,
				'Street1': street1,
				'Street2': street2,
				'City': city,
				'State': state,
				'BusinessPhone': businessPhone,
				'BusinessStartdate': businessStartdate,
				'Industry': industry,
				'MerchantNotes': merchantNotes,
				'IsGoodStanding': isGoodStanding,
				'Category': Category,
				'referenceNumber': payload.eventData.ReferenceNumber
			}
		};
	}
	return {
		'result': result,
		'detail': null,
		'data': Data,
		'rejectcode': '',
		'exception': []
	};
}