function data_attribute_pandata(payload) {
	var result = 'Passed';
	var response = null;
	if (typeof (payload) != 'undefined') {
		if (payload.eventData.Response != null) {
			response = payload.eventData.Response;
		}

	}
	var Data = {
		'PanData': {
			'PanData':response,
			'referenceNumber': payload.eventData.ReferenceNumber
		}
	};
	return {
		'result': result,
		'detail': null,
		'data': Data,
		'rejectcode': '',
		'exception': []
	};
}
