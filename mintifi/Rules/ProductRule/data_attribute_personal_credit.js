function data_attribute_personal_credit(payload) {
	var objData = null;
	var OFACFlag = false;
	var SSNFlag = false;
	var CreditHistoryMonthsFlag = false;
	var BankruptcyMonthsFlag = false;
	var CreditScoreFlag = false;
	var CreditScoreLimit = 450;
	var CreditHistoryMonthsLimit = 36;
	var BankruptcyandLineMonthsLimit = 36;
	var creditScoreTotal = 0;
	var CreditHistory = '';
	var OFAC = null;
	var IsNotSSNFraudVictim = false;
	var CreditHistoryCheck = '';
	var OFACFlag = true;
	var Bankruptcy = null;
	var Lines = null;
	var Linesflag = false;
	var Bankruptcyflag = false;
	var LinesDate = '';
	var BankruptcyDate = '';
	var key = '';
	var Data = {};
	var BankruptcyCodes = ['c', 'd', 'g', 'i', 'v'];
	var BankruptcyDispostionflag = false;
	var result = 'Failed';
	var bankruptcyDates = {};
	var bankruptcyDatesList = '';
	var notes = [];
	try {
		if (payload !== null) {
			if (payload.eventData !== null && payload.eventData.Response !== null) {
				if (payload.eventData.Request != null) {
					key = payload.eventData.Request.OwnerId;
				}
				objData = payload.eventData.Response.Report.consumerCreditReport[0];
				if (objData !== null) {
					if (objData.usConsumerCreditReportType[0].usfico != null) {
						creditScoreTotal = objData.usConsumerCreditReportType[0].usfico.ficoScore;
					}
					if (objData.usConsumerCreditReportType[0].usHeader.usCreditFile.fileSinceDate != null) {
						CreditHistory = objData.usConsumerCreditReportType[0].usHeader.usCreditFile.fileSinceDate.value;
					}
					IsNotSSNFraudVictim = objData.usConsumerCreditReportType[0].usHeader.usCreditFile.fraudVictimIndicator === null ? false : true;
					OFAC = objData.usConsumerCreditReportType[0].usofacAlerts;
					Bankruptcy = objData.usConsumerCreditReportType[0].usBankruptcies;
					Lines = objData.usConsumerCreditReportType[0].usTaxLiens;
				}
				if (Lines != null) {
					for (var i = 0; i < Lines.length; i++) {
						LinesDate = Lines[i].dateFiled.value;
						var monthdiff = GetMonthDiff(new Date(LinesDate), new Date());
						if (monthdiff <= BankruptcyandLineMonthsLimit) {
							Linesflag = true;
							break;
						}
					}
				}
				if (Bankruptcy != null) {
					for (var i = 0; i < Bankruptcy.length; i++) {
						BankruptcyDate = Bankruptcy[i].dateFiled.value;
						var date = '01/' + BankruptcyDate;
						var monthdiff = GetMonthDiff(new Date(date), new Date());
						if (monthdiff <= BankruptcyandLineMonthsLimit) {
							Bankruptcyflag = true;
							break;
						}
						var disposition = Bankruptcy[i].disposition;
						if (disposition != null) {
							if (BankruptcyCodes.indexOf(disposition.code.toLowerCase()) > -1) {
								BankruptcyDispostionflag = true;
								break;
							}
						}
					}
				}
				if (Bankruptcy != null) {
					for (var i = 0; i < Bankruptcy.length; i++) {
						var note = {
							DateFiled: null,
							IndustryCode: null,
							DispositionCode: null
						};
						note.DateFiled = Bankruptcy[i].dateFiled.value;
						note.IndustryCode = Bankruptcy[i].type.code + ' (' + Bankruptcy[i].type.description + ')';
						note.DispositionCode = Bankruptcy[i].disposition.code + ' (' + Bankruptcy[i].disposition.description + ')';
						notes.push(note);
					}
				}
				bankruptcyDates = {
					Notes: notes
				};
				if (CreditHistory !== null && CreditHistory !== undefined) {
					CreditHistoryCheck = GetMonthDiff(new Date(CreditHistory), new Date());
				}
				if (objData !== null) {
					if (parseInt(creditScoreTotal) > CreditScoreLimit) {
						CreditScoreFlag = true;
					}
					if (parseInt(CreditHistoryCheck) > CreditHistoryMonthsLimit) {
						CreditHistoryMonthsFlag = true;
					}
					if (OFAC === null) {
						OFACFlag = false;
					} else {
						var OFACAlert = OFAC[0];
						if (OFACAlert != null) {
							if (OFACAlert.responseCode.code == 'O') {
								OFACFlag = true;
							} else {
								OFACFlag = false;
							}
						}
					}
					SSNFlag = IsNotSSNFraudVictim;
				}
				result = 'Passed';
			}
		}
		var records = {
			'CreditScore': creditScoreTotal,
			'CreditHistory': CreditHistory,
			'OFAC': OFAC,
			'IsNotSSNFraudVictim': IsNotSSNFraudVictim,
			'CreditScoreFlag': CreditScoreFlag,
			'CreditHistoryMonthsFlag': CreditHistoryMonthsFlag,
			'OFACFlag': OFACFlag,
			'SSNFlag': SSNFlag,
			'SecondaryName': key,
			'CreditHistoryCheck': CreditHistoryCheck,
			'BankruptcyDate': BankruptcyDate,
			'LinesDate': LinesDate,
			'Linesflag': Linesflag,
			'Bankruptcyflag': Bankruptcyflag,
			'BankruptcyDispostionflag': BankruptcyDispostionflag,
			'bankruptcyDates': bankruptcyDates
		};
		Data[key] = records;
		return {
			'result': result,
			'detail': null,
			'data': Data,
			'rejectcode': '',
			'exception': []
		};
		function GetMonthDiff(d1, d2) {
			var months;
			months = (d2.getFullYear() - d1.getFullYear()) * 12;
			months -= d1.getMonth() + 1;
			months += d2.getMonth();
			return months <= 0 ? 0 : months;
		}
	} catch (e) {
		return {
			'result': result,
			'detail': null,
			'data': null,
			'rejectcode': '',
			'exception': [e.message]
		};
	}
}