function data_attribute_applicationcreatedmodified(payload) {
    var result = 'Passed';
    var BusinessApplicantName = null;
    var ContactFirstName = null;
    var ContactLastName = null;
    var ContactMiddleName = null;
    var ApplicantBankIsDefault = null;
    var ApplicantBankIsVerified = null;
    var ApplicantBankVerifiedDate = null;
    var ApplicantBankAccountHolderName = null;
    var ApplicantBankAccountNumber = null;
    var ApplicantBankAccountType = null;
    var ApplicantBankId = null;
    var ApplicantBankName = null;
    var ApplicantBankRoutingNumber = null;
    var ApplicantBankVerifiedBy = null;
    var ApplicantPhone = null;
    var BusinessPhone = null;
    var ApplicantWorkEmail = '';
    var Tags = null;
    var Gurantors = null;
    var LoanTimeFrame = null;
    var Submitted = null;
    var ProductId = null;
    var RequestedTermType = null;
    var ApplicationId = null;
    var ApplicationNumber = null;
    var ApplicationDate = null;
    var RequestedAmount = null;
    var RequestedTermValue = null;
    var PurposeOfLoan = null;
    var DecisionDate = null;
    var ExpiryDate = null;
    var BusinessAddress = null;
    var SocialLinks = null;
    var BusinessFax = null;
    var BusinessWebsite = null;
    var BusinessStartDate = null;
    var LegalBusinessName = null;
    var BusinessType = null;
    var SourceId = null;
    var SourceType = null;
    var StatusHistory = null;
    var StatusCode = null;
    var StatusName = null;
    var StatusDate = null;
    var Owners = null;
    var AnnualRevenue = null;
    var AverageBankBalance = null;
    var PropertyType = null;
    var Industry = null;
    var HaveExistingLoan = null;
    var DrawDownAmount = 0;
    var BusinessLocation = null;
    var LoanPriority = null;
    var TimeInBusinessResult = true;
    var TIB = '';
    var State = null;
    var UseOfFunds = null;
    var ZipCode = null;
    var RegisteredBusinessName = null;
    var LegalStatusOfBusiness = null;
    var NatureOfBusiness = null;
    var GSTRegistrationDate = null;
    var StatusOfRegistration = null;
    var AuthorizedSignatories = null;
    var BusinessContactEmail = null;
    var GstRegiterdAddress = null;
    var GstRegiterdState = null;
    var IsPrimaryContactDetail = null;
    var TinPanServiceTaxAssesseeCode = null;
    var Gstin = null;
    var CinLlpin = null;
    var WhereToSell = null;
    var TypeOfProductOrService = null;
    var LoanAmount='';
    var SelfReportedAnnualRevenue='';
    var Address=null;
    var Phone='';

    if (typeof (payload) != 'undefined') {
        var application = payload.eventData.Application;
        var applicant = payload.eventData.Applicant;
        if (applicant != null && application != null) {
            if (applicant.PhoneNumbers != null && applicant.PhoneNumbers.length > 0) {
                for (var i = 0; i < applicant.PhoneNumbers.length; i++) {
                    if (applicant.PhoneNumbers[i].PhoneType == 'Mobile') {
                        ApplicantPhone = applicant.PhoneNumbers[i].Phone;
                    }
                }
            }
            if (applicant.BusinessPhone != null) {
                BusinessPhone = applicant.BusinessPhone.Phone;
            }
            if (application.PrimaryPhone != null) {

                Phone = application.PrimaryPhone.Phone;
            }
            if (application.PrimaryEmail != null) {

                ApplicantWorkEmail = application.PrimaryEmail.Email;
            }
            if (applicant.Owners != null && applicant.Owners.length > 0) {
                Owners = applicant.Owners;
                BusinessApplicantName = applicant.Owners[0].FirstName + ' ' + applicant.Owners[0].LastName;
                ContactFirstName = application.ContactFirstName;
                ContactMiddleName = null;
                ContactLastName = application.ContactLastName;
            }
            if (applicant.PrimaryAddress != null) {
                BusinessAddress = applicant.PrimaryAddress;
            }
            if (applicant.Addresses != null && applicant.Addresses.length>0) {
               for(i=0; i<applicant.Addresses.length;i++)
               {
                   if(applicant.Addresses[i].AddressType.toLowerCase()=='gst')
                   Address = applicant.Addresses[i];
               }
            }
            if (applicant.EmailAddresses != null && applicant.EmailAddresses.length>0) {
                for(i=0; i<applicant.EmailAddresses.length;i++)
                {
                    if(applicant.EmailAddresses[i].EmailType.toLowerCase()=='work')
                    BusinessContactEmail = applicant.EmailAddresses[i].Email;
                }
             }
            if (applicant.SocialLinks != null) {
                SocialLinks[0] = applicant.SocialLinks.FacebookAddress;
                SocialLinks[1] = applicant.SocialLinks.YelpAddress;
                SocialLinks[2] = applicant.SocialLinks.GoogleAddress;
            }
            BusinessFax = applicant.PrimaryFax;
            BusinessWebsite = applicant.BusinessWebsite;
            BusinessStartDate = applicant.BusinessStartDate;
            LegalBusinessName = applicant.LegalBusinessName;
            BusinessType = applicant.BusinessType;
            ProductId = application.ProductId;
            DrawDownAmount = application.TotalDrawDownAmount;
            if (application.LinkedBankInformation != null) {
                ApplicantBankIsDefault = application.LinkedBankInformation.IsDefault;
                ApplicantBankAccountHolderName = application.LinkedBankInformation.AccountHolderName;
                ApplicantBankName = application.LinkedBankInformation.BankName;
                ApplicantBankId = application.LinkedBankInformation.BankId;
                ApplicantBankRoutingNumber = application.LinkedBankInformation.RoutingNumber;
                ApplicantBankVerifiedBy = application.LinkedBankInformation.VerifiedBy;
                ApplicantBankIsVerified = application.LinkedBankInformation.IsVerified;
                ApplicantBankVerifiedDate = application.LinkedBankInformation.VerifiedDate;
                ApplicantBankAccountNumber = application.LinkedBankInformation.AccountNumber;
                ApplicantBankAccountType = application.LinkedBankInformation.AccountType;
            }
            ApplicationId = application.ApplicantId;
            ApplicationNumber = application.ApplicationNumber;
            LoanTimeFrame = application.LoanTimeFrame;
            Gurantors = application.Gurantors;
            Submitted = application.ApplicationDate;
            ApplicationDate = application.ApplicationDate;
            RequestedAmount = application.RequestedAmount;
            RequestedTermType = application.RequestedTermType;
            RequestedTermValue = application.RequestedTermValue;
            PurposeOfLoan = application.PurposeOfLoan;
            DecisionDate = application.DecisionDate;
            ExpiryDate = application.ExpiryDate;
            AnnualRevenue = application.SelfDeclareInformation.AnnualRevenue;
            AverageBankBalance = application.SelfDeclareInformation.AverageBankBalance;
            HaveExistingLoan = application.SelfDeclareInformation.IsExistingBusinessLoan;
            SelfReportedAnnualRevenue=application.SelfDeclareInformation.SelfReportedAnnualRevenue;
            PropertyType = applicant.PropertyType;
            Industry = applicant.Industry;
            BusinessLocation = applicant.BusinessLocation;
            LoanPriority = applicant.LoanPriority;
            if (application.Source != null) {
                SourceId = application.Source.SourceReferenceId;
                SourceType = application.Source.SourceType;
            }
            StatusCode = payload.eventData.StatusCode;
            StatusDate = payload.eventData.StatusDate;
            StatusName = payload.eventData.StatusName;
            Tags = payload.eventData.Tags;
            TIB = GetMonthDiff(new Date(BusinessStartDate.Time), new Date());
            if (TIB < 36) {
                TimeInBusinessResult = false;
            }
            State = applicant.State;
            UseOfFunds = applicant.UseOfFunds;
            ZipCode = applicant.UseOfFunds;
            RegisteredBusinessName = applicant.RegisteredBusinessName;
            LegalStatusOfBusiness = applicant.LegalStatusOfBusiness;
            NatureOfBusiness = applicant.NatureOfBusiness;
            GSTRegistrationDate = applicant.GSTRegistrationDate;
            StatusOfRegistration = applicant.StatusofRegistration;
            AuthorizedSignatories = applicant.AuthorizedSignatories;
            GstRegiterdAddress = applicant.GstRegiterdAddress;
            GstRegiterdState = applicant.GstRegiterdState;
            IsPrimaryContactDetail = applicant.IsPrimaryContactDetail;
            TinPanServiceTaxAssesseeCode = applicant.TinPanServiceTaxAssesseeCode;
            CinLlpin = applicant.CinLlpin;
            Gstin = applicant.Gstin;
            WhereToSell = applicant.whereToSell;
            TypeOfProductOrService = applicant.TypeOfProductOrService;
            LoanAmount = application.LoanAmount;
            

        }
    }
    var Data = {
        'application': {
            'businessApplicantName': BusinessApplicantName,
            'applicationDate': ApplicationDate,
            'contactFirstName': ContactFirstName,
            'contactLastName': ContactLastName,
            'contactMiddleName': ContactMiddleName,
            'applicantBankIsDefault': ApplicantBankIsDefault,
            'applicantBankIsVerified': ApplicantBankIsVerified,
            'applicantBankVerifiedDate': ApplicantBankVerifiedDate,
            'applicantBankAccountHolderName': ApplicantBankAccountHolderName,
            'applicantBankAccountNumber': ApplicantBankAccountNumber,
            'applicantBankAccountType': ApplicantBankAccountType,
            'applicantBankId': ApplicantBankId,
            'applicantBankName': ApplicantBankName,
            'applicantBankRoutingNumber': ApplicantBankRoutingNumber,
            'applicantBankVerifiedBy': ApplicantBankVerifiedBy,
            'applicantPhone': ApplicantPhone,
            'tags': Tags,
            'gurantors': Gurantors,
            'loanTimeFrame': LoanTimeFrame,
            'submitted': Submitted,
            'productId': ProductId,
            'requestedTermType': RequestedTermType,
            'applicationId': ApplicationId,
            'applicationNumber': ApplicationNumber,
            'requestedAmount': RequestedAmount,
            'requestedTermValue': RequestedTermValue,
            'purposeOfLoan': PurposeOfLoan,
            'decisionDate': DecisionDate,
            'expiryDate': ExpiryDate,
            'businessAddress': BusinessAddress,
            'socialLinks': SocialLinks,
            'businessFax': BusinessFax,
            'businessWebsite': BusinessWebsite,
            'businessStartDate': BusinessStartDate,
            'legalBusinessName': LegalBusinessName,
            'businessType': BusinessType,
            'applicantWorkEmail': ApplicantWorkEmail,
            'sourceId': SourceId,
            'sourceType': SourceType,
            'statusHistory': StatusHistory,
            'statusCode': StatusCode,
            'statusName': StatusName,
            'statusDate': StatusDate,
            'owners': Owners,
            'businessPhone': BusinessPhone,
            'annualRevenue': AnnualRevenue,
            'averageBankBalance': AverageBankBalance,
            'haveExistingLoan': HaveExistingLoan,
            'propertyType': PropertyType,
            'industry': Industry,
            'DrawDownAmount': DrawDownAmount,
            'businessLocation': BusinessLocation,
            'loanPriority': LoanPriority,
            'timeInBusinessResult': TimeInBusinessResult,
            'UseOfFunds': UseOfFunds,
            'ZipCode': ZipCode,
            'RegisteredBusinessName': RegisteredBusinessName,
            'LegalStatusOfBusiness': LegalStatusOfBusiness,
            'NatureOfBusiness': NatureOfBusiness,
            'GSTRegistrationDate': GSTRegistrationDate,
            'StatusOfRegistration': StatusOfRegistration,
            'AuthorizedSignatories': AuthorizedSignatories,
            'BusinessContactEmail': BusinessContactEmail,
            'GstRegiterdState': GstRegiterdState,
            'IsPrimaryContactDetail': IsPrimaryContactDetail,
            'TinPanServiceTaxAssesseeCode': TinPanServiceTaxAssesseeCode,
            'State': State,
            'CinLlpin': CinLlpin,
            'Gstin':Gstin,
            'WhereToSell': WhereToSell,
            'TypeOfProductOrService': TypeOfProductOrService,
            'loanAmount':LoanAmount,
            'SelfReportedAnnualRevenue':SelfReportedAnnualRevenue,
            'Address':Address,
            'Phone':Phone

        }
    };
    return {
        'result': result,
        'detail': null,
        'data': Data,
        'rejectcode': '',
        'exception': []
    };

    function GetMonthDiff(d1, d2) {
        var months;
        months = (d2.getFullYear() - d1.getFullYear()) * 12;
        months -= d1.getMonth() + 1;
        months += d2.getMonth();
        return months <= 0 ? 0 : months;
    }
}