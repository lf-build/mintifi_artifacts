var path = require('path');
var fs = require('fs');

var walkSync = function (dir, filelist) {
    var path = path || require('path');
    var fs = fs || require('fs'),
        files = fs.readdirSync(dir);
    filelist = filelist || [];
    files.forEach(function (file) {
        if (fs.statSync(path.join(dir, file)).isDirectory()) {
            filelist = walkSync(path.join(dir, file), filelist);
        }
        else {
            filelist.push(path.join(dir, file));
        }
    });
    return filelist;
};

function saveFilter(serviceUrl, filterFile, token) {
    var api = require('./api_module.js')(token);
    var pathObject = path.parse(filterFile);
    if (pathObject.ext.toLocaleLowerCase() == ".json") {
        console.log('Publishing Open filter ' + filterFile);
        var data = fs.readFileSync(filterFile, 'utf8');
        var filterKey = pathObject.name;
        console.log(serviceUrl + '/SaveSearch');
        return api.post(serviceUrl + '/SaveSearch', data).then(function(result) {
            if (result.statusCode == 200 || result.statusCode == 204) {
                console.log("Filter Published " + filterKey);
            } else {
                console.log(result);
                console.log("Failed to Publish Filter " + filterKey);
            }
        });
    } else {
        console.warn('Skipping file ' + filterFile + ' as extension :' + pathObject.ext.toLocaleLowerCase());
    }
}

function importFilter(serviceUrl, folderPath, token) {
    var files = walkSync(folderPath);
    var lookupToProcess = [];

    for (var i = 0; i < files.length; i++) {
        lookupToProcess.push(files[i]);
    }

    lookupToProcess = lookupToProcess.map(r => () => saveFilter(serviceUrl, r, token));

    return lookupToProcess.reduce((a, c) => {
        return a.then(_ => {
            return c();
        }).catch(reason => {
            //TODO: Check if fail, it will publish others
            console.error('Lookup failed: ', reason);
            return c();
        });
    }, new Promise((resolve) => resolve({})));
}

module.exports = function (token) {
    return {
        saveFilter: function (serviceUrl, filterFile) {
            var pathObject = path.parse(filterFile);
            if(pathObject.dir.endsWith("application")){
                return saveFilter(serviceUrl + "application", filterFile, token);
            }
            else if(pathObject.dir.endsWith("drawdown")){
                return saveFilter(serviceUrl + "drawdown", filterFile, token);
            }
            else if(pathObject.dir.endsWith("loan")){
                return saveFilter(serviceUrl + "loan", filterFile, token);
            }
            else if(pathObject.dir.endsWith("cashflow")){
                return saveFilter(serviceUrl + "cashflow", filterFile, token);
            }
            else{
                console.log("Invalid filter type");
            }
        },
        importFilter: function (serviceUrl, folderPath) {
            return importFilter(serviceUrl + "application", folderPath + "/application", token).then(function(){
                return importFilter(serviceUrl + "drawdown", folderPath + "/drawdown", token).then(function(){
                    return importFilter(serviceUrl + "loan", folderPath + "/loan", token).then(function(){
                        return importFilter(serviceUrl + "cashflow", folderPath + "/cashflow", token);
                    })
                });
            });;
        }
    }
}
