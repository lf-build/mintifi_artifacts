const mongoConnectionString = 'mongodb://fc360:fcSigma@35.165.224.155:27017/loan-management?authSource=admin';
const collectionName = 'loan-onboarding';
const {
	MongoClient
} = require('mongodb');
const {
	map
} = require('ramda');

MongoClient.connect(mongoConnectionString)
.then((db) => {
	var x = new Date();
	console.log(x.toTimeString());
	db.collection(collectionName).find({}).toArray()
	.then((codeMap) => {
		var bulk = db.collection(collectionName).initializeUnorderedBulkOp();
		var loan_management = codeMap.map(function (x) {
				var data = x;
				if (data.AutoPayHistory == undefined || data.AutoPayHistory == null || data.AutoPayHistory.length <= 0)
					return data;
				for (var i = 0; i < data.AutoPayHistory.length; i++) {
					(function (j) {
					
						if (j.AutoPayDate != undefined && j.AutoPayDate != null) {
							if (j.IsAutoPay){							
								j.ResumeDate = j.AutoPayDate;
								j.StopDate = null;
							}
							else{							
								j.StopDate = j.AutoPayDate;
								j.ResumeDate = null;
							}
							delete j.AutoPayDate;
						}
					})(data.AutoPayHistory[i]);
				}
         
				bulk.find({_id: data._id}).updateOne(data);
				
				return data;
			});

		bulk.execute();
	}).then((statuses) => {
		var x = new Date();
		console.log(x.toTimeString());
		db.close();
	})
	.catch (function (e) {
		console.log(e.message);
	});
})
.catch ((e) => {
	console.log(e.message);
});
