var path = require('path');
var fs = require('fs');

var walkSync = function (dir, filelist) {
    var path = path || require('path');
    var fs = fs || require('fs'),
        files = fs.readdirSync(dir);
    filelist = filelist || [];
    files.forEach(function (file) {
        if (fs.statSync(path.join(dir, file)).isDirectory()) {
            filelist = walkSync(path.join(dir, file), filelist);
        }
        else {
            filelist.push(path.join(dir, file));
        }
    });
    return filelist;
};

function saveLookup(serviceUrl, lookupFile, token) {
    var api = require('./api_module.js')(token);
    var pathObject = path.parse(lookupFile);
    if (pathObject.ext.toLocaleLowerCase() == ".json") {
        //console.log('Publishing Open Lookup ' + lookupFile);
        var name = pathObject.name;
        var data = fs.readFileSync(lookupFile, 'utf8').replace(/\r|\n|\t/g, ' ');
        return api.delete(serviceUrl + '/' + name).then(function () {
            return api.post(serviceUrl + '/' + name, data).then(function (result) {
                console.log(serviceUrl + '/' + name);
                if (result.statusCode == 200 || result.statusCode == 204) {
                    console.log("[Done] Publishing lookup: " + name);
                }
                else {
                    console.log(result);
                    console.log("[Error] Failed to publish lookup: " + name);
                }
            });
        });
    }
}

function importLookup(serviceUrl, folderPath, token) {
    var files = walkSync(folderPath);
    var lookupToProcess = [];

    for (var i = 0; i < files.length; i++) {
        lookupToProcess.push(files[i]);
    }

    lookupToProcess = lookupToProcess.map(r => () => saveLookup(serviceUrl, r, token));

    return lookupToProcess.reduce((a, c) => {
        return a.then(_ => {
            return c();
        }).catch(reason => {
            //TODO: Check if fail, it will publish others
            console.error('Lookup failed: ', reason);
            return c();
        });
    }, new Promise((resolve) => resolve({})));
}

module.exports = function (token) {
    return {
        saveLookup: function (serviceUrl, lookupFile) {
            return saveLookup(serviceUrl, lookupFile, token);
        },
        importLookup: function (serviceUrl, folderPath) {
            return importLookup(serviceUrl, folderPath, token);
        }
    }
}